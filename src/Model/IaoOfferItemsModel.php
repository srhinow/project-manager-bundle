<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Model;

class IaoOfferItemsModel extends IaoModel
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_iao_offer_items';

    /**
     * Find published entries by their parent (project) ID.
     *
     * @param int   $pid        An Project-ID from actual FrontendUser
     * @param int   $intLimit   An optional limit
     * @param int   $intOffset  An optional offset
     * @param array $arrOptions An optional options array
     *
     * @return \Contao\Model\Collection|IaoModel|null
     */
    public static function findPublishedItemsByOfferId($pid, $intLimit = 0, $intOffset = 0, array $arrOptions = [])
    {
        if (empty($pid)) {
            return null;
        }

        $t = static::$strTable;

        // nach Project filtern
        $arrColumns = ["$t.pid =".$pid];

        // veroeffentlicht
        $arrColumns[] = "$t.published='1'";

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.tstamp DESC";
        }

        $arrOptions['limit'] = $intLimit;
        $arrOptions['offset'] = $intOffset;

        return static::findBy($arrColumns, null, $arrOptions);
    }
}
