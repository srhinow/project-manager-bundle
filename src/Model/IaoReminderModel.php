<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Model;

class IaoReminderModel extends IaoModel
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_iao_reminder';

    public static function getLastReminder($invoiceId = null, $reminderId = null)
    {
        if (!$invoiceId) {
            return false;
        }

        $arrColumns = ['`invoice_id`=?'];
        $arrValues = [$invoiceId, 1];
        $arrOptions = ['order' => 'id DESC'];

        if ($reminderId) {
            $arrColumns[] = self::$strTable.'.`id`!=?';
            $arrValues[] = $reminderId;
        }

        return self::findOneBy($arrColumns, $arrValues, $arrOptions);
    }

    /**
     * count reminder entries for Reminder-Status
     * (
     * erinnerung = 1,
     * erste Abmahnung = 2
     * zweite Abmahnung = 3
     * dritte Abmahnung = 4
     * ).
     *
     * @param int        $invoiceId
     * @param mixed|null $reminderId
     *
     * @return bool|int
     */
    public static function countForReminderStatus($invoiceId = 0, $reminderId = null)
    {
        if (0 === $invoiceId) {
            return false;
        }

        $arrColumns = ['`invoice_id`=?', '`published`=?'];
        $arrValues = [$invoiceId, 1];

        if ($reminderId) {
            $arrColumns[] = '`id`!=?';
            $arrValues[] = $reminderId;
        }

        return self::countBy($arrColumns, $arrValues);
    }
}
