<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Model;

use Contao\Model;

class IaoModel extends Model
{
    /**
     * @param $memberId
     * @param string $status
     * @param int    $intLimit
     * @param int    $intOffset
     *
     * @return Model\Collection|IaoModel|null
     */
    public static function findPublishedByMember($memberId, $status = '', $intLimit = 0, $intOffset = 0, array $arrOptions = [])
    {
        if (empty($memberId)) {
            return null;
        }

        $t = static::$strTable;

        // Rechnnung nach  aktuellem Mitglied filtern
        $arrColumns = ["$t.member=".$memberId];

        // veroeffentlicht
        $arrColumns[] = "$t.published='1'";

        // falls angegeben nach Status der Rechnung filtern
        if ('' !== $status) {
            $arrColumns[] = "$t.status=".$status;
        }

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.tstamp DESC";
        }

        $arrOptions['limit'] = $intLimit;
        $arrOptions['offset'] = $intOffset;

        return static::findBy($arrColumns, null, $arrOptions);
    }

    /**
     * Find published invoice by their parent ID.
     *
     * @param int   $id         An invoice-ID
     * @param int   $memberId   An User-ID from actual FrontendUser
     * @param array $arrOptions An optional options array
     *
     * @return IaoModel|null
     */
    public static function findPublishedByMemberAndPk($id, $memberId, array $arrOptions = [])
    {
        if (empty($id) || empty($memberId)) {
            return null;
        }

        $t = static::$strTable;

        // Rechnnung nach  aktuellem Mitglied filtern
        $arrColumns = ["$t.id=".$id];

        // Rechnnung nach  aktuellem Mitglied filtern
        $arrColumns[] = "$t.member=".$memberId;

        // veroeffentlicht
        $arrColumns[] = "$t.published='1'";

        return static::findOneBy($arrColumns, null, $arrOptions);
    }

    /**
     * Find published entries by their parent (project) ID.
     *
     * @param int    $pid        An Project-ID from actual FrontendUser
     * @param string $status     optional filter
     * @param int    $intLimit   An optional limit
     * @param int    $intOffset  An optional offset
     * @param array  $arrOptions An optional options array
     *
     * @return Model\Collection|IaoModel|null
     */
    public static function findPublishedByPid($pid, $status = '', $intLimit = 0, $intOffset = 0, array $arrOptions = [])
    {
        if (empty($arrPids)) {
            return null;
        }

        $t = static::$strTable;

        // nach Project filtern
        $arrColumns = ["$t.pid =".$pid];

        // veroeffentlicht
        $arrColumns[] = "$t.published='1'";

        //falls angegeben nach Status der Rechnung filtern
        if ('' !== $status) {
            $arrColumns[] = "$t.status=".$status;
        }

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.tstamp DESC";
        }

        $arrOptions['limit'] = $intLimit;
        $arrOptions['offset'] = $intOffset;

        return static::findBy($arrColumns, null, $arrOptions);
    }

    /**
     * Count published entites by their member (Frontend-User) ID.
     *
     * @param int    $memberId   An Member-ID from actual FrontendUser
     * @param string $status     optional filter
     * @param array  $arrOptions An optional options array
     *
     * @return int|null
     */
    public static function countPublishedByMember($memberId, $status = '', array $arrOptions = [])
    {
        if (empty($memberId)) {
            return null;
        }

        $t = static::$strTable;

        // Eintraege nach  aktuellem Mitglied filtern
        $arrColumns = ["$t.member=".$memberId];

        // veroeffentlicht
        $arrColumns[] = "$t.published='1'";

        //falls angegeben nach Status der Rechnung filtern
        if ('' !== $status) {
            $arrColumns[] = "$t.status=".$status;
        }

        return static::countBy($arrColumns, null, $arrOptions);
    }

    /**
     * Count published credit by their parent (project) ID.
     *
     * @param int    $pid        An Project-ID from actual FrontendUser
     * @param string $status     optional filter
     * @param array  $arrOptions An optional options array
     *
     * @return int|null
     */
    public static function countPublishedByPid($pid, $status = '', array $arrOptions = [])
    {
        if (empty($pid)) {
            return null;
        }

        $t = static::$strTable;

        // nach Project filtern
        $arrColumns = ["$t.pid =".$pid];

        // veroeffentlicht
        $arrColumns[] = "$t.published='1'";

        //falls angegeben nach Status der Rechnung filtern
        if ('' !== $status) {
            $arrColumns[] = "$t.status=".$status;
        }

        return static::countBy($arrColumns, null, $arrOptions);
    }
}
