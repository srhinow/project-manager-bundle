<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Model;

class IaoProjectsModel extends IaoModel
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_iao_projects';

    /**
     * Find published project their ID or alias.
     *
     * @param mixed $varId      The numeric ID or alias name
     * @param array $arrOptions An optional options array
     */
    public static function findOnePublicProject($varId, array $arrOptions = [])
    {
        $isAlias = !preg_match('/^[1-9]\d*$/', $varId);
        $t = static::$strTable;

        // Try to load from the registry
        $arrColumns = ($isAlias) ? ["$t.reference_alias='".$varId."'"] : ["$t.id=".$varId];

        // veroeffentlicht
        $arrColumns[] = "$t.finished='1'";
        $arrColumns[] = "$t.in_reference='1'";

        return static::findOneBy($arrColumns, null, $arrOptions);
    }

    /**
     * Find bbk-items for pagination-list.
     *
     * @param int $intLimit
     * @param int $intOffset
     *
     * @return \Contao\Model\Collection|IaoProjectsModel|null
     */
    public static function findProjects($intLimit = 0, $intOffset = 0, array $filter = [], array $arrOptions = [])
    {
        $t = static::$strTable;
        $arrColumns = (\count($filter) > 0) ? $filter : null;

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.id DESC";
        }

        $arrOptions['limit'] = $intLimit;
        $arrOptions['offset'] = $intOffset;

        return static::findBy($arrColumns, null, $arrOptions);
    }

    /**
     * Count all project items.
     *
     * @param array $filter     where-options
     * @param array $arrOptions An optional options array
     *
     * @return int
     */
    public static function countEntries(array $filter = [], array $arrOptions = [])
    {
        $t = static::$strTable;
        $arrColumns = (\count($filter) > 0) ? $filter : null;

        return static::countBy($arrColumns, null, $arrOptions);
    }
}
