<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Model;

class IaoAgreementsModel extends IaoModel
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_iao_agreements';

    /**
     * Find published agreement by their parent ID.
     *
     * @param int    $memberId   An User-ID from actual FrontendUser
     * @param string $status     optional filter
     * @param int    $intLimit   An optional limit
     * @param int    $intOffset  An optional offset
     * @param array  $arrOptions An optional options array
     *
     * @return \Contao\Model\Collection|IaoModel|null
     */
    public static function findPublishedByMember(
        $memberId,
        $status = '',
        $intLimit = 0,
        $intOffset = 0,
        array $arrOptions = []
    ) {
        $t = static::$strTable;

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.tstamp DESC";
        }

        return parent::findPublishedByMember($memberId, $status, $intLimit, $intOffset, $arrOptions);
    }

    /**
     * Find published agreement by their parent (project) ID.
     *
     * @param int    $pid        An Project-ID from actual FrontendUser
     * @param string $status     optional filter
     * @param int    $intLimit   An optional limit
     * @param int    $intOffset  An optional offset
     * @param array  $arrOptions An optional options array
     *
     * @return \Contao\Model\Collection|IaoModel|null
     */
    public static function findPublishedByPid($pid, $status = '', $intLimit = 0, $intOffset = 0, array $arrOptions = [])
    {
        $t = static::$strTable;

        if (!isset($arrOptions['order'])) {
            $arrOptions['order'] = "$t.tstamp DESC";
        }

        return parent::findPublishedByPid($pid, $status, $intLimit, $intOffset, $arrOptions);
    }
}
