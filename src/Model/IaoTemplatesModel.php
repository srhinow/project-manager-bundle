<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Model;

class IaoTemplatesModel extends IaoModel
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_iao_templates';
}
