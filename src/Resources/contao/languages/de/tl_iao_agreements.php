<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

$GLOBALS['TL_LANG']['tl_iao_agreements']['setting_id'] = ['Konfiguration', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['pid'] = [
    'Projekt',
    'Optional können sie hier das entsprechende Projekt auswählen.',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['title'] = ['Bezeichnung', 'Bezeichnung des Elementes'];
$GLOBALS['TL_LANG']['tl_iao_agreements']['agreement_pdf_file'] = [
    'Vertrag',
    'Vertrag als PDF-Datei zuweisen.',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['member'] = [
    'Kunde',
    'Adresse aus gespeicherten Kunden in nachstehendes Feld befüllen',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['text_generate'] = [
    'Adresstext aus Kundendaten generieren',
    'VORSICHT! Der evtl. bestehende Adresstext wird gelöscht.',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['address_text'] = [
    'Mahnungs-Adresse',
    'Adresse die in der Mahnungs-PDF-Datei geschrieben wird.',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['published'] = ['veröffentlicht', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['status'] = ['Status dieses Vertrages', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['periode'] = [
    'Periode',
    'Geben Sie die Periode in Form von strtotime ein z.B. +1 year = 1 Jahr weiter, +2 months = weitere 2 Monate',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['agreement_date'] = ['Vertrag seit', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['beginn_date'] = ['Zyklusbeginn', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['end_date'] = ['Zyklusende', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['terminated_date'] = ['gekündigt zum', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['new_generate'] = ['den neuen Zyklus setzen', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['sendEmail'] = ['Email-Erinnerung einrichten', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['remind_before'] = [
    'erinnern vor Ablauf des Vertrags-Zyklus',
    'Die Angaben müssen im strtotime-Format (z.B. -3 weeks) gesetzt werden.',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['email_from'] = [
    'Email-Sender',
    'Die Absendeadresse sollte hier zur Domain gehören, sonst kann es sein das der Server diese Email nicht sendet.',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['email_to'] = [
    'Email-Empfänger',
    'Die Emailadresse, zur der die Erinnerung das eine Vertragsperiode endet, gesendet werden soll.',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['email_subject'] = ['Email-Betreff', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['email_text'] = ['Email-Text', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['before_template'] = [
    'Text vor den Posten',
    'Eine Auswahl an Rechnungsvorlagen',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['after_template'] = [
    'Text nach den Posten',
    'Eine Auswahl an Rechnungsvorlagen',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['posten_template'] = [
    'Posten-Template',
    'Eine Auswahl an Rechnungsposten',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['notice'] = ['Notiz', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['price'] = ['Preis', 'Geben Sie hier den Preis an.'];
$GLOBALS['TL_LANG']['tl_iao_agreements']['vat_incl'] = ['Preis-Angabe mit oder ohne MwSt.', '(brutto / netto)'];
$GLOBALS['TL_LANG']['tl_iao_agreements']['count'] = ['Anzahl', 'Die Anzahl wird mit dem Preis multipliziert.'];
$GLOBALS['TL_LANG']['tl_iao_agreements']['amountStr'] = ['Art der Anzahl', ''];
$GLOBALS['TL_LANG']['tl_iao_agreements']['vat'] = ['MwSt.', 'Art der MwSt. zu diesem Posten.'];
$GLOBALS['TL_LANG']['tl_iao_agreements']['execute_date'] = [
    'Ausgeführt am',
    'Dieses Angabe wird vom Finanzamt vorgeschrieben um die Vorsteuer zu ziehen.',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['expiry_date'] = [
    'Begleichen bis',
    'Das Datum nachdem die Mahnungsstufen anfangen.',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['invoice_pdf_file'] = [
    'Mahnungsdatei',
    'Wenn hier eine Datei angegeben wurde wird diese statt einer generierten ausgegeben. 
    Unter normalen Umständen sollte dieses Feld leer bleiben. Es ist hauptsächlich für Importe gedacht.',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['paid_on_date'] = [
    'Bezahlt am',
    'Das Datum an dem die Zahlung auf dem Konto eingegangen ist.',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['invoice'] = [
    'Für den Servicevertrag die aktuelle Rechnung generieren.',
    'Servicevertrag ID %s als Rechnung anlegen',
];
$GLOBALS['TL_LANG']['tl_iao_agreements']['toggle'] = 'Vertrag als aktiv/ nicht aktiv markieren';

/*
* Buttons
*/
$GLOBALS['TL_LANG']['tl_iao_agreements']['new'] = ['Neuer Vertrag', 'Einen neuen Vertrag anlegen'];
$GLOBALS['TL_LANG']['tl_iao_agreements']['edit'] = ['Vertrag bearbeiten', 'Vertrag ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_iao_agreements']['copy'] = ['Vertrag duplizieren', 'Vertrag ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_iao_agreements']['delete'] = ['Vertrag löschen', 'Vertrag ID %s löschen'];
$GLOBALS['TL_LANG']['tl_iao_agreements']['deleteConfirm'] = 'Soll die Vertrag ID %s wirklich gelöscht werden?!';
$GLOBALS['TL_LANG']['tl_iao_agreements']['show'] = ['Details anzeigen', 'Details der Vertrag ID %s anzeigen'];

/*
 * Legend
 */
$GLOBALS['TL_LANG']['tl_iao_agreements']['invoice_generate_legend'] = 'Einstellungen für die Genrierung der Rechnungen';
$GLOBALS['TL_LANG']['tl_iao_agreements']['agreement_legend'] = 'Vertrag-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_agreements']['settings_legend'] = 'Konfiguration-Zuweisung';
$GLOBALS['TL_LANG']['tl_iao_agreements']['title_legend'] = 'Titel Einstellung';
$GLOBALS['TL_LANG']['tl_iao_agreements']['address_legend'] = 'Adress-Angaben';
$GLOBALS['TL_LANG']['tl_iao_agreements']['status_legend'] = 'Status-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_agreements']['email_legend'] = 'Email-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_agreements']['other_legend'] = 'weitere Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_agreements']['notice_legend'] = 'Notiz anlegen';

/*
 * Select-fiels options
 */
$GLOBALS['TL_LANG']['tl_iao_agreements']['status_options'] = ['1' => 'aktiv', '2' => 'gekündigt'];
$GLOBALS['TL_LANG']['tl_iao_agreements']['vat_incl_percents'] = [1 => 'netto', 2 => 'brutto'];

/*
* Frontend-Templates
*/
$GLOBALS['TL_LANG']['tl_iao_agreements']['fe_table_head']['title'] = 'Titel:';
$GLOBALS['TL_LANG']['tl_iao_agreements']['fe_table_head']['beginn_date'] = 'Zyklusbeginn:';
$GLOBALS['TL_LANG']['tl_iao_agreements']['fe_table_head']['end_date'] = 'Zyklusende:';
$GLOBALS['TL_LANG']['tl_iao_agreements']['fe_table_head']['price'] = 'Betrag:';
$GLOBALS['TL_LANG']['tl_iao_agreements']['fe_table_head']['file'] = 'Vertrag (pdf):';

// Meldungen
$GLOBALS['TL_LANG']['tl_iao_agreements']['no_entries_msg'] = 'Es sind keine Einträge für diesen Bereich vorhanden.';
