<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

$GLOBALS['TL_LANG']['tl_iao_credit']['setting_id'] = ['Konfiguration', ''];
$GLOBALS['TL_LANG']['tl_iao_credit']['pid'] = ['Projekt', 'das entsprechende Projekt auswählen.'];
$GLOBALS['TL_LANG']['tl_iao_credit']['title'] = ['Bezeichnung', 'Bezeichnung des Elementes'];
$GLOBALS['TL_LANG']['tl_iao_credit']['alias'] = ['Alias', ''];

$GLOBALS['TL_LANG']['tl_iao_credit']['member'] = [
    'Kunde',
    'Adresse aus gespeicherten Kunden in nachstehendes Feld befüllen',
];
$GLOBALS['TL_LANG']['tl_iao_credit']['text_generate'] = [
    'Adresstext aus Kundendaten generieren',
    'VORSICHT! Der evtl. bestehende Adresstext wird gelöscht.',
];
$GLOBALS['TL_LANG']['tl_iao_credit']['address_text'] = [
    'Gutschrift-Adresse',
    'Adresse die in der Gutschrift-PDF-Datei geschrieben wird.',
];
$GLOBALS['TL_LANG']['tl_iao_credit']['before_text'] = ['Text vor den Posten', ''];
$GLOBALS['TL_LANG']['tl_iao_credit']['before_template'] = ['Text-Template vor den Posten', ''];
$GLOBALS['TL_LANG']['tl_iao_credit']['after_text'] = ['Text nach den Posten', ''];
$GLOBALS['TL_LANG']['tl_iao_credit']['after_template'] = ['Text-Template nach den Posten', ''];
$GLOBALS['TL_LANG']['tl_iao_credit']['published'] = ['veröffentlicht', ''];
$GLOBALS['TL_LANG']['tl_iao_credit']['status'] = ['Status dieser Gutschrift', ''];
$GLOBALS['TL_LANG']['tl_iao_credit']['noVat'] = [
    'keine MwSt. ausweisen',
    'z.B. Gutschrift in nicht Bundesrepublik Deutschland',
];
$GLOBALS['TL_LANG']['tl_iao_credit']['price_netto'] = ['Gutschrift-Höhe (Netto)', ''];
$GLOBALS['TL_LANG']['tl_iao_credit']['price_brutto'] = ['Gutschrift-Höhe (Brutto)', ''];
$GLOBALS['TL_LANG']['tl_iao_credit']['member'] = ['Kunde', ''];
$GLOBALS['TL_LANG']['tl_iao_credit']['notice'] = ['Notiz', 'Diese Notizen werden ausschließlich im Backend verwendet.'];
$GLOBALS['TL_LANG']['tl_iao_credit']['credit_date'] = [
    'Gutschriftdatum',
    'wenn es leer bleibt dann wird das aktuelle Datum eingetragen. 
    Unter normalen Umständen sollte dieses Feld leer bleiben. Es ist hauptsächlich für Importe gedacht.',
];
$GLOBALS['TL_LANG']['tl_iao_credit']['credit_tstamp'] = [
    'Gutschriftdatum als Timestamp',
    'Wenn es leer bleibt dann wird der Timestamp vom Gutschriftdatum eingetragen. 
    Unter normalen Umständen sollte dieses Feld leer bleiben. Es ist hauptsächlich für Importe gedacht.',
];
$GLOBALS['TL_LANG']['tl_iao_credit']['credit_id'] = [
    'Gutschrift-ID',
    'Dieses Feld wird hauptsächlich zum hochzählen der nächsten Gutschrift benötigt.',
];
$GLOBALS['TL_LANG']['tl_iao_credit']['credit_id_str'] = [
    'Gutschrift-ID-Name',
    'Dieses Feld wird für den PDF-Namen und direkt auf der Gutschrift ausgegeben.',
];
$GLOBALS['TL_LANG']['tl_iao_credit']['expiry_date'] = ['Gültig bis', 'Diese Gutschrift ist bis zu diesem Datum gültig.'];
$GLOBALS['TL_LANG']['tl_iao_credit']['credit_pdf_file'] = [
    'Gutschriftdatei',
    'Wenn hier eine Datei angegeben wurde wird diese statt einer generierten ausgegeben. 
    Unter normalen Umständen sollte dieses Feld leer bleiben. Es ist hauptsächlich für Importe gedacht.',
];
$GLOBALS['TL_LANG']['tl_iao_credit']['toggle'] = 'Gutschrift als akzeptiert/ nicht akzeptiert markieren';
$GLOBALS['TL_LANG']['tl_iao_credit']['gender']['male'] = 'Herr';
$GLOBALS['TL_LANG']['tl_iao_credit']['gender']['female'] = 'Frau';

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_iao_credit']['new'] = ['Neue Gutschrift', 'Eine neue Gutschrift anlegen'];
$GLOBALS['TL_LANG']['tl_iao_credit']['edit'] = ['Gutschrift bearbeiten', 'Gutschrift ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_iao_credit']['copy'] = ['Gutschrift duplizieren', 'Gutschrift ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_iao_credit']['delete'] = ['Gutschrift löschen', 'Gutschrift ID %s löschen'];
$GLOBALS['TL_LANG']['tl_iao_credit']['deleteConfirm'] = 'Soll die Gutschrift ID %s wirklich gelöscht werden?!';
$GLOBALS['TL_LANG']['tl_iao_credit']['show'] = ['Details anzeigen', 'Details der Gutschrift ID %s anzeigen'];
$GLOBALS['TL_LANG']['tl_iao_credit']['pdf'] = ['PDF generieren', 'eine PDF zu dieser Gutschrift generieren'];

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_iao_credit']['settings_legend'] = 'Konfiguration-Zuweisung';
$GLOBALS['TL_LANG']['tl_iao_credit']['credit_id_legend'] = 'Gutschrift-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_credit']['address_legend'] = 'Adress-Angaben';
$GLOBALS['TL_LANG']['tl_iao_credit']['text_legend'] = 'Gutschrift-Texte';
$GLOBALS['TL_LANG']['tl_iao_credit']['extend_legend'] = 'weitere Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_credit']['status_legend'] = 'Status-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_credit']['notice_legend'] = 'Notiz anlegen';

/*
* Frontend-Templates
*/
$GLOBALS['TL_LANG']['tl_iao_credit']['fe_table_head']['title'] = 'Titel/ Rechnungsnr.:';
$GLOBALS['TL_LANG']['tl_iao_credit']['fe_table_head']['date'] = 'erstellt am:';
$GLOBALS['TL_LANG']['tl_iao_credit']['fe_table_head']['price'] = 'Betrag:';
$GLOBALS['TL_LANG']['tl_iao_credit']['fe_table_head']['remaining'] = 'offen:';
$GLOBALS['TL_LANG']['tl_iao_credit']['fe_table_head']['file'] = 'PDF:';

// Meldungen
$GLOBALS['TL_LANG']['tl_iao_credit']['no_entries_msg'] = 'Es sind keine Einträge für diesen Bereich vorhanden.';
