<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_module']['fe_iao_template'][0] = 'Template';
$GLOBALS['TL_LANG']['tl_module']['fe_iao_template'][1] = '';
$GLOBALS['TL_LANG']['tl_module']['fe_iao_numberOfItems'][0] = 'Anzahl der Einträge';
$GLOBALS['TL_LANG']['tl_module']['fe_iao_numberOfItems'][1] = '0 = alle Einträge';
$GLOBALS['TL_LANG']['tl_module']['status'][0] = 'Status der angezeigten Einträge';
$GLOBALS['TL_LANG']['tl_module']['status'][1] = 'Wenn alle angezeigt werden soll dann keinen "-" Status zuweisen.';
