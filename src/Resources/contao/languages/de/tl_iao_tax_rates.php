<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['name'][0] = 'Name des Steuersatzes';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['name'][1] = 'Geben Sie einen eindeutigen Namen ein.';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['value'][0] = 'Wert (%)';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['value'][1] = 'z.B. 19 pder 7 oder 0';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['sorting'][0] = 'Sortierung';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['sorting'][1] = 'Um die Sortierung anzupassen, tragen Sie hier eine Zahl ein.';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['default_value'] = [
    'Standart-Wert',
    'Dieser Wert wird beim neuanlegen als Vorauswahl angezeigt.',
];

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['new'][0] = 'Neuer Steuersatz';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['new'][1] = 'Ein neuer Steuersatz erstellen.';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['edit'][0] = 'Steuersatz bearbeiten';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['edit'][1] = 'Steuersatz ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['copy'][0] = 'Steuersatz duplizieren';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['copy'][1] = 'Steuersatz ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['delete'][0] = 'Steuersatz löschen';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['delete'][1] = 'Steuersatz ID %s löschen.';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['show'][0] = 'Steuersatzsdetails anzeigen';
$GLOBALS['TL_LANG']['tl_iao_tax_rates']['show'][1] = 'Details für Steuersatz ID %s anzeigen.';
