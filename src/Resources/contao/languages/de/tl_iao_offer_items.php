<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

$GLOBALS['TL_LANG']['tl_iao_offer_items']['posten_template'] = ['Template', 'Posten-Template'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['headline'] = ['Bezeichnung', 'Posten-Bezeichnung'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['type'] = ['Inhaltstyp', 'neuer Eintrag oder PDF-Seitentrenner'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['headline_to_pdf'] = [
    'Bezeichnung in PDF aufnhemen',
    'wenn die Bezeichnung in der PDF-Datei vor dem Text aufgenommen werden soll.',
];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['alias'] = ['Alias', ''];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['author'] = ['Posten-Ersteller', ''];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['text'] = [
    'Beschreibung',
    'hier wird die Beschreibung zu dem Posten eingegeben.',
];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['price'] = ['Preis', 'geben Sie hier den Preis an.'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['vat_incl'] = ['Preis-Angabe mit oder ohne MwSt.', '(brutto / netto)'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['count'] = ['Anzahl', 'die Anzahl wird mit dem Preis multipliziert'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['amountStr'] = ['Art der Anzahl', ''];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['vat'] = ['MwSt.', 'Art der MwSt. zu diesem Posten.'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['operator'] = [
    'Zahlungsart',
    'soll dieser Posten hinzugefügt oder abgezogen werden?',
];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['pagebreak_after'] = [
    'Seitenumbruch nach diesem Posten',
    'Seitenumbruch nach diesem Posten erzwingen.',
];

/*
 * Global Operation
 */
$GLOBALS['TL_LANG']['tl_iao_offer_items']['new'] = ['Neuer Posten', 'Ein neuen Posten anlegen'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['pdf'] = ['PDF generieren', 'eine PDF zu dieses Angebotes generieren'];

/*
 * Operations
 */
$GLOBALS['TL_LANG']['tl_iao_offer_items']['edit'] = ['Posten bearbeiten', 'Element ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['copy'] = ['Posten duplizieren', 'Element ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['delete'] = ['Posten löschen', 'Element ID %s löschen'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['deleteConfirm'] = 'Soll der Posten ID %s wirklich gelöscht werden?!';
$GLOBALS['TL_LANG']['tl_iao_offer_items']['show'] = ['Details anzeigen', 'Details des Postens ID %s anzeigen'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['postentemplate'] = [
    'diesen Posten als Vorlage speichern',
    'Posten ID %s als Vorlage speichern',
];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['tstamp'] = ['Letzte Änderung', ''];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['published'] = ['veröffentlicht', ''];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['templates_legend'] = 'Template-Auswahl';
$GLOBALS['TL_LANG']['tl_iao_offer_items']['title_legend'] = 'Grundeinstellungen';
$GLOBALS['TL_LANG']['tl_iao_offer_items']['item_legend'] = 'Posten-Daten';
$GLOBALS['TL_LANG']['tl_iao_offer_items']['publish_legend'] = 'Veröffentlichung';
$GLOBALS['TL_LANG']['tl_iao_offer_items']['type_legend'] = 'Elementtyp-Einstellung';
$GLOBALS['TL_LANG']['tl_iao_offer_items']['vat_incl_percents'] = [1 => 'netto', 2 => 'brutto'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['operators'] = ['+' => '+ (addieren)', '-' => '- (subtrahieren)'];
$GLOBALS['TL_LANG']['tl_iao_offer_items']['type_options'] = ['item' => 'Eintrag', 'devider' => 'PDF-Trenner'];
