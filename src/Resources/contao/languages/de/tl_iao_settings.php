<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_iao_settings']['name'] = [
    'Name der Konfiguration',
    'Geben Sie einen eindeutigen Namen ein.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['fallback'] = [
    'Standard-Konfiguration',
    'Verwendet dies als Standardkonfiguration für die Anzeige im Backend.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_tax'] = [
    'Standard-Steuersatz',
    'Wird für die Berechnung der Standardwerte benötigt. Wirkt sich auch auf Verpackungskosten etc aus.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_costumer_group'] = [
    'Kunden-Gruppe',
    'Diese Gruppen werden automatisch bei fehlender Angabe der Firma gesetzt.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_currency'] = [
    'Währungkürzel',
    'Bitte das offizielle Währungskennzeichen dieses Shops eingeben.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_currencysymbol'] = [
    'Währungssymbol',
    'Bitte das Währungssymbol der Shopwährung.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_pdf_margins'] = [
    'Randabstände',
    'Geben Sie hier die Abstände in folgender Reihenfolge ein. (oben,rechts,unten,links, Maßeinheit)',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_pdf_css'] = ['CSS-Datei für die PDF-Darstellung', ''];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_invoice_mail_from'] = [
    'Rechnung-Absender-Mailadresse',
    'in der form foo@bar.de',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_invoice_startnumber'] = [
    'Rechnung-Startnummer',
    'ab welcher Nummer soll hochgezählt werden.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_invoice_pdf'] = [
    'Rechnung-PDF-Vorlage',
    'eine PDF-Vorlage sollte 2 Seiten beinhalten die erste für die Startseite und die zweite für alle weiteren.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_invoice_number_format'] = [
    'Rechnung-Nummer-Formatierung',
    'die Platzhalter  {date} für aktuelles Datum  und {nr} für die Rechnungs-ID können angegeben werden.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_invoice_duration'] = ['Begleichungsdauer', 'in Tagen'];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_offer_mail_from'] = [
    'Angebot-Absender-Mailadresse',
    'in der form foo@bar.de',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_offer_startnumber'] = [
    'Angebot-Startnummer',
    'ab welcher Nummer soll hochgezählt werden.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_offer_pdf'] = [
    'Angebot-PDF-Vorlage',
    'eine PDF-Vorlage sollte 2 Seiten beinhalten die erste für die Startseite und die zweite für alle weiteren.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_offer_number_format'] = [
    'Angebot-Nummer-Formatierung',
    'die Platzhalter  {date} für aktuelles Datum  und {nr} für die Angebot-ID können angegeben werden.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_offer_expiry_date'] = [
    'Angebot gültig bis',
    'die Platzhalter  im Format d+n:m-n:Y+n für ein viertel Jahr z.B. d:m+3:Y.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_credit_mail_from'] = [
    'Gutschrift-Absender-Mailadresse',
    'in der form foo@bar.de',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_credit_startnumber'] = [
    'Gutschrift-Startnummer',
    'ab welcher Nummer soll hochgezählt werden.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_credit_pdf'] = [
    'Gutschrift-PDF-Vorlage',
    'eine PDF-Vorlage sollte 2 Seiten beinhalten die erste für die Startseite und die zweite für alle weiteren.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_credit_number_format'] = [
    'Gutschrift-Nummer-Formatierung',
    'die Platzhalter  {date} für aktuelles Datum  und {nr} für die Gutschrift-ID können angegeben werden.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_credit_expiry_date'] = [
    'Gutschrift gültig bis',
    'die Platzhalter  im Format d+n:m-n:Y+n für ein viertel Jahr z.B. d:m+3:Y.',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_tax'] = ['Mahnungs-Zins (%)', ''];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_postage'] = [
    'Portokosten (&euro;)',
    'mit Punkt getrennt (englisch)',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_1_duration'] = ['Erinnerungs-Zeitraum', 'Status 4'];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_1_text'] = [
    'Erinnerungs-Text',
    'Platzhalter: alle Contao-Inserttags http://de.contaowiki.org/Insert-Tags 
    , Rechnungsdatum = {{invoice::date}} , Rechnungsnummer = {{invoice::invoice_id_str}}
    , Frist-Datum = {{iao::period-date}}',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_1_pdf'] = ['Erinnerungs-PDF-Vorlage', ''];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_2_duration'] = ['1. Mahnung-Zeitraum', ''];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_2_text'] = [
    '1. Mahnung-Text',
    'Platzhalter: alle Contao-Inserttags http://de.contaowiki.org/Insert-Tags 
    , Rechnungsdatum = {{invoice::date}}
    , Rechnungsnummer = {{invoice::invoice_id_str}}, Frist-Datum = {{iao::period-date}}',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_2_pdf'] = ['1. Mahnung-PDF-Vorlage', ''];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_3_duration'] = ['2. Mahnung-Zeitraum', ''];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_3_text'] = [
    '2. Mahnung-Text',
    'Platzhalter: alle Contao-Inserttags http://de.contaowiki.org/Insert-Tags
    , Rechnungsdatum = {{invoice::date}}
    , Rechnungsnummer = {{invoice::invoice_id_str}}
    , Frist-Datum = {{iao::period-date}}',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_3_pdf'] = ['2. Mahnung-PDF-Vorlage', ''];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_4_duration'] = ['3. Mahnung-Zeitraum', ''];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_4_text'] = [
    '3. Mahnung-Text',
    'Platzhalter: alle Contao-Inserttags http://de.contaowiki.org/Insert-Tags
    , Rechnungsdatum = {{invoice::date}}
    , Rechnungsnummer = {{invoice::invoice_id_str}}
    , Frist-Datum = {{iao::period-date}}',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_reminder_4_pdf'] = ['3. Mahnung-PDF-Vorlage', ''];

$GLOBALS['TL_LANG']['tl_iao_settings']['iao_agreements_remind_before'] = [
    'erinnern vor Ablauf des Vertrags-Zyklus', 'Die Angaben im strtotime-Format z.B. -3 weeks',
];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_agreements_mail_from'] = ['Email-Sender', ''];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_agreements_mail_to'] = ['Email-Empfänger', ''];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_agreements_mail_subject'] = ['Email-Betreff', ''];
$GLOBALS['TL_LANG']['tl_iao_settings']['iao_agreements_mail_text'] = ['Email-Text', ''];

$GLOBALS['TL_LANG']['tl_iao_settings']['remind_before_default'] = '-3 weeks';
$GLOBALS['TL_LANG']['tl_iao_settings']['email_subject_default'] = 'Servicevertrag von ##iao::project::name##';
$GLOBALS['TL_LANG']['tl_iao_settings']['email_text_default'] = 'Der jährliche Vertrag läuft 
am ##iao::project::end_date## aus';

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_iao_settings']['new'][0] = 'Neue Konfiguration';
$GLOBALS['TL_LANG']['tl_iao_settings']['new'][1] = 'Eine neue Konfiguration erstellen.';
$GLOBALS['TL_LANG']['tl_iao_settings']['edit'][0] = 'Konfiguration bearbeiten';
$GLOBALS['TL_LANG']['tl_iao_settings']['edit'][1] = 'Konfiguration ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_iao_settings']['copy'][0] = 'Konfiguration duplizieren';
$GLOBALS['TL_LANG']['tl_iao_settings']['copy'][1] = 'Konfiguration ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_iao_settings']['delete'][0] = 'Konfiguration löschen';
$GLOBALS['TL_LANG']['tl_iao_settings']['delete'][1] = 'Konfiguration ID %s löschen.  
    Dies löscht nicht die zugeordneten Dateien sondern lediglich die Grundkonfiguration.';
$GLOBALS['TL_LANG']['tl_iao_settings']['show'][0] = 'Konfigurationsdetails anzeigen';
$GLOBALS['TL_LANG']['tl_iao_settings']['show'][1] = 'Details für Konfiguration ID %s anzeigen.';

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_iao_settings']['name_legend'] = 'Name';
$GLOBALS['TL_LANG']['tl_iao_settings']['currency_legend'] = 'Währungs-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_settings']['standards_legend'] = 'Standard-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_settings']['customermails_legend'] = 'Kunden-Maileinstellungen';
$GLOBALS['TL_LANG']['tl_iao_settings']['secure_legend'] = 'Sicherheitseinstellungen';
$GLOBALS['TL_LANG']['tl_iao_settings']['pdf_legend'] = 'PDF-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_settings']['invoice_legend'] = 'Rechnung-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_settings']['offer_legend'] = 'Angebot-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_settings']['credit_legend'] = 'Gutschrift-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_settings']['agreements_legend'] = 'Vertrags-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_settings']['reminder_legend'] = 'Mahnung-Einstellungen';
