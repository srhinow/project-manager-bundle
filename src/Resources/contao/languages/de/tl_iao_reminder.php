<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

$GLOBALS['TL_LANG']['tl_iao_reminder']['title'] = ['Bezeichnung', 'Bezeichnung des Elementes'];
$GLOBALS['TL_LANG']['tl_iao_reminder']['pid'] = ['Projekt', 'das entsprechende Projekt auswählen.'];
$GLOBALS['TL_LANG']['tl_iao_reminder']['setting_id'] = ['zugehörige Einstellungen', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['alias'] = ['Alias', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['member'] = [
    'Kunde',
    'Adresse aus gespeicherten Kunden in nachstehendes Feld befüllen',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['text_generate'] = [
    'Adresstext aus Kundendaten generieren',
    'VORSICHT! Der evtl. bestehende Adresstext wird gelöscht.',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['address_text'] = [
    'Mahnungs-Adresse',
    'Adresse die in der Mahnungs-PDF-Datei geschrieben wird.',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['text'] = ['Mahnungs-Text', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['text_finish'] = ['Mahnungs-Text (Vorschau)', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['reminder_tstamp'] = ['Datum der Erinnerung/Mahnung', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['published'] = ['veröffentlicht', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['status'] = ['Status dieser Mahnung', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['new'] = ['Neue Erinnerung', 'Eine neue Erinnerung anlegen'];
$GLOBALS['TL_LANG']['tl_iao_reminder']['checkReminder'] = [
    'Erinnnerungen aktualisieren',
    'Nach neuen Erinnerungen durchsuchen und anlegen.',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['edit'] = ['Mahnung bearbeiten', 'Mahnung ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_iao_reminder']['copy'] = ['Mahnung duplizieren', 'Mahnung ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_iao_reminder']['delete'] = ['Mahnung löschen', 'Mahnung ID %s löschen'];
$GLOBALS['TL_LANG']['tl_iao_reminder']['deleteConfirm'] = 'Soll die Mahnung ID %s wirklich gelöscht werden?!';
$GLOBALS['TL_LANG']['tl_iao_reminder']['show'] = ['Details anzeigen', 'Details der Mahnung ID %s anzeigen'];
$GLOBALS['TL_LANG']['tl_iao_reminder']['invoice_id'] = [
    'Rechnung',
    'wählen Sie hier die Rechnung aus welche im Verzug ist.',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['sum'] = ['Mahnung-Höhe (&euro;)', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['member'] = ['Kunde', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['periode_date'] = ['zu zahlen bis', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['notice'] = ['Notiz', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['step'] = ['Mahnungs-Schritt', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['set_step_values'] = [
    'Werte setzen',
    'Zu dem links ausgewählten Mahnungsschritt werden alle zugehörigen Felder befüllt.',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['unpaid'] = [
    'Schuld (&euro;)',
    'Die Eingabe muss mit dem Dezimal-Trennzeichen . angelegt werden.',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['tax'] = ['Zins', 'optional'];
$GLOBALS['TL_LANG']['tl_iao_reminder']['postage'] = ['Versand/Porto (&euro;)', 'optional'];
$GLOBALS['TL_LANG']['tl_iao_reminder']['reminder_id_str'] = [
    'Mahnungs-ID-Name',
    'Dieses Feld wird für den PDF-Namen und direkt auf der Mahnung ausgegeben.',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['execute_date'] = [
    'Ausgeführt am',
    'Dieses Angabe wird vom Finanzamt vorgeschrieben um die Vorsteuer zu ziehen.',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['expiry_date'] = [
    'begleichen bis',
    'Das Datum nachdem die Mahnungsstufen anfangen.',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['invoice_pdf_file'] = [
    'Mahnungsdatei',
    'Wenn hier eine Datei angegeben wurde wird diese statt einer generierten ausgegeben. 
    Unter normalen Umständen sollte dieses Feld leer bleiben. Es ist hauptsächlich für Importe gedacht.',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['paid_on_date'] = [
    'Bezahlt am',
    'Das Datum an dem die Zahlung auf dem Konto eingegangen ist.',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['before_template'] = ['Text-Template vor den Posten', ''];
$GLOBALS['TL_LANG']['tl_iao_reminder']['after_template'] = ['Text-Template nach den Posten', ''];

$GLOBALS['TL_LANG']['tl_iao_reminder']['toggle'] = 'Mahnung als bezahlt/ nicht bezahlt markieren';
$GLOBALS['TL_LANG']['tl_iao_reminder']['gender']['male'] = 'Herr';
$GLOBALS['TL_LANG']['tl_iao_reminder']['gender']['female'] = 'Frau';
$GLOBALS['TL_LANG']['tl_iao_reminder']['Reminder_is_checked'] = 'Die Erinnerungen wurden erfolgreich aktualisiert.';
$GLOBALS['TL_LANG']['tl_iao_reminder']['to_much_steps'] = 'Es gibt keine weitere Mahnstufe für die Rechnung ';

/*
 * Legend
 */
$GLOBALS['TL_LANG']['tl_iao_reminder']['settings_legend'] = 'Haupteinstellungen';
$GLOBALS['TL_LANG']['tl_iao_reminder']['invoice_legend'] = 'erweiterte Mahnungs-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_reminder']['address_legend'] = 'Adress-Angaben';
$GLOBALS['TL_LANG']['tl_iao_reminder']['text_legend'] = 'Mahnungs-Texte';
$GLOBALS['TL_LANG']['tl_iao_reminder']['status_legend'] = 'Status-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_reminder']['notice_legend'] = 'Notiz anlegen';

/*
 * Select-fiels options
 */
$GLOBALS['TL_LANG']['tl_iao_reminder']['steps'] = [
    '1' => 'Erinnerung',
    '2' => '1. Mahnung',
    '3' => '2. Mahnung',
    '4' => '3. Mahnung',
];
$GLOBALS['TL_LANG']['tl_iao_reminder']['status_options'] = [
    '1' => 'nicht bezahlt',
    '2' => 'bezahlt',
    '3' => 'ruht (keine Mahnungen)',
];

/*
* Frontend-Templates
*/
$GLOBALS['TL_LANG']['tl_iao_reminder']['fe_table_head']['step'] = 'Stufe:';
$GLOBALS['TL_LANG']['tl_iao_reminder']['fe_table_head']['title'] = 'Rechnungsnr.:';
$GLOBALS['TL_LANG']['tl_iao_reminder']['fe_table_head']['date'] = 'erstellt am:';
$GLOBALS['TL_LANG']['tl_iao_reminder']['fe_table_head']['invoice_expiry'] = 'ausstehend seid:';
$GLOBALS['TL_LANG']['tl_iao_reminder']['fe_table_head']['reminder_expiry'] = 'begleichen bis:';
$GLOBALS['TL_LANG']['tl_iao_reminder']['fe_table_head']['price'] = 'Betrag:';
$GLOBALS['TL_LANG']['tl_iao_reminder']['fe_table_head']['expiry'] = 'offen:';
$GLOBALS['TL_LANG']['tl_iao_reminder']['fe_table_head']['file'] = 'PDF:';

// Meldungen
$GLOBALS['TL_LANG']['tl_iao_reminder']['no_entries_msg'] = 'Es sind keine Einträge für diesen Bereich vorhanden.';
