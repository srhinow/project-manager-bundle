<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

$GLOBALS['TL_LANG']['tl_iao_templates']['title'] = ['Bezeichnung', 'Bezeichnung der Vorlage'];
$GLOBALS['TL_LANG']['tl_iao_templates']['text'] = ['Vorlagen-Text', ''];
$GLOBALS['TL_LANG']['tl_iao_templates']['position'] = ['Position', ''];
$GLOBALS['TL_LANG']['tl_iao_templates']['new'] = ['Neue Vorlage', 'Eine neue Vorlage anlegen'];
$GLOBALS['TL_LANG']['tl_iao_templates']['edit'] = ['Vorlage bearbeiten', 'Vorlage ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_iao_templates']['copy'] = ['Vorlage duplizieren', 'Vorlage ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_iao_templates']['delete'] = ['Vorlage löschen', 'Vorlage ID %s löschen'];

$GLOBALS['TL_LANG']['tl_iao_templates']['template_options'] = [
    'invoice_before_text' => 'Rechnung Text vor Positionen',
    'invoice_after_text' => 'Rechnung Text nach Positionen',
    'offer_before_text' => 'Angebot Text vor Positionen',
    'offer_after_text' => 'Angebot Text nach Positionen',
    'credit_before_text' => 'Gutschrift Text vor Positionen',
    'credit_after_text' => 'Gutschrift Text nach Positionen',
];
