<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

$GLOBALS['TL_LANG']['tl_iao']['types']['offer'] = 'Angebot';
$GLOBALS['TL_LANG']['tl_iao']['types']['invoice'] = 'Rechnung';
$GLOBALS['TL_LANG']['tl_iao']['types']['credit'] = 'Gutschrift';

$GLOBALS['TL_LANG']['tl_iao']['active'] = ['aktiv', ''];
$GLOBALS['TL_LANG']['tl_iao']['status']['title'] = ['Status', ''];
$GLOBALS['TL_LANG']['tl_iao']['counter'] = ['Counter', ''];

$GLOBALS['TL_LANG']['tl_iao']['credit_status'][0] = ['nicht angenommen', ''];
$GLOBALS['TL_LANG']['tl_iao']['credit_status'][1] = ['angenommen', ''];

$GLOBALS['TL_LANG']['tl_iao']['gender']['male'] = 'Herr';
$GLOBALS['TL_LANG']['tl_iao']['gender']['female'] = 'Frau';

$GLOBALS['TL_LANG']['tl_iao']['beforetext_as_template'] = ['Text vor den Posten als Template speichern.'];
$GLOBALS['TL_LANG']['tl_iao']['aftertext_as_template'] = ['Text nach den Posten als Template speichern.'];

$GLOBALS['TL_LANG']['ERR']['PermissionDenied'] = 'keine Berechtigung für %s';

$GLOBALS['TL_LANG']['tl_iao']['vat_incl_percents'] = [1 => 'netto', 2 => 'brutto'];
$GLOBALS['TL_LANG']['tl_iao']['operators'] = ['+' => '+ (addieren)', '-' => '- (subtrahieren)'];
$GLOBALS['TL_LANG']['tl_iao']['type_options'] = ['item' => 'Eintrag', 'devider' => 'PDF-Trenner'];
$GLOBALS['TL_LANG']['tl_iao']['status_options'] = ['1' => 'nicht angenommen', '2' => 'angenommen'];

