<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_member']['myid'] = [
    'Kunden-ID',
    'Falls Datensätze importiert werden, müssen evtl. 
    die Verbindungen zu Rechnnungen, Angebote oder Gutscheinen eine Verknüpfung zum Kundendatensatz haben.',
];

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_member']['import_settings'] = 'Import-Einstellungen';
$GLOBALS['TL_LANG']['tl_member']['new'] = ['Neuer Kunde', 'Einen neuen Kunden anlegen'];
$GLOBALS['TL_LANG']['tl_member']['title'] = ['Titel', 'z.B. Dr. oder Prof.'];
$GLOBALS['TL_LANG']['tl_member']['text_generate'] = [
    'Adresstext aus Kundendaten generieren',
    'VORSICHT! Der evtl. bestehende Adresstext wird gelöscht.',
];
$GLOBALS['TL_LANG']['tl_member']['address_text'] = [
    'Anschrift-Adresstext',
    'Adresse die in die PDF-Datei geschrieben wird.',
];
$GLOBALS['TL_LANG']['tl_member']['custom_addresstext_legend'] = 'angepasster Adresstext';
