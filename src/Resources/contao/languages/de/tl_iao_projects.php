<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

$GLOBALS['TL_LANG']['tl_iao_projects']['setting_id'] = ['Konfiguration', ''];
$GLOBALS['TL_LANG']['tl_iao_projects']['title'] = ['Bezeichnung', 'Bezeichnung des Elementes'];
$GLOBALS['TL_LANG']['tl_iao_projects']['member'] = [
    'Kunde',
    'Adresse aus gespeicherten Kunden in nachstehendes Feld befüllen',
];
$GLOBALS['TL_LANG']['tl_iao_projects']['reference_title'] = ['Titel für die Referenzen', ''];
$GLOBALS['TL_LANG']['tl_iao_projects']['reference_customer'] = ['Kunden-Beschreibung', ''];
$GLOBALS['TL_LANG']['tl_iao_projects']['reference_todo'] = ['Aufgaben', ''];
$GLOBALS['TL_LANG']['tl_iao_projects']['reference_desription'] = ['Projekt-Beschreibung', ''];
$GLOBALS['TL_LANG']['tl_iao_projects']['in_reference'] = ['in Referenzen aufnehmen.', ''];
$GLOBALS['TL_LANG']['tl_iao_projects']['finished'] = ['fertig gestellt', ''];
$GLOBALS['TL_LANG']['tl_iao_projects']['singleSRC'] = [
    'Projekt-Bild',
    'wird für alle Projekt-Teaser als Hauptbild verwendet',
];
$GLOBALS['TL_LANG']['tl_iao_projects']['multiSRC'] = ['Projekt-Bild-Galerie', ''];
$GLOBALS['TL_LANG']['tl_iao_projects']['url'] = ['www', 'Website-URL'];
$GLOBALS['TL_LANG']['tl_iao_projects']['notice'] = [
    'Notiz',
    'Diese Notizen werden ausschließlich im Backend verwendet.',
];

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_iao_projects']['offer'] = ['Projekt-Angebote', 'alle Angebote von diesem Projekt'];
$GLOBALS['TL_LANG']['tl_iao_projects']['invoice'] = ['Projekt-Rechnungen', 'alle Rechnungen von diesem Projekt'];
$GLOBALS['TL_LANG']['tl_iao_projects']['credit'] = ['Projekt-Gutschriften', 'alle Gutschriften vone diesem Projekt'];
$GLOBALS['TL_LANG']['tl_iao_projects']['reminder'] = ['Projekt-Erinnerungen', 'alle Erinnerungen von diesem Projekt'];
$GLOBALS['TL_LANG']['tl_iao_projects']['agreements'] = ['Projekt-Verträge', 'alle Verträge von diesem Projekt'];
$GLOBALS['TL_LANG']['tl_iao_projects']['new'] = ['Neues Projekt', 'Ein neues Projekt anlegen'];
$GLOBALS['TL_LANG']['tl_iao_projects']['edit'] = ['Projekt bearbeiten', 'Projekt ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_iao_projects']['copy'] = ['Projekt duplizieren', 'Projekt ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_iao_projects']['delete'] = ['Projekt löschen', 'Projekt ID %s löschen'];
$GLOBALS['TL_LANG']['tl_iao_projects']['deleteConfirm'] = 'Soll das Projekt mit der ID %s wirklich gelöscht werden?!';
$GLOBALS['TL_LANG']['tl_iao_projects']['show'] = ['Details anzeigen', 'Details vom Projekt ID %s anzeigen'];

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_iao_projects']['settings_legend'] = 'Konfiguration-Zuweisung';
$GLOBALS['TL_LANG']['tl_iao_projects']['project_legend'] = 'Projekt-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_projects']['reference_legend'] = 'Referenz-Zuweisung';

/*
* Frontend-Templates
*/
$GLOBALS['TL_LANG']['tl_iao_projects']['fe_table_head']['title'] = 'Titel/ Rechnungsnr.:';
$GLOBALS['TL_LANG']['tl_iao_projects']['fe_table_head']['date'] = 'erstellt am:';
$GLOBALS['TL_LANG']['tl_iao_projects']['fe_table_head']['price'] = 'Betrag:';
$GLOBALS['TL_LANG']['tl_iao_projects']['fe_table_head']['remaining'] = 'offen:';
$GLOBALS['TL_LANG']['tl_iao_projects']['fe_table_head']['file'] = 'PDF:';

// Meldungen
$GLOBALS['TL_LANG']['tl_iao_projects']['no_entries_msg'] = 'Es sind keine Einträge für diesen Bereich vorhanden.';
