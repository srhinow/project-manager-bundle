<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

$GLOBALS['TL_LANG']['tl_iao_credit_items']['posten_template'] = ['Template', 'Posten-Template'];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['headline'] = ['Bezeichnung', 'Posten-Bezeichnung'];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['headline_to_pdf'] = ['Bezeichnung in PDF aufnehmen'];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['type'] = ['Inhaltstyp', 'neuer Eintrag oder PDF-Seitentrenner'];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['alias'] = ['Alias', ''];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['author'] = ['Posten-Ersteller', ''];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['text'] = [
    'Beschreibung',
    'hier wird die Beschreibung zu dem Posten eingegeben.',
];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['price'] = ['Preis', 'geben Sie hier den Preis an.'];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['vat_incl'] = ['Preis-Angabe mit oder ohne MwSt.', '(brutto / netto)'];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['count'] = ['Anzahl', 'die Anzahl wird mit dem Preis multipliziert'];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['amountStr'] = ['Art der Anzahl', ''];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['vat'] = ['MwSt.', 'Art der MwSt. zu diesem Posten.'];

/*
 * Global operation
 */
$GLOBALS['TL_LANG']['tl_iao_credit_items']['pdf'] = ['PDF generieren', 'eine PDF zu dieser Gutschrift generieren'];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['new'] = ['Neuer Posten', 'Ein neuen Posten anlegen'];

/*
 * Operation
 */
$GLOBALS['TL_LANG']['tl_iao_credit_items']['edit'] = ['Posten bearbeiten', 'Element ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['copy'] = ['Posten duplizieren', 'Element ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['delete'] = ['Posten löschen', 'Element ID %s löschen'];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['deleteConfirm'] = 'Soll der Posten ID %s wirklich gelöscht werden?!';
$GLOBALS['TL_LANG']['tl_iao_credit_items']['show'] = ['Details anzeigen', 'Details des Postens ID %s anzeigen'];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['postentemplate'] = [
    'diesen Posten als Vorlage speichern',
    'Posten ID %s als Vorlage speichern',
];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['tstamp'] = ['Letzte Änderung', ''];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['published'] = ['veröffentlicht', ''];
$GLOBALS['TL_LANG']['tl_iao_credit_items']['templates_legend'] = 'Template-Auswahl';
$GLOBALS['TL_LANG']['tl_iao_credit_items']['title_legend'] = 'Grundeinstellungen';
$GLOBALS['TL_LANG']['tl_iao_credit_items']['item_legend'] = 'Posten-Daten';
$GLOBALS['TL_LANG']['tl_iao_credit_items']['publish_legend'] = 'Veröffentlichung';
$GLOBALS['TL_LANG']['tl_iao_credit_items']['type_legend'] = 'Elementtyp-Einstellung';

