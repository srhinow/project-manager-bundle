<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

$GLOBALS['TL_LANG']['tl_iao_templates_items']['title'] = ['Bezeichnung', 'Bezeichnung der Vorlage'];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['text'] = ['Vorlagen-Text', ''];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['position'] = ['Position', ''];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['headline'] = ['Bezeichnung', 'Posten-Bezeichnung'];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['headline_to_pdf'] = [
    'Bezeichnung in PDF aufnhemen',
    'wenn die Bezeichnung in der PDF-Datei vor dem Text aufgenommen werden soll.',
];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['text'] = [
    'Beschreibung',
    'hier wird die Beschreibung zu dem Posten eingegeben.',
];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['price'] = ['Preis', 'geben Sie hier den Preis an.'];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['vat_incl'] = ['Preis-Angabe mit oder ohne MwSt.', '(brutto / netto)'];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['count'] = ['Anzahl', 'die Anzahl wird mit dem Preis multipliziert'];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['amountStr'] = ['Art der Anzahl', ''];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['operator'] = [
    'Zahlungsart',
    'soll dieser Posten hinzugefügt oder abgezogen werden?',
];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['vat'] = ['MwSt.', 'Art der MwSt. zu diesem Posten.'];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['published'] = ['veröffentlicht'];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['new'] = ['Neue Posten-Vorlage', 'Eine neue Vorlage anlegen'];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['edit'] = ['Posten-Vorlage bearbeiten', 'Vorlage ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['copy'] = ['Posten-Vorlage duplizieren', 'Vorlage ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['delete'] = ['posten-Vorlage löschen', 'Vorlage ID %s löschen'];

$GLOBALS['TL_LANG']['tl_iao_templates_items']['title_legend'] = 'Grundeinstellungen';
$GLOBALS['TL_LANG']['tl_iao_templates_items']['item_legend'] = 'Posten-Daten';
$GLOBALS['TL_LANG']['tl_iao_templates_items']['publish_legend'] = 'Veröffentlichung';

$GLOBALS['TL_LANG']['tl_iao_templates_items']['vat_incl_percents'] = [
    1 => 'netto',
    2 => 'brutto',
];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['operators'] = [
    '+' => '+ (addieren)',
    '-' => '- (subtrahieren)',
];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['type_options'] = [
    'item' => 'Eintrag',
    'devider' => 'PDF-Trenner',
];
$GLOBALS['TL_LANG']['tl_iao_templates_items']['position_options'] = [
    'offer' => 'Angebot',
    'credit' => 'Gutschrift',
    'invoice' => 'Rechnung',
];
