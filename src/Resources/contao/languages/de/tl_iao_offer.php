<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

$GLOBALS['TL_LANG']['tl_iao_offer']['setting_id'] = ['Konfiguration', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['pid'] = ['Projekt', 'das entsprechende Projekt auswählen.'];
$GLOBALS['TL_LANG']['tl_iao_offer']['title'] = [
    'Angebot-Titel',
    'Diese Bezeichnung wird außschließlich zur besseren Übersicht im Backend angezeigt.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['alias'] = ['Alias', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['offer_id'] = [
    'Angebots-ID',
    'Dieses Feld wird hauptsächlich zum hochzählen des nächsten Angebots benötigt.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['offer_id_str'] = [
    'Rechnungs-ID-Name',
    'Dieses Feld wird für den PDF-Namen und direkt auf dem Angebot ausgegeben.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['before_text'] = ['Text vor den Posten', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['after_text'] = ['Text nach den Posten', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['published'] = ['veröffentlicht', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['status'] = ['Status des Angebotes', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['price_netto'] = ['Angebot-Höhe (Netto)', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['price_brutto'] = ['Angebot-Höhe (Brutto)', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['member'] = ['Kunde', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['text_generate'] = [
    'Adresstext aus Kundendaten generieren',
    'VORSICHT! Der evtl. bestehende Adresstext wird gelöscht.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['address_text'] = [
    'Angebots-Adresse',
    'Adresse die in der Angebot-PDF-Datei geschrieben wird.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['noVat'] = [
    'keine MwSt. ausweisen',
    'z.B. Rechnung in nicht Bundesrepublik Deutschland',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['offer_date'] = [
    'Angebotsdatum',
    'wenn es leer bleibt dann wird das aktuelle Datum eingetragen. 
    Unter normalen Umständen sollte dieses Feld leer bleiben. Es ist hauptsächlich für Importe gedacht.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['offer_tstamp'] = [
    'Angebotsdatum',
    'Wenn es leer bleibt, dann wird das aktuelle Datum eingetragen.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['offer_id'] = [
    'Angebots-ID',
    'Dieses Feld wird hauptsächlich zum hochzählen des nächsten Angebots benötigt.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['offer_id_str'] = [
    'Angebots-ID-Name',
    'Dieses Feld wird für den PDF-Namen und direkt auf dem Angebot ausgegeben.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['expiry_date'] = [
    'Gültig bis',
    'Dieses Angebot ist bis zu diesem Datum gültig.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['offer_pdf_file'] = [
    'Angebotdatei',
    'Wenn hier eine Datei angegeben wurde wird diese statt einer generierten ausgegeben. 
    Unter normalen Umständen sollte dieses Feld leer bleiben. Es ist hauptsächlich für Importe gedacht.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['before_template'] = ['Text-Template vor den Posten', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['after_template'] = ['Text-Template nach den Posten', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['notice'] = ['Notiz', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['csv_source'] = ['CSV der Angebote', 'z.B. tl_iao_offer_YYYY-MM-DD.csv'];
$GLOBALS['TL_LANG']['tl_iao_offer']['csv_posten_source'] = [
    'CSV der Angebot-Posten',
    'z.B. tl_iao_offer_items_YYYY-MM-DD.csv',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['pdf_import_dir'] = [
    'Verzeichnis der Angebote-PDF-Dateien',
    'Geben Sie hier das Verzeichnis an in dem die Angebote liegen die beim Import verknüpft werden sollen.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['drop_first_row'] = [
    'erste Zeile überspringen',
    'Wenn z.B. die Spaltennamen in der ersten Spalte steht müssen diese beim Import übersprungen werden.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['drop_exist_entries'] = [
    'existierende Einträge in der Datenbank-Tabelle löschen',
    'Alle bereits existierenden Einträge werden vor dem Import entfernt.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['importCSV'] = ['Import starten', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['csv_export_dir'] = [
    'Export-Ziel-Verzeichnis',
    'Wählen Sie das Verzeichnis, in welchem die Dateien exportiert werden sollen. 
    Beachten Sie das es Schreibrechte besitzt.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['export_offer_filename'] = ['Dateiname der Angebote', 'OHNE ENDUNG .csv'];
$GLOBALS['TL_LANG']['tl_iao_offer']['export_offer_item_filename'] = ['Dateiname der Angebotposten', 'OHNE ENDUNG .csv'];
$GLOBALS['TL_LANG']['tl_iao_offer']['exportCSV'] = ['Export starten', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['toggle'] = 'Angebot als angenommen/ nicht angenommen markieren';
$GLOBALS['TL_LANG']['tl_iao_offer']['gender']['male'] = 'Herr';
$GLOBALS['TL_LANG']['tl_iao_offer']['gender']['female'] = 'Frau';

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_iao_offer']['new'] = ['Neues Angebot', 'Ein neues Angebot anlegen'];
$GLOBALS['TL_LANG']['tl_iao_offer']['edit'] = ['Angebot bearbeiten', 'Angebot ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_iao_offer']['copy'] = ['Angebot duplizieren', 'Angebot ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_iao_offer']['delete'] = ['Angebot löschen', 'Angebot ID %s löschen'];
$GLOBALS['TL_LANG']['tl_iao_offer']['deleteConfirm'] = 'Soll das Angebot ID %s wirklich gelöscht werden?!';
$GLOBALS['TL_LANG']['tl_iao_offer']['show'] = ['Details anzeigen', 'Details des Angebots ID %s anzeigen'];
$GLOBALS['TL_LANG']['tl_iao_offer']['invoice'] = [
    'dieses Angebot als Rechnung übernehmen',
    'Angebot ID %s als Rechnung anlegen',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['importOffer'] = ['Import', 'Angebote aus CSV-Dateien importieren'];
$GLOBALS['TL_LANG']['tl_iao_offer']['exportOffer'] = ['Export', 'Angebote und deren Posten in CSV-Dateien exportieren.'];
$GLOBALS['TL_LANG']['tl_iao_offer']['pdf'] = ['PDF generieren', 'eine PDF zu diesem Angebot generieren'];

/*
 * Legend
 */
$GLOBALS['TL_LANG']['tl_iao_offer']['settings_legend'] = 'Konfiguration-Zuweisung';
$GLOBALS['TL_LANG']['tl_iao_offer']['title_legend'] = 'Titel Einstellung';
$GLOBALS['TL_LANG']['tl_iao_offer']['offer_id_legend'] = 'erweiterte Angebots-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_offer']['address_legend'] = 'Adress-Angaben';
$GLOBALS['TL_LANG']['tl_iao_offer']['text_legend'] = 'Angebot-Texte';
$GLOBALS['TL_LANG']['tl_iao_offer']['status_legend'] = 'Status-Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_offer']['extend_legend'] = 'weitere Einstellungen';
$GLOBALS['TL_LANG']['tl_iao_offer']['notice_legend'] = 'Notiz anlegen';

/*
 * Import / Export-Libs
 */
$GLOBALS['TL_LANG']['tl_iao_offer']['export_separator'] = ['Feldtrenner', ''];
$GLOBALS['TL_LANG']['tl_iao_offer']['export_filename'] = [
    'Name der Rechnungen (CSV-Datei)',
    'Bei Bedarf ändern sie den Namen der Export-Datei ohne Endung.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['export_item_filename'] = [
    'Name der Rechnungs-Posten (CSV-Datei)',
    'Bei Bedarf ändern sie den Namen der Export-Datei ohne Endung.',
];
$GLOBALS['TL_LANG']['tl_iao_offer']['importlib_invoiceandoffer'] = 'Invoice and Offer';

/*
 * Notify
 */
$GLOBALS['TL_LANG']['tl_iao_offer']['Offer_imported'] = 'Es wurden die Angebot-Datensätze aus %s erfogreich importiert';
$GLOBALS['TL_LANG']['tl_iao_offer']['Offer_exported'] = 'Es wurden die Angebot-Datensätze erfogreich exportiert';

/*
 * Select-fiels options
 */
$GLOBALS['TL_LANG']['tl_iao_offer']['status_options'] = ['1' => 'nicht angenommen', '2' => 'angenommen'];

/*
* Frontend-Templates
*/
$GLOBALS['TL_LANG']['tl_iao_offer']['fe_table_head']['title'] = 'Titel/ Rechnungsnr.:';
$GLOBALS['TL_LANG']['tl_iao_offer']['fe_table_head']['date'] = 'erstellt am:';
$GLOBALS['TL_LANG']['tl_iao_offer']['fe_table_head']['price'] = 'Betrag:';
$GLOBALS['TL_LANG']['tl_iao_offer']['fe_table_head']['expiry'] = 'offen:';
$GLOBALS['TL_LANG']['tl_iao_offer']['fe_table_head']['file'] = 'PDF:';

// Meldungen
$GLOBALS['TL_LANG']['tl_iao_offer']['no_entries_msg'] = 'Es sind keine Einträge für diesen Bereich vorhanden.';
