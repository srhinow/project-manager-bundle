<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Table tl_iao_invoice_items.
 */
$GLOBALS['TL_DCA']['tl_iao_invoice_items'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ptable' => 'tl_iao_invoice',
        'enableVersioning' => true,
        'oncreate_callback' => [
            ['srhinow.projectmanager.listener.dca.invoice_item', 'setDefaultTaxRate'],
            ['srhinow.projectmanager.listener.dca.invoice_item', 'setDefaultItemUnit'],
        ],
        'onload_callback' => [
            ['srhinow.projectmanager.listener.dca.invoice_item', 'checkPermission'],
        ],
        'onsubmit_callback' => [
            ['srhinow.projectmanager.listener.dca.invoice_item', 'saveNettoAndBrutto'],
            ['srhinow.projectmanager.listener.dca.invoice_item', 'saveAllPricesToParent'],
            ['srhinow.projectmanager.listener.dca.invoice_item', 'updateRemaining'],
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'pid' => 'index',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 4,
            'fields' => ['sorting'],
            'flag' => 1,
            'headerFields' => ['title', 'tstamp', 'price', 'member', 'price_netto', 'price_brutto'],
            'panelLayout' => '',
            'child_record_callback' => ['srhinow.projectmanager.listener.dca.invoice_item', 'listItems'],
        ],
        'label' => [
            'fields' => ['headline'],
            'format' => '%s',
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
            'pdf' => [
                'href' => isset($_GET['id'])?'key=pdf&id='.$_GET['id']:null,
                'class' => 'header_generate_pdf',
                'button_callback' => ['srhinow.projectmanager.listener.dca.invoice_item', 'showPDFButton'],
            ],
        ],
        'operations' => [
            'edit' => [
                'href' => 'act=edit',
                'icon' => 'edit.svg',
            ],
            'copy' => [
                'href' => 'act=paste&amp;mode=copy',
                'icon' => 'copy.svg',
            ],
            'cut' => [
                'href' => 'act=paste&amp;mode=cut',
                'icon' => 'cut.svg',
            ],
            'delete' => [
                'href' => 'act=delete',
                'icon' => 'delete.svg',
                'attributes' => 'onclick="if (!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\')) 
                return false; Backend.getScrollOffset();"',
            ],
            'toggle' => [
                'icon' => 'visible.svg',
                'href' => 'act=toggle&amp;field=published'
            ],
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_iao_invoice_items']['show'],
                'href' => 'act=show',
                'icon' => 'show.svg',
            ],
            'postentemplate' => [
                'label' => &$GLOBALS['TL_LANG']['tl_iao_invoice_items']['postentemplate'],
                'href' => 'key=addPostenTemplate',
                'icon' => 'system/modules/invoice_and_offer/html/icons/posten_templates_16.png',
                'button_callback' => ['srhinow.projectmanager.listener.dca.invoice_item', 'addPostenTemplate'],
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__' => ['type'],
        'default' => '{type_legend},type',
        'item' => '
            {type_legend},type;
            {templates_legend:hide},posten_template;
            {title_legend},headline,headline_to_pdf;
            {item_legend},text,price,vat,count,amountStr,operator,vat_incl;
            {publish_legend},published',
        'devider' => '
            {type_legend},type;
            {publish_legend},published',
    ],
    // Subpalettes
    'subpalettes' => [
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'foreignKey' => 'tl_iao_invoice.title',
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'belongsTo', 'load' => 'eager'],
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'type' => [
            'default' => 'item',
            'exclude' => true,
            'filter' => true,
            'inputType' => 'select',
            'options' => ['item' => 'Eintrag', 'devider' => 'PDF-Trenner'],
            'eval' => ['submitOnChange' => true],
            'sql' => "varchar(32) NOT NULL default 'item'",
        ],
        'headline' => [
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'clr long'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'headline_to_pdf' => [
            'default' => '1',
            'filter' => true,
            'flag' => 2,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'w50'],
            'sql' => "char(1) NOT NULL default '1'",
        ],
        'sorting' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'date' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'time' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'text' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'helpwizard' => true, 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'sql' => 'mediumtext NULL',
        ],
        'count' => [
            'exclude' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'amountStr' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'default' => '',
            'options_callback' => ['srhinow.projectmanager.listener.dca.invoice_item', 'getItemUnitsOptions'],
            'eval' => ['tl_class' => 'w50', 'submitOnChange' => false],
            'sql' => ['type' => 'string', 'length' => 64, 'default' => ''],
        ],
        'operator' => [
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options' => ['+','-'],
            'reference' => &$GLOBALS['TL_LANG']['tl_iao']['operators'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => "char(1) NOT NULL default '+'",
        ],
        'price' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'price_netto' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'price_brutto' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'vat' => [
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.invoice_item', 'getTaxRatesOptions'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => 'float unsigned NOT NULL default 0',
        ],
        'vat_incl' => [
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options' => [1,2],
            'reference' => &$GLOBALS['TL_LANG']['tl_iao']['vat_incl_percents'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '1'",
        ],
        'posten_template' => [
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.invoice_item', 'getPostenTemplate'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange' => true, 'chosen' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice_item', 'fillPostenFields'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'published' => [
            'exclude' => true,
            'toggle'  => true,
            'filter' => true,
            'flag' => 2,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        // -- Backport C2 SQL-Import
        'pagebreak_after' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        //--
    ],
];
