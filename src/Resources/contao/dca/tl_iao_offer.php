<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Table tl_iao_offer.
 */
$GLOBALS['TL_DCA']['tl_iao_offer'] = [
    // Config
    'config' => [
        'dataContainer' => \Contao\DC_Table::class,
        'ptable' => 'tl_iao_projects',
        'ctable' => ['tl_iao_offer_items'],
        'doNotCopyRecords' => true,
        'switchToEdit' => true,
        'enableVersioning' => false,
        'onload_callback' => [
            ['srhinow.projectmanager.listener.dca.offer', 'generateOfferPDF'],
            ['srhinow.projectmanager.listener.dca.offer', 'checkPermission'],
            ['srhinow.projectmanager.listener.dca.offer', 'updateExpiryToTstmp'],
        ],
        'oncreate_callback' => [
            ['srhinow.projectmanager.listener.dca.offer', 'preFillFields'],
            ['srhinow.projectmanager.listener.dca.offer', 'setMemberfieldsFromProject'],
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'pid' => 'index',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 1,
            'fields' => ['offer_tstamp'],
            'flag' => 8,
            'panelLayout' => 'filter;search,limit',
        ],
        'label' => [
            'fields' => ['title', 'offer_id_str'],
            'format' => '%s (%s)',
            'label_callback' => ['srhinow.projectmanager.listener.dca.offer', 'listEntries'],
        ],
        'global_operations' => [
            'importOffer' => [
                'href' => 'key=importOffer',
                'class' => 'global_import',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
            'exportOffer' => [
                'href' => 'key=exportOffer',
                'class' => 'global_export',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
            'all' => [
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'href' => 'table=tl_iao_offer_items',
                'icon' => 'edit.gif',
                'attributes' => 'class="contextmenu"',
            ],
            'editheader' => [
                'href' => 'act=edit',
                'icon' => 'header.gif',
            ],
            'copy' => [
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'delete' => [
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if (!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\')) 
                    return false; Backend.getScrollOffset();"',
            ],
            'show' => [
                'href' => 'act=show',
                'icon' => 'show.gif',
            ],
            'invoice' => [
                'href' => 'key=addInvoice',
                'icon' => 'bundles/srhinowprojectmanager/icons/kontact_todo.png',
                'button_callback' => ['srhinow.projectmanager.listener.dca.offer', 'addInvoiceButton'],
            ],
            'toggle' => [
                'icon' => 'ok.gif',
                'href' => 'act=toggle&amp;field=assumed',
                'button_callback' => ['srhinow.projectmanager.listener.dca.offer', 'toggleAssumedIcon'],
                'showInHeader' => true
            ],
            'pdf' => [
                'label' => &$GLOBALS['TL_LANG']['tl_iao_offer']['pdf'],
                'href' => 'do=iao_offer&key=pdf',
                'icon' => 'iconPDF.gif',
                'button_callback' => ['srhinow.projectmanager.listener.dca.offer', 'showPDFButton'],
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__' => [],
        'default' => '
            {settings_legend},setting_id,pid;
            {title_legend},title;
            {offer_id_legend:hide},offer_id,offer_id_str,offer_tstamp,offer_pdf_file,expiry_date;
            {address_legend},member,text_generate,address_text;
            {text_before_legend},before_template,before_text,beforetext_as_template;
            {text_after_legend},after_template,after_text,aftertext_as_template;
            {status_legend},published,status;{extend_legend},noVat;
            {notice_legend:hide},notice',
    ],

    // Subpalettes
    'subpalettes' => [
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'foreignKey' => 'tl_iao_projects.title',
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => false, 'chosen' => true, 'submitOnChange' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'belongsTo', 'load' => 'eager'],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.offer', 'fillMemberAndAddressFields'],
            ],
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'sorting' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'setting_id' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.offer', 'getSettingOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'title' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'long'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'offer_tstamp' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['rgxp' => 'datim', 'doNotCopy' => true, 'datepicker' => $this->getDatePickerString(), 'tl_class' => 'w50 wizard'],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.offer', 'generateOfferTstamp'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'expiry_date' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['rgxp' => 'date', 'doNotCopy' => true, 'datepicker' => $this->getDatePickerString(), 'tl_class' => 'w50 wizard'],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.offer', 'generateExpiryDate'],
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'offer_id' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['rgxp' => 'alnum', 'doNotCopy' => true, 'spaceToUnderscore' => true, 'maxlength' => 128, 'tl_class' => 'w50'],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.offer', 'setFieldOfferNumber'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'offer_id_str' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['doNotCopy' => true, 'spaceToUnderscore' => true, 'maxlength' => 128, 'tl_class' => 'w50'],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.offer', 'setFieldOfferNumberStr'],
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'offer_pdf_file' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'fileTree',
            'eval' => ['fieldType' => 'radio', 'tl_class' => 'clr', 'extensions' => 'pdf', 'files' => true, 'filesOnly' => true, 'mandatory' => false],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'member' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'search' => true,
            'flag' => 11,
            'inputType' => 'select',
            // 'foreignKey'              => 'tl_member.company',
            'options_callback' => ['srhinow.projectmanager.listener.dca.offer', 'getMemberOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'chosen' => true],
            'sql' => "varbinary(128) NOT NULL default ''",
        ],
        'text_generate' => [
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'clr', 'submitOnChange' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.offer', 'fillAddressText'],
            ],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'address_text' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'mediumtext NULL',
        ],
        'before_template' => [
            'exclude' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.offer', 'getBeforeTemplate'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange' => true, 'chosen' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.offer', 'fillBeforeText'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'before_text' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'helpwizard' => true, 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'text NULL',
        ],
        'beforetext_as_template' => [
            'label' => &$GLOBALS['TL_LANG']['tl_iao']['beforetext_as_template'],
            'flag' => 1,
            'inputType' => 'checkbox',
            'default' => '',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.offer', 'saveBeforeTextAsTemplate'],
            ],
        ],
        'after_template' => [
            'exclude' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.offer', 'getAfterTemplate'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange' => true, 'chosen' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.offer', 'fillAfterText'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'after_text' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'helpwizard' => true, 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'text NULL',
        ],
        'aftertext_as_template' => [
            'label' => &$GLOBALS['TL_LANG']['tl_iao']['aftertext_as_template'],
            'flag' => 1,
            'inputType' => 'checkbox',
            'default' => '',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.offer', 'saveAfterTextAsTemplate'],
            ],
        ],
        'published' => [
            'exclude'                 => true,
            'toggle'                  => true,
            'filter'                  => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('doNotCopy'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        ],
        'assumed' => [
            'exclude'                 => true,
            'filter'                  => true,
            'toggle' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.offer', 'updateAssumed'],
            ],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'status' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options' => ['1','2'],
            'reference' => &$GLOBALS['TL_LANG']['tl_iao']['status_options'],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.offer', 'updateStatus'],
            ],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'noVat' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true, 'tl_class' => 'w50'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'notice' => [
            'exclude' => true,
            'search' => true,
            'filter' => false,
            'inputType' => 'textarea',
            'eval' => ['cols' => '10', 'rows' => '10', 'style' => 'height:100px'],
            'sql' => 'text NULL',
        ],
        'price_netto' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'price_brutto' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'csv_export_dir' => [
            'inputType' => 'fileTree',
            'eval' => ['mandatory' => true, 'required' => true, 'fieldType' => 'radio'],
            'sql' => 'binary(16) NULL',
        ],
        'pdf_import_dir' => [
            'inputType' => 'fileTree',
            'eval' => ['fieldType' => 'radio', 'files' => false, 'filesOnly' => false, 'class' => 'mandatory'],
            'sql' => 'binary(16) NULL',
        ],
        'csv_source' => [
            'inputType' => 'fileTree',
            'eval' => ['fieldType' => 'radio', 'files' => true, 'filesOnly' => true, 'extensions' => 'csv', 'class' => 'mandatory'],
            'sql' => 'binary(16) NULL',
        ],
        'csv_posten_source' => [
            'inputType' => 'fileTree',
            'eval' => ['fieldType' => 'radio', 'files' => true, 'filesOnly' => true, 'extensions' => 'csv', 'class' => 'mandatory'],
            'sql' => 'binary(16) NULL',
        ],
        // -- Backport C2 SQL-Import
        'sendEmail' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'FromEmail' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'ToEmail' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'alias' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
    ],
];
