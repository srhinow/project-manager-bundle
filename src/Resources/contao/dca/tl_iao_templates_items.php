<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Table tl_iao_templates_items.
 */
$GLOBALS['TL_DCA']['tl_iao_templates_items'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'switchToEdit' => true,
        'enableVersioning' => false,
        'onload_callback' => [
            ['srhinow.projectmanager.listener.dca.template_item', 'checkPermission'],
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 1,
            'fields' => ['position'],
            'flag' => 12,
            'panelLayout' => 'filter;search,limit',
        ],
        'label' => [
            'fields' => ['headline'],
            'format' => '%s',
        ],
        'global_operations' => [
            'back' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['backBT'],
                'href' => 'mod=&table=',
                'class' => 'header_back',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'href' => 'table=tl_iao_templates_items&act=edit',
                'icon' => 'edit.gif',
                'attributes' => 'class="contextmenu"',
            ],
            'copy' => [
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],

            'delete' => [
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if (!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\')) 
                    return false; Backend.getScrollOffset();"',
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__' => [],
        'default' => 'position;
            {title_legend},headline,headline_to_pdf;
            {item_legend},text,price,vat,count,amountStr,operator,vat_incl;
            {publish_legend},published',
    ],

    // Subpalettes
    'subpalettes' => [
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'headline' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'headline_to_pdf' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 2,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true, 'tl_class' => 'w50'],
            'sql' => "char(1) NOT NULL default '1'",
        ],
        'sorting' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'date' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'time' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'text' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'helpwizard' => true, 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'mediumtext NULL',
        ],
        'count' => [
            'exclude' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'amountStr' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.template_item', 'getItemUnitsOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange' => false],
            'sql' => "varchar(64) NOT NULL default ''",
        ],
        'operator' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options' => &$GLOBALS['TL_LANG']['tl_iao_templates_items']['operators'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => "char(1) NOT NULL default '+'",
        ],
        'price' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'price_netto' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'price_brutto' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'vat' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.template_item', 'getTaxRatesOptions'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '19'",
        ],
        'vat_incl' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options' => &$GLOBALS['TL_LANG']['tl_iao_templates_items']['vat_incl_percents'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '1'",
        ],
        'published' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 2,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'position' => [
            'exclude' => true,
            'filter' => true,
            'inputType' => 'select',
            'options' => &$GLOBALS['TL_LANG']['tl_iao_templates_items']['position_options'],
            'sql' => "varchar(25) NOT NULL default ''",
        ],
    ],
];
