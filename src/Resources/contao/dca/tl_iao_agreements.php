<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Table tl_iao_agreements.
 */
$GLOBALS['TL_DCA']['tl_iao_agreements'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ptable' => 'tl_iao_projects',
        'switchToEdit' => true,
        'enableVersioning' => false,
        'onload_callback' => [
            ['srhinow.projectmanager.listener.dca.agreement', 'checkPermission'],
            ['srhinow.projectmanager.listener.dca.agreement', 'fillEmptyMember'],
        ],
        'onsubmit_callback' => [
            ['srhinow.projectmanager.listener.dca.agreement', 'saveNettoAndBrutto'],
        ],
        'oncreate_callback' => [
            ['srhinow.projectmanager.listener.dca.agreement', 'setMemberfieldsFromProject'],
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'pid' => 'index',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 2,
            'fields' => ['end_date'],
            'flag' => 8,
            'panelLayout' => 'filter;search,limit',
        ],
        'label' => [
            'fields' => ['title', 'beginn_date', 'end_date', 'price_brutto'],
            'format' => '%s (aktuelle Laufzeit: %s - %s)',
            'label_callback' => ['srhinow.projectmanager.listener.dca.agreement', 'listEntries'],
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_iao_agreements']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif',
                'attributes' => 'class="contextmenu"',
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_iao_agreements']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'invoice' => [
                'label' => &$GLOBALS['TL_LANG']['tl_iao_agreements']['invoice'],
                'href' => 'key=addInvoice',
                'icon' => 'bundles/srhinowprojectmanager/icons/kontact_todo.png',
                'button_callback' => ['srhinow.projectmanager.listener.dca.agreement', 'addInvoice'],
            ],
            'pdf' => [
                'label' => &$GLOBALS['TL_LANG']['tl_iao_agreements']['pdf'],
                'href' => 'key=pdf',
                'icon' => 'iconPDF.gif',
                'button_callback' => ['srhinow.projectmanager.listener.dca.agreement', 'showPDF'],
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_iao_agreements']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if (!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\')) return false; Backend.getScrollOffset();"',
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__' => ['sendEmail'],
        'default' => '{settings_legend},setting_id,pid;
                      {title_legend},title;
                      {agreement_legend:hide},agreement_pdf_file;
                      {address_legend},member,text_generate,address_text;
                      {other_legend},price,vat,vat_incl,count,amountStr;
                      {status_legend},agreement_date,periode,beginn_date,end_date,status,terminated_date,new_generate;
                      {email_legend},sendEmail;
                      {invoice_generate_legend},before_template,after_template,posten_template;
                      {notice_legend:hide},notice',
    ],
    // Subpalettes
    'subpalettes' => [
        'sendEmail' => ('remind_before,email_from,email_to,email_subject,email_text'),
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'foreignKey' => 'tl_iao_projects.title',
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '1'",
            'relation' => ['type' => 'belongsTo', 'load' => 'eager'],
        ],
        'setting_id' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.agreement', 'getSettingOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'title' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => false, 'maxlength' => 255, 'tl_class' => 'long'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'agreement_date' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['rgxp' => 'date', 'datepicker' => $this->getDatePickerString(), 'tl_class' => 'w50 wizard'],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.agreement', 'getAgreementValue'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'periode' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.agreement', 'getPeriodeValue'],
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'beginn_date' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['rgxp' => 'date', 'datepicker' => $this->getDatePickerString(), 'tl_class' => 'w50 wizard'],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.agreement', 'getBeginnDateValue'],
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'end_date' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['rgxp' => 'date', 'datepicker' => $this->getDatePickerString(), 'tl_class' => 'w50 wizard'],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.agreement', 'getEndDateValue'],
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'new_generate' => [
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'clr'],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.agreement', 'setNewCycle'],
            ],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'terminated_date' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['rgxp' => 'date', 'datepicker' => $this->getDatePickerString(), 'tl_class' => 'w50 wizard'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'price' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'clr'],
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'price_netto' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'price_brutto' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'vat' => [
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.agreement', 'getTaxRatesOptions'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '19'",
        ],
        'vat_incl' => [
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options' => &$GLOBALS['TL_LANG']['tl_iao_agreements']['vat_incl_percents'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => "int(10) unsigned NOT NULL default '1'",
        ],
        'count' => [
            'exclude' => true,
            'flag' => 1,
            'inputType' => 'text',
            'default' => '1',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'amountStr' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.agreement', 'getItemUnitsOptions'],
            'eval' => ['tl_class' => 'w50', 'submitOnChange' => false],
            'sql' => "varchar(64) NOT NULL default ''",
        ],
        'member' => [
            'exclude' => true,
            'filter' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.agreement', 'getMemberOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'chosen' => true],
            'sql' => "varbinary(128) NOT NULL default ''",
        ],
        'text_generate' => [
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'clr', 'submitOnChange' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.agreement', 'fillAddressText'],
            ],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'address_text' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'style' => 'height:60px;', 'tl_class' => 'clr', 'nullIfEmpty' => true],
            'explanation' => 'insertTags',
            'sql' => 'mediumtext NULL',
        ],
        'status' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options' => &$GLOBALS['TL_LANG']['tl_iao_agreements']['status_options'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'agreement_pdf_file' => [
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => ['fieldType' => 'radio', 'files' => true, 'filesOnly' => true, 'mandatory' => false, 'extensions' => 'pdf'],
            'sql' => 'binary(16) NULL',
        ],
        'sendEmail' => [
            'filter' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true, 'submitOnChange' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'remind_before' => [
            'exclude' => true,
            'default' => '-3 weeks',
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'clr'],
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'email_from' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'rgxp' => 'email', 'maxlength' => 32, 'decodeEntities' => true, 'tl_class' => 'clr w50'],
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'email_to' => [
            'exclude' => true,
            'flag' => 11,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'decodeEntities' => true, 'maxlength' => 128, 'tl_class' => 'w50'],
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'email_subject' => [
            'exclude' => true,
            'flag' => 11,
            'inputType' => 'text',
            'eval' => ['mandatory' => false, 'maxlength' => 255, 'tl_class' => 'clr long'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'email_text' => [
            'exclude' => true,
            'inputType' => 'textarea',
            'eval' => ['mandatory' => true, 'decodeEntities' => true],
            'sql' => 'text NULL',
        ],
        'before_template' => [
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.agreement', 'getBeforeTemplate'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'after_template' => [
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.agreement', 'getAfterTemplate'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'posten_template' => [
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.agreement', 'getPostenTemplate'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'notice' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['cols' => '10', 'rows' => '10', 'style' => 'height:100px'],
            'sql' => 'text NULL',
        ],
    ],
];
