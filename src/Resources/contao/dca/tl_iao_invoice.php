<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Table tl_iao_invoice.
 */
$GLOBALS['TL_DCA']['tl_iao_invoice'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ptable' => 'tl_iao_projects',
        'ctable' => ['tl_iao_invoice_items'],
        'doNotCopyRecords' => true,
        'switchToEdit' => true,
        'enableVersioning' => false,
        'onload_callback' => [
            ['srhinow.projectmanager.listener.dca.invoice', 'generateInvoicePDF'],
            ['srhinow.projectmanager.listener.dca.invoice', 'checkPermission'],
            ['srhinow.projectmanager.listener.dca.invoice', 'fillEmptyMember'],
        ],
        'oncreate_callback' => [
            ['srhinow.projectmanager.listener.dca.invoice', 'preFillFields'],
            ['srhinow.projectmanager.listener.dca.invoice', 'setMemberfieldsFromProject'],
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'pid' => 'index',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 1,
            'fields' => ['invoice_tstamp'],
            'flag' => 8,
            'panelLayout' => 'filter;search,limit',
        ],
        'label' => [
            'fields' => ['title', 'invoice_id_str'],
            'format' => '%s (%s)',
            'label_callback' => ['srhinow.projectmanager.listener.dca.invoice', 'listEntries'],
        ],
        'global_operations' => [
            'importInvoices' => [
                'href' => 'key=importInvoices',
                'class' => 'global_import',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
            'exportInvoices' => [
                'href' => 'key=exportInvoices',
                'class' => 'global_export',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
            'all' => [
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'href' => 'table=tl_iao_invoice_items',
                'icon' => 'edit.gif',
                'attributes' => 'class="contextmenu"',
            ],
            'editheader' => [
                'href' => 'act=edit',
                'icon' => 'header.gif',
            ],
            'copy' => [
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'delete' => [
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if (!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\')) 
                return false; Backend.getScrollOffset();"',
            ],
            'show' => [
                'href' => 'act=show',
                'icon' => 'show.gif',
            ],
            'toggle' => [
                'icon' => 'ok.gif',
                'href' => 'act=toggle&amp;field=paid',
                'button_callback' => ['srhinow.projectmanager.listener.dca.invoice', 'togglePaidIcon'],
                'showInHeader' => true
            ],
            'pdf' => [
                'href' => 'key=pdf',
                'icon' => 'iconPDF.gif',
                'button_callback' => ['srhinow.projectmanager.listener.dca.invoice', 'showPDFButton'],
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__' => ['discount'],
        'default' => '
            {settings_legend},setting_id,pid;
            {title_legend},title;
            {invoice_id_legend:hide},invoice_id,invoice_id_str,invoice_tstamp,agreement_id,
                invoice_pdf_file,execute_date,expiry_date;
            {address_legend},member,text_generate,address_text;
            {text_before_legend},before_template,before_text,beforetext_as_template;
            {text_after_legend},after_template,after_text,aftertext_as_template;            
            {paid_legend},priceall_brutto,status,paid_on_dates,remaining;
            {extend_legend},noVat,discount;
            {notice_legend:hide},notice;
            {status_legend},published',
    ],

    // Subpalettes
    'subpalettes' => [
        'discount' => ('discount_title,discount_value,discount_operator'),
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'foreignKey' => 'tl_iao_projects.title',
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'belongsTo', 'load' => 'eager'],
        ],
        'reminder_id' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'sorting' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'setting_id' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.invoice', 'getSettingOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'title' => [
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'tl_class' => 'long'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'invoice_tstamp' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => [
                'doNotCopy' => true,
                'rgxp' => 'datim',
                'datepicker' => $this->getDatePickerString(),
                'tl_class' => 'w50 wizard',
            ],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'generateInvoiceTstamp'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'execute_date' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => [
                'doNotCopy' => true,
                'rgxp' => 'date',
                'datepicker' => $this->getDatePickerString(),
                'tl_class' => 'w50 wizard',
            ],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'generateExecuteDate'],
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'expiry_date' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => [
                'doNotCopy' => true,
                'rgxp' => 'date',
                'datepicker' => $this->getDatePickerString(),
                'tl_class' => 'w50 wizard',
            ],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'generateExpiryDate'],
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'invoice_id' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['doNotCopy' => true, 'tl_class' => 'w50'],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'setFieldInvoiceNumber'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'invoice_id_str' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['doNotCopy' => true, 'spaceToUnderscore' => true, 'maxlength' => 128, 'tl_class' => 'w50'],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'setFieldInvoiceNumberStr'],
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'invoice_pdf_file' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'fileTree',
            'eval' => [
                'fieldType' => 'radio',
                'tl_class' => 'clr',
                'extensions' => 'pdf',
                'files' => true,
                'filesOnly' => true,
                'mandatory' => false,
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'member' => [
            'filter' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.invoice', 'getMemberOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'chosen' => true],
            'sql' => "varbinary(128) NOT NULL default ''",
        ],
        'text_generate' => [
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'clr', 'submitOnChange' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'fillAddressText'],
            ],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'address_text' => [
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'mediumtext NULL',
        ],
        'before_template' => [
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.invoice', 'getBeforeTemplate'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange' => true, 'chosen' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'fillBeforeText'],
            ],
            'sql' => "int(10) unsigned NOT NULL default 0",
        ],
        'before_text' => [
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'helpwizard' => true, 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'text NULL',
        ],
        'beforetext_as_template' => [
            'flag' => 1,
            'inputType' => 'checkbox',
            'default' => '',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'saveBeforeTextAsTemplate'],
            ],
        ],
        'after_template' => [
            'flag' => 11,
            'inputType' => 'select',
            'default' => 0,
            'options_callback' => ['srhinow.projectmanager.listener.dca.invoice', 'getAfterTemplate'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange' => true, 'chosen' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'fillAfterText'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'after_text' => [
            'inputType' => 'textarea',
            'default' => '',
            'eval' => ['rte' => 'tinyMCE', 'helpwizard' => true, 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'text NULL',
        ],
        'aftertext_as_template' => [
            'flag' => 1,
            'inputType' => 'checkbox',
            'default' => '',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'saveAfterTextAsTemplate'],
            ],
        ],
        'published' => [
            'exclude'                 => true,
            'filter'                  => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'status' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'default' => '1',
            'options' => &$GLOBALS['TL_LANG']['tl_iao_invoice']['status_options'],
            'eval' => ['doNotCopy' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'updateStatus'],
            ],
            'sql' => "char(1) NOT NULL default '1'",
        ],
        'paid' => [
            'exclude'                 => true,
            'filter'                  => true,
            'toggle' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'updatePaid'],
            ],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'paid_on_date' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => [
                'doNotCopy' => true,
                'rgxp' => 'date',
                'datepicker' => $this->getDatePickerString(),
                'tl_class' => 'w50 wizard',
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'paid_on_dates' => [
            'exclude' => true,
            'inputType' => 'multiColumnWizard',
            'eval' => [
                // 'style'                 => 'width:100%;',
                'doNotCopy' => true,
                'columnFields' => [
                    'paydate' => [
                        'label' => &$GLOBALS['TL_LANG']['tl_iao_invoice']['paydate'],
                        'exclude' => true,
                        'inputType' => 'text',
                        'default' => '',
                        'eval' => [
                            'rgxp' => 'datim',
                            'datepicker' => $this->getDatePickerString(),
                            'tl_class' => 'wizard',
                            'style' => 'width:65%;',
                        ],
                    ],
                    'payamount' => [
                        'label' => &$GLOBALS['TL_LANG']['tl_iao_invoice']['payamount'],
                        'exclude' => true,
                        'search' => true,
                        'inputType' => 'text',
                        'eval' => ['style' => 'width:80%'],
                    ],
                    'paynotice' => [
                        'label' => &$GLOBALS['TL_LANG']['tl_iao_invoice']['paynotice'],
                        'exclude' => true,
                        'search' => true,
                        'inputType' => 'text',
                        // 'eval'              => array('style' => 'width:60%;'),
                    ],
                ],
            ],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'updateRemaining'],
            ],
            'sql' => 'blob NULL',
        ],
        'remaining' => [
            'inputType' => 'text',
            'default' => 0,
            'eval' => ['readonly' => true, 'style' => 'border:0'],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'priceFormat'],
            ],
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'priceall_brutto' => [
            'inputType' => 'text',
            'eval' => ['readonly' => true, 'style' => 'border:0'],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.invoice', 'getPriceallValue'],
                ['srhinow.projectmanager.listener.dca.invoice', 'priceFormat'],
            ],
        ],
        'noVat' => [
            'filter' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'default' => '',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'discount' => [
            'flag' => 1,
            'inputType' => 'checkbox',
            'default' => '',
            'eval' => ['doNotCopy' => true, 'submitOnChange' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'discount_title' => [
            'search' => true,
            'inputType' => 'text',
            'default' => '',
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(64) NOT NULL default 'Skonto'",
        ],
        'discount_value' => [
            'search' => true,
            'inputType' => 'text',
            'default' => '',
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(64) NOT NULL default '3'",
        ],
        'discount_operator' => [
            'flag' => 1,
            'inputType' => 'select',
            'default' => '',
            'options' => &$GLOBALS['TL_LANG']['tl_iao_invoice']['discount_operators'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => "char(1) NOT NULL default '%'",
        ],
        'notice' => [
            'search' => true,
            'inputType' => 'textarea',
            'default' => '',
            'eval' => ['cols' => '10', 'rows' => '10', 'style' => 'height:100px'],
            'sql' => 'text NULL',
        ],
        'agreement_id' => [
            'exclude' => false,
            'flag' => 11,
            'inputType' => 'select',
            'default' => 0,
            'options_callback' => ['srhinow.projectmanager.listener.dca.invoice', 'getAgreements'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'price_netto' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'price_brutto' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'pdf_import_dir' => [
            'inputType' => 'fileTree',
            'eval' => ['fieldType' => 'radio', 'files' => false, 'filesOnly' => false, 'class' => 'mandatory'],
            'sql' => 'binary(16) NULL',
        ],
        'csv_export_dir' => [
            'inputType' => 'fileTree',
            'eval' => ['mandatory' => true, 'required' => true, 'fieldType' => 'radio'],
            'sql' => 'binary(16) NULL',
        ],
        'csv_source' => [
            'inputType' => 'fileTree',
            'eval' => [
                'fieldType' => 'radio',
                'files' => true,
                'filesOnly' => true,
                'extensions' => 'csv',
                'class' => 'mandatory',
            ],
            'sql' => 'binary(16) NULL',
        ],
        'csv_posten_source' => [
            'inputType' => 'fileTree',
            'eval' => [
                'fieldType' => 'radio',
                'files' => true,
                'filesOnly' => true,
                'extensions' => 'csv',
                'class' => 'mandatory',
            ],
            'sql' => 'binary(16) NULL',
        ],
    ],
];
