<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Extend default palette.
 */
$GLOBALS['TL_DCA']['tl_user']['palettes']['extend'] =
    $GLOBALS['TL_DCA']['tl_user']['palettes']['extend']
    .';{iaolegend},iaomodules,iaomodulep,iaosettings,iaosettingp';

$GLOBALS['TL_DCA']['tl_user']['palettes']['custom'] =
    $GLOBALS['TL_DCA']['tl_user']['palettes']['custom']
    .';{iaolegend},iaomodules,iaomodulep,iaosettings,iaosettingp';

/*
 * Add fields to tl_user_group
 */
$GLOBALS['TL_DCA']['tl_user']['fields']['iaomodules'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_user']['iaomodules'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'foreignKey' => 'tl_news_archive.title',
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];

$GLOBALS['TL_DCA']['tl_user']['fields']['iaomodulep'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_user']['iaomodulep'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'options' => ['create', 'delete'],
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];

$GLOBALS['TL_DCA']['tl_user']['fields']['iaosettings'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_user']['iaosettings'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'foreignKey' => 'tl_news_feed.title',
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];

$GLOBALS['TL_DCA']['tl_user']['fields']['iaosettingp'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_user']['iaosettingp'],
    'exclude' => true,
    'inputType' => 'checkbox',
    'options' => ['create', 'delete'],
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval' => ['multiple' => true],
    'sql' => 'blob NULL',
];
