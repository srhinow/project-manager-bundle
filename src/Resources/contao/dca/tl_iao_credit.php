<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Table tl_iao_credit.
 */
$GLOBALS['TL_DCA']['tl_iao_credit'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ptable' => 'tl_iao_projects',
        'ctable' => ['tl_iao_credit_items'],
        'doNotCopyRecords' => true,
        'switchToEdit' => true,
        'enableVersioning' => false,
        'onload_callback' => [
            ['srhinow.projectmanager.listener.dca.credit', 'generateCreditPDF'],
            ['srhinow.projectmanager.listener.dca.credit', 'checkPermission'],
        ],
        'oncreate_callback' => [
            ['srhinow.projectmanager.listener.dca.credit', 'preFillFields'],
            ['srhinow.projectmanager.listener.dca.credit', 'setMemberfieldsFromProject'],
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'pid' => 'index',
            ],
        ],
    ],
    // List
    'list' => [
        'sorting' => [
            'mode' => 1,
            'fields' => ['credit_tstamp'],
            'flag' => 8,
            'panelLayout' => 'filter;search,limit',
        ],
        'label' => [
            'fields' => ['title', 'credit_id_str'],
            'format' => '%s (%s)',
            'label_callback' => ['srhinow.projectmanager.listener.dca.credit', 'listEntries'],
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'href' => 'table=tl_iao_credit_items',
                'icon' => 'edit.gif',
                'attributes' => 'class="contextmenu"',
            ],
            'editheader' => [
                'href' => 'act=edit',
                'icon' => 'header.gif',
            ],
            'copy' => [
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],

            'delete' => [
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if (!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\')) 
                return false; Backend.getScrollOffset();"',
            ],
            'show' => [
                'href' => 'act=show',
                'icon' => 'show.gif',
            ],
            'toggle' => [
                'icon' => 'ok.gif',
                'href' => 'act=toggle&amp;field=published',
                'button_callback' => ['srhinow.projectmanager.listener.dca.credit', 'toggleIcon'],
                'showInHeader' => true
            ],
            'pdf' => [
                'href' => 'key=pdf',
                'icon' => 'iconPDF.gif',
                'button_callback' => ['srhinow.projectmanager.listener.dca.credit', 'showPDFButton'],
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        'default' => '
        {settings_legend},setting_id,pid;title;
        {credit_id_legend:hide},credit_id,credit_id_str,credit_tstamp,credit_pdf_file,expiry_date;
        {address_legend},member,text_generate,address_text;
        {text_legend},before_template,before_text,after_template,after_text;
        {extend_legend},noVat;
        {status_legend},published,status;
        {notice_legend:hide},notice',
    ],
    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'foreignKey' => 'tl_iao_projects.title',
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'belongsTo', 'load' => 'eager'],
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'sorting' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'setting_id' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.credit', 'getSettingOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'title' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'credit_tstamp' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => [
                'rgxp' => 'datim',
                'doNotCopy' => true,
                'datepicker' => $this->getDatePickerString(),
                'tl_class' => 'w50 wizard',
            ],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.credit', 'generateCreditTstamp'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'expiry_date' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => [
                'rgxp' => 'date',
                'doNotCopy' => true,
                'datepicker' => $this->getDatePickerString(),
                'tl_class' => 'w50 wizard',
            ],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.credit', 'generateExpiryDate'],
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'credit_id' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => [
                'rgxp' => 'alnum',
                'doNotCopy' => true,
                'spaceToUnderscore' => true,
                'maxlength' => 128,
                'tl_class' => 'w50',
            ],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.credit', 'setFieldCreditNumber'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'credit_id_str' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['doNotCopy' => true, 'spaceToUnderscore' => true, 'maxlength' => 128, 'tl_class' => 'w50'],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.credit', 'setFieldCreditNumberStr'],
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'credit_pdf_file' => [
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => [
                'fieldType' => 'radio',
                'tl_class' => 'clr',
                'extensions' => 'pdf',
                'files' => true,
                'filesOnly' => true,
                'mandatory' => false,
            ],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'member' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.credit', 'getMemberOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'chosen' => true],
            'sql' => "varbinary(128) NOT NULL default ''",
        ],
        'text_generate' => [
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'clr', 'submitOnChange' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.credit', 'fillAddressText'],
            ],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'address_text' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'mediumtext NULL',
        ],
        'before_template' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.credit', 'getBeforeTemplate'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.credit', 'fillBeforeTextFromTemplate'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'before_text' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'helpwizard' => true, 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'text NULL',
        ],
        'beforetext_as_template' => [
            'filter' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'default' => '',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.credit', 'saveBeforeTextAsTemplate'],
            ],
        ],
        'after_template' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.credit', 'getAfterTemplate'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.credit', 'fillAfterTextFromTemplate'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'after_text' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'helpwizard' => true, 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'text NULL',
        ],
        'aftertext_as_template' => [
            'filter' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.credit', 'saveAfterTextAsTemplate'],
            ],
        ],
        'price_netto' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'price_brutto' => [
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'published' => [
            'exclude' => true,
            'toggle' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'status' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options' => ['1','2'],
            'reference' => &$GLOBALS['TL_LANG']['tl_iao']['status_options'],
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'noVat' => [
            'filter' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'notice' => [
            'exclude' => true,
            'search' => true,
            'filter' => false,
            'inputType' => 'textarea',
            'eval' => ['cols' => '10', 'rows' => '10', 'style' => 'height:100px'],
            'sql' => 'text NULL',
        ],
    ],
];
