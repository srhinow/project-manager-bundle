<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Table tl_iao_reminder.
 */
$GLOBALS['TL_DCA']['tl_iao_reminder'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ptable' => 'tl_iao_projects',
        'doNotCopyRecords' => true,
        'switchToEdit' => true,
        'enableVersioning' => false,
        'onload_callback' => [
            ['srhinow.projectmanager.listener.dca.reminder', 'checkPDF'],
            ['srhinow.projectmanager.listener.dca.reminder', 'checkPermission'],
        ],
        'onsubmit_callback' => [
            ['srhinow.projectmanager.listener.dca.reminder', 'setTextFinish'],
        ],
        'ondelete_callback' => [
            ['srhinow.projectmanager.listener.dca.reminder', 'onDeleteReminder'],
        ],
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'pid' => 'index',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 1,
            'fields' => ['reminder_tstamp'],
            'flag' => 8,
            'panelLayout' => 'filter;search,limit',
        ],
        'label' => [
            'fields' => ['title', 'invoice_id'],
            'format' => '%s (%s)',
            'label_callback' => ['srhinow.projectmanager.listener.dca.reminder', 'listEntries'],
        ],
        'global_operations' => [
            'checkReminder' => [
                'href' => 'key=checkReminder',
                'class' => 'check_reminder',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
            'all' => [
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'href' => 'act=edit',
                'icon' => 'edit.gif',
                'attributes' => 'class="contextmenu"',
            ],
            'copy' => [
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'delete' => [
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if (!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\')) 
                    return false; Backend.getScrollOffset();"',
            ],
            'show' => [
                'href' => 'act=show',
                'icon' => 'show.gif',
            ],
            'toggle' => [
                'icon' => 'ok.gif',
                'button_callback' => ['srhinow.projectmanager.listener.dca.reminder', 'toggleIcon'],
            ],
            'pdf' => [
                'href' => 'key=pdf',
                'icon' => 'iconPDF.gif',
                'button_callback' => ['srhinow.projectmanager.listener.dca.reminder', 'showPDFButton'],
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__' => [],
        'default' => '
            {settings_legend},setting_id,pid;
            {invoice_legend},invoice_id,step,set_step_values,title,reminder_tstamp,periode_date,tax,postage,unpaid,sum;
            {address_legend},member,text_generate,address_text;
            {reminder_legend},text,text_finish;
            {status_legend},published,status,paid_on_date;
            {notice_legend:hide},notice',
    ],

    // Subpalettes
    'subpalettes' => [
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'foreignKey' => 'tl_iao_projects.title',
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'belongsTo', 'load' => 'eager'],
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'setting_id' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.reminder', 'getSettingOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'title' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => false, 'maxlength' => 255, 'tl_class' => 'clr'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'text' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'helpwizard' => true, 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'mediumtext NULL',
        ],
        'text_finish' => [
            'exclude' => true,
            'eval' => ['tl_class' => 'clr'],
            'input_field_callback' => ['srhinow.projectmanager.listener.dca.reminder', 'getTextFinish'],
            'sql' => 'mediumtext NULL',
        ],
        'reminder_tstamp' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => [
                'doNotCopy' => true,
                'rgxp' => 'datim',
                'datepicker' => $this->getDatePickerString(),
                'tl_class' => 'clr w50 wizard',
            ],
            'load_callback' => [
                ['srhinow.projectmanager.listener.dca.reminder', 'generateReminderTstamp'],
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'periode_date' => [
            'exclude' => true,
            'inputType' => 'text',
            'default' => strtotime('+14 days'),
            'eval' => ['rgxp' => 'date', 'datepicker' => $this->getDatePickerString(), 'tl_class' => 'w50 wizard'],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'paid_on_date' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['rgxp' => 'date', 'datepicker' => $this->getDatePickerString(), 'tl_class' => 'w50 wizard'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'invoice_id' => [
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.reminder', 'getInvoices'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'step' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options' => &$GLOBALS['TL_LANG']['tl_iao_reminder']['steps'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'set_step_values' => [
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'w50', 'submitOnChange' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.reminder', 'fillStepFields'],
            ],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'unpaid' => [
            'exclude' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['tl_class' => 'w50', 'rgxp' => 'digit', 'nospace' => true],
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'tax' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 2, 'tl_class' => 'w50'],
            'sql' => "varchar(2) NOT NULL default '0'",
        ],
        'tax_typ' => [
            'exclude' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options' => ['1' => 'Soll (Zins von Brutto)', '2' => 'Ist (Zins von Netto)'],
            'eval' => ['tl_class' => 'w50'],
            'sql' => "varchar(25) NOT NULL default ''",
        ],
        'sum' => [
            'exclude' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['tl_class' => 'w50', 'rgxp' => 'digit', 'nospace' => true],
            'sql' => "varchar(64) NOT NULL default '0'",
        ],
        'postage' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 25, 'tl_class' => 'w50'],
            'sql' => "varchar(25) NOT NULL default '0'",
        ],
        'member' => [
            'filter' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.reminder', 'getMemberOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'chosen' => true],
            'sql' => "varbinary(128) NOT NULL default ''",
        ],
        'text_generate' => [
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['tl_class' => 'clr', 'submitOnChange' => true],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.reminder', 'fillAddressText'],
            ],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'address_text' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'mediumtext NULL',
        ],
        'published' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'status' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'select',
            'options' => &$GLOBALS['TL_LANG']['tl_iao_reminder']['status_options'],
            'eval' => ['tl_class' => 'w50'],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.reminder', 'updateStatus'],
            ],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'notice' => [
            'exclude' => true,
            'search' => true,
            'filter' => false,
            'inputType' => 'textarea',
            'eval' => ['cols' => '10', 'rows' => '10', 'style' => 'height:100px'],
            'sql' => 'mediumtext NULL',
        ],
    ],
];
