<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Table tl_iao_projects.
 */
$GLOBALS['TL_DCA']['tl_iao_projects'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ctable' => ['tl_iao_offer', 'tl_iao_invoice', 'tl_iao_credit'],
        'switchToEdit' => false,
        'enableVersioning' => true,

        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 1,
            'fields' => ['title'],
            'flag' => 1,
            'panelLayout' => 'filter;sort,search,limit',
        ],
        'label' => [
            'fields' => ['title'],
            'format' => '%s',
        ],
        'global_operations' => [
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"',
            ],
        ],
        'operations' => [
            'offer' => [
                'href' => 'table=tl_iao_offer&onlyproj=1',
                'icon' => 'bundles/srhinowprojectmanager/icons/16-file-page.png',
            ],
            'invoice' => [
                'href' => 'table=tl_iao_invoice&onlyproj=1',
                'icon' => 'bundles/srhinowprojectmanager/icons/kontact_todo.png',
            ],
            'credit' => [
                'href' => 'table=tl_iao_credit&onlyproj=1',
                'icon' => 'bundles/srhinowprojectmanager/icons/16-tag-pencil.png',
            ],
            'reminder' => [
                'href' => 'table=tl_iao_reminder&onlyproj=1',
                'icon' => 'bundles/srhinowprojectmanager/icons/warning.png',
            ],
            'agreements' => [
                'href' => 'table=tl_iao_agreements&onlyproj=1',
                'icon' => 'bundles/srhinowprojectmanager/icons/clock_history_frame.png',
            ],
            'edit' => [
                'href' => 'act=edit',
                'icon' => 'edit.gif',
                'attributes' => 'class="contextmenu"',
            ],
            'copy' => [
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'delete' => [
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if (!confirm(\''.($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null).'\')) 
                    return false; Backend.getScrollOffset();"',
            ],
            'show' => [
                'href' => 'act=show',
                'icon' => 'show.gif',
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        '__selector__' => ['in_reference', 'finished'],
        'default' => '
            {settings_legend},setting_id;
            {project_legend},title,member,url;notice;
            {finshed_legend},finished;
            {reference_legend},in_reference',
    ],
    // Subpalettes
    'subpalettes' => [
        'in_reference' => 'reference_title,reference_alias,reference_short_title,reference_subtitle,
            reference_customer,reference_todo,reference_desription,tags,singleSRC,multiSRC,orderSRC',
        'finished' => 'finished_date',
    ],

    // Fields
    'fields' => [
        'id' => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'sorting' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'sorting' => true,
        ],
        'setting_id' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.project', 'getSettingOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => false, 'chosen' => true],
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'title' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'member' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'flag' => 11,
            'inputType' => 'select',
            'options_callback' => ['srhinow.projectmanager.listener.dca.project', 'getMemberOptions'],
            'eval' => ['tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange' => true, 'chosen' => true],
            'sql' => "varbinary(128) NOT NULL default ''",
        ],
        'in_reference' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true, 'submitOnChange' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'reference_title' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'reference_alias' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => [
                'rgxp' => 'alias',
                'doNotCopy' => true,
                'unique' => true,
                'maxlength' => 128,
                'tl_class' => 'w50',
            ],
            'save_callback' => [
                ['srhinow.projectmanager.listener.dca.project', 'generateReferenceAlias'],
            ],
            'sql' => "varchar(128) NOT NULL default ''",
        ],

        'reference_short_title' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'clr'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'reference_subtitle' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'reference_customer' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'mediumtext NULL',
        ],
        'reference_todo' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'mediumtext NULL',
        ],
        'reference_desription' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'textarea',
            'eval' => ['rte' => 'tinyMCE', 'style' => 'height:60px;', 'tl_class' => 'clr'],
            'explanation' => 'insertTags',
            'sql' => 'mediumtext NULL',
        ],
        'tags' => [
            'label' => &$GLOBALS['TL_LANG']['MSC']['tags'],
            'inputType' => 'tag',
            'sql' => 'mediumtext NULL',
        ],
        'url' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'finished' => [
            'exclude' => true,
            'filter' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => ['doNotCopy' => true, 'submitOnChange' => true],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'finished_date' => [
            'exclude' => true,
            'inputType' => 'text',
            'eval' => ['rgxp' => 'date', 'datepicker' => true, 'tl_class' => 'w50 wizard'],
            'sql' => "varchar(10) NOT NULL default ''",
        ],
        'notice' => [
            'exclude' => true,
            'search' => true,
            'filter' => false,
            'inputType' => 'textarea',
            'eval' => [
                'cols' => '10',
                'rows' => '10',
                'style' => 'height:100px',
                'tl_css' => 'clr m12 long',
            ],
            'sql' => 'text NULL',
        ],
        'singleSRC' => [
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => [
                'filesOnly' => true,
                'fieldType' => 'radio',
                'tl_class' => 'clr',
                'extensions' => \Contao\Config::get('validImageTypes'),
            ],
            'sql' => 'binary(16) NULL',
        ],
        'multiSRC' => [
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => [
                'multiple' => true,
                'fieldType' => 'checkbox',
                'orderField' => 'orderSRC',
                'files' => true,
                'extensions' => \Contao\Config::get('validImageTypes'),
            ],
            'sql' => 'blob NULL',
        ],
        'orderSRC' => [
            'sql' => 'blob NULL',
        ],
    ],
];
