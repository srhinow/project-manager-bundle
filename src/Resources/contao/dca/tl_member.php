<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * Table tl_member.
 */
$GLOBALS['TL_DCA']['tl_member']['palettes']['iao_customer'] = '
    {address_legend},company,street,postal,city,state,country;
    {personal_legend:hide},title,firstname,lastname,dateOfBirth,gender;
    {custom_addresstext_legend:hide},text_generate,address_text;
    {contact_legend},phone,mobile,fax,email,website;
    {groups_legend},groups;
    {login_legend},login;
    {homedir_legend:hide},assignDir;
    {import_settings:hide},myid;
    {account_legend},disable,start,stop';

$GLOBALS['TL_DCA']['tl_member']['fields']['firstname']['eval']['mandatory'] = false;
$GLOBALS['TL_DCA']['tl_member']['fields']['firstname']['eval']['lastname'] = false;
$GLOBALS['TL_DCA']['tl_member']['fields']['title'] = [
    'exclude' => true,
    'search' => true,
    'sorting' => true,
    'flag' => 1,
    'inputType' => 'text',
    'eval' => ['mandatory' => false, 'maxlength' => 255, 'feEditable' => true, 'feViewable' => true, 'feGroup' => 'personal'],
    'sql' => "varchar(255) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_member']['fields']['myid'] = [
    'exclude' => true,
    'inputType' => 'text',
    'eval' => ['rgxp' => 'alnum', 'doNotCopy' => true, 'spaceToUnderscore' => true, 'maxlength' => 128, 'tl_class' => 'w50'],
    'sql' => "int(10) unsigned NOT NULL default '0'",
];

$GLOBALS['TL_DCA']['tl_member']['fields']['iao_group'] = [
    'sql' => "int(10) unsigned NOT NULL default '0'",
];

$GLOBALS['TL_DCA']['tl_member']['fields']['text_generate'] = [
    'flag' => 1,
    'inputType' => 'checkbox',
    'eval' => ['tl_class' => 'clr', 'submitOnChange' => true],
    'save_callback' => [
        ['srhinow.projectmanager.listener.dca.member', 'fillAddressText'],
    ],
    'sql' => "char(1) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_member']['fields']['address_text'] = [
    'search' => true,
    'inputType' => 'textarea',
    'eval' => ['rte' => 'tinyMCE', 'style' => 'height:60px;', 'tl_class' => 'clr'],
    'explanation' => 'insertTags',
    'sql' => 'mediumtext NULL',
];
