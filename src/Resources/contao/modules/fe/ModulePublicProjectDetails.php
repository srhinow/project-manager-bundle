<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Modules\Fe;

use Contao\Environment;
use Contao\FrontendTemplate;
use Contao\Input;
use Contao\Module;
use Contao\StringUtil;
use Srhinow\ProjectManagerBundle\Model\IaoProjectsModel;

/**
 * Class ModulePublicProjectDetails.
 *
 * Frontend module "project-manager-bundle"
 */
class ModulePublicProjectDetails extends Module
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'mod_public_project_details';

    protected $itemTemplate = 'iao_public_project_details';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (\defined('TL_MODE') && TL_MODE === 'BE') {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### IAO PUBLIC PROJECT DETAILS ###';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['project']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item'])) {
            Input::setGet('project', Input::get('auto_item'));
        }

        // Ajax Requests abfangen
        if (Input::get('project') && Environment::get('isAjaxRequest')) {
            $this->generateAjax();
        }

        // Do not index or cache the page if no news item has been specified
        if (!Input::get('project')) {
            global $objPage;
            $objPage->noSearch = 1;
            $objPage->cache = 0;

            return '';
        }

        return parent::generate();
    }

    /**
     * generiert die Details ohne den kompletten DOM.
     */
    public function generateAjax(): void
    {
        /** @var FrontendTemplate|object $objTemplate */
        $objTemplate = new \FrontendTemplate($this->strTemplate);
        $objProject = IaoProjectsModel::findOnePublicProject(Input::get('project'));
        $objTemplate->detailHtml = $this->generateProjectHtml($objProject);
        $objTemplate->isAjax = true;
        echo $objTemplate->parse();
        exit();
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        global $objPage;

        $objProject = IaoProjectsModel::findOnePublicProject(Input::get('project'));

        $this->Template->detailHtml = $this->generateProjectHtml($objProject);
        $this->Template->isAjax = false;
        $this->Template->referer = 'javascript:history.go(-1)';
        $this->Template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];

        //SEO-Werte setzen
        if (null !== $objProject) {
            if (\strlen($objProject->reference_title) > 0) {
                $objPage->title = strip_tags(StringUtil::stripInsertTags($objProject->reference_title));
                $objPage->pageTitle = strip_tags(StringUtil::stripInsertTags($objProject->reference_title));
            }
            if (\strlen($objProject->reference_subtitle) > 0) {
                $objPage->pageTitle .= ' - '.$objProject->reference_subtitle;
            }

            if (\strlen($objProject->reference_todo) > 0) {
                $GLOBALS['TL_KEYWORDS'] .= strip_tags(StringUtil::stripInsertTags($objProject->reference_todo));
            }
            $objPage->description = StringUtil::substr(
                $objProject->reference_customer.' '
                .$objProject->reference_todo.' '
                .$objProject->reference_desription,
                320);
        }
    }

    /**
     * holt die Projekt-Referenz-Daten fürs Template.
     *
     * @param IaoProjectsModel|null $objProject
     *
     * @return string
     */
    protected function generateProjectHtml($objProject)
    {
        // Fallback template
        if ($this->fe_iao_template) {
            $this->itemTemplate = $this->fe_iao_template;
        }

        $Template = new FrontendTemplate($this->itemTemplate);

        if (null === $objProject) {
            $Template->notFound = true;

            return $Template->parse();
        }

        //alle Daten 1zu1 dem Template übergeben
        $Template->setData($objProject->row());

        // Website
        $Template->url = ('http' !== substr($objProject->url, 0, 4)) ? 'http://'.$objProject->url : $objProject->url;

        // Add the article image as enclosure
        if (null !== $objProject->singleSRC) {
            $objFile = \FilesModel::findByUuid($objProject->singleSRC);

            if (null !== $objFile) {
                $Template->objImage = $objFile;
            }
        }

        return $Template->parse();
    }
}
