<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Modules\Fe;

/*
 * @copyright  Sven Rhinow 2011-2018
 * @author     sr-tag Sven Rhinow Webentwicklung <http://www.sr-tag.de>
 * @package    project-manager-bundle
 * @license    LGPL
 * @filesource
 */

use Contao\BackendTemplate;
use Contao\Input;
use Contao\Module;
use Contao\Pagination;
use Srhinow\ProjectManagerBundle\Model\IaoProjectsModel;

/**
 * Class ModulePublicProjectList.
 *
 * Frontend module "project-manager-bundle"
 */
class ModulePublicProjectList extends Module
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'iao_public_project_list';

    /**
     * Target pages.
     *
     * @var array
     */
    protected $arrTargets = [];

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (\defined('TL_MODE') && TL_MODE === 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### IAO PUBLIC PROJECT LIST ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id='.$this->id;

            return $objTemplate->parse();
        }

        // Fallback template
        if (\strlen($this->fe_iao_template)) {
            $this->strTemplate = $this->fe_iao_template;
        }

        return parent::generate();
    }

    /**
     * Generate module.
     */
    protected function compile(): void
    {
        $offset = 0;
        $limit = null;

        // Maximum number of items
        if ($this->fe_iao_numberOfItems > 0) {
            $limit = $this->fe_iao_numberOfItems;
        }

        $searchWhereArr[] = '`finished` = 1';
        $searchWhereArr[] = '`in_reference` = 1';

        // Get the total number of items
        $intTotal = IaoProjectsModel::countEntries($searchWhereArr);

        // Filter anwenden um die Gesamtanzahl zuermitteln
        if ((int) $intTotal > 0) {
            $total = $intTotal - $offset;

            // Split the results
            if ($this->perPage > 0 && (!isset($limit) || $this->numberOfItems > $this->perPage)) {
                // Adjust the overall limit
                if (isset($limit)) {
                    $total = min($limit, $total);
                }

                // Get the current page
                $id = 'page_n'.$this->id;
                $page = Input::get($id) ?: 1;

                // Do not index or cache the page if the page number is outside the range
                if ($page < 1 || $page > max(ceil($total / $this->perPage), 1)) {
                    global $objPage;
                    $objPage->noSearch = 1;
                    $objPage->cache = 0;

                    // Send a 404 header
                    header('HTTP/1.1 404 Not Found');

                    return;
                }

                // Set limit and offset
                $limit = $this->perPage;
                $offset = (max($page, 1) - 1) * $this->perPage;

                // Overall limit
                if ($offset + $limit > $total) {
                    $limit = $total - $offset;
                }

                // Add the pagination menu
                $objPagination = new Pagination($total, $this->perPage);
                $this->Template->pagination = $objPagination->generate("\n  ");
            }

            // Get the items
            if (isset($limit)) {
                $itemsObj = IaoProjectsModel::findProjects($limit, $offset, $searchWhereArr, ['order' => 'finished_date DESC']);
            } else {
                $itemsObj = IaoProjectsModel::findProjects(0, $offset, $searchWhereArr, ['order' => 'finished_date DESC']);
            }

            $itemsArray = [];
            $count = -1;

            while ($itemsObj->next()) {
                //row - Class
                $class = 'row_'.++$count.((0 === $count) ? ' row_first' : '')
                    .(($count >= ($limit - 1)) ? ' row_last' : '')
                    .((0 === ($count % 2)) ? ' even' : ' odd');

                // Add the article image as enclosure
                $image = '';

                if (null !== $itemsObj->singleSRC) {
                    $objFile = \FilesModel::findByUuid($itemsObj->singleSRC);

                    if (null !== $objFile) {
                        $image = $objFile->path;
                    }
                }

                //Detail-Url
                $detailUrl = false;
                if ($this->jumpTo) {
                    $objDetailPage = \PageModel::findByPk($this->jumpTo);
                    $detailUrl = ampersand($this->generateFrontendUrl(
                        $objDetailPage->row(),
                        '/'.((\strlen($itemsObj->reference_alias) > 0) ? $itemsObj->reference_alias : $itemsObj->id))
                    );
                }

                $itemsArray[] = [
                    'short_title' => $itemsObj->reference_short_title,
                    'title' => $itemsObj->reference_title,
                    'subtitle' => $itemsObj->reference_subtitle,
                    'customer' => $itemsObj->reference_customer,
                    'todo' => $itemsObj->reference_todo,
                    'desription' => $itemsObj->reference_desription,
                    'file' => $image,
                    'detailUrl' => $detailUrl,
                ];
            }
        }

        $this->Template->headline = $this->headline;
        $this->Template->items = $itemsArray;
        $this->Template->messages = ''; // Backwards compatibility
    }
}
