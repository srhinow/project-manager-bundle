<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Modules\Be;

use Contao\BackendModule;
use Contao\Controller;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Input;

/**
 * Class ModuleIaoSetup
 * Back end module project-manager-bundle "setup".
 */
class ModuleIaoSetup extends BackendModule
{
    /**
     * Template.
     *
     * @var string
     */
    protected $strTemplate = 'be_iao_setup';

    /**
     * Isotope modules.
     *
     * @var array
     */
    protected $arrModules = [];

    /**
     * Generate the module.
     *
     * @return string
     */
    public function generate()
    {
        $this->import('BackendUser', 'User');

        foreach ($GLOBALS['IAO_MOD'] as $strGroup => $arrModules) {
            foreach ($arrModules as $strModule => $arrConfig) {
                if ($this->User->hasAccess($strModule, 'iao_modules')) {
                    if (\is_array($arrConfig['tables'])) {
                        $GLOBALS['BE_MOD']['iao']['iao_setup']['tables'] += $arrConfig['tables'];
                    }

                    $this->arrModules[$GLOBALS['TL_LANG']['PMB'][$strGroup]][$strModule] = [
                        'name' => ($GLOBALS['TL_LANG']['PMB'][$strModule][0] ? $GLOBALS['TL_LANG']['PMB'][$strModule][0] : $strModule),
                        'description' => $GLOBALS['TL_LANG']['PMB'][$strModule][1],
                        'icon' => $arrConfig['icon'],
                    ];
                }
            }
        }

        // Open module
        if (null !== Input::get('mod')) {
            //fix: Back-Link
            if ('' === Input::get('mod')) {
                $redirectUrl = Controller::addToUrl('', true, ['table', 'mod']);
                $this->redirect($redirectUrl);
            }

            return $this->getIAOModule(Input::get('mod'));
        }

        // Table set but module missing, fix the saveNcreate link
        if ('' !== Input::get('table')) {
            foreach ($GLOBALS['IAO_MOD'] as $arrGroup) {
                foreach ($arrGroup as $strModule => $arrConfig) {
                    if (\is_array($arrConfig['tables'])
                        && \in_array(Input::get('table'), $arrConfig['tables'], true)) {
                        $this->redirect($this->addToUrl('mod='.$strModule));
                    }
                }
            }
        }

        return parent::generate();
    }

    /**
     * Generate the module.
     */
    protected function compile(): void
    {
        $this->Template->modules = $this->arrModules;
        $this->Template->script = \Contao\Environment::get('script').'/contao';
        $this->Template->welcome = sprintf($GLOBALS['TL_LANG']['PMB']['config_module'], IAO_VERSION.'.'.IAO_BUILD);
    }

    /**
     * Open an iao module and return it as HTML.
     *
     * @param string
     */
    protected function getIAOModule($module)
    {
        $arrModule = [];
        $logger = static::getContainer()->get('monolog.logger.contao');
        foreach ($GLOBALS['IAO_MOD'] as $arrGroup) {
            if (!empty($arrGroup) && \in_array($module, array_keys($arrGroup), true)) {
                $arrModule = &$arrGroup[$module];
            }
        }

        // Check whether the current user has access to the current module
        if (!$this->User->isAdmin && !$this->User->hasAccess($module, 'iao_modules')) {
            $logger->log(
                'project-manager-bundle module "'.$module.'" was not allowed for user "'.$this->User->username.'"',
                'ModuleIsotopeSetup getIsotopeModule()',
                ['contao' => new ContaoContext(__CLASS__.'::'.__METHOD__, 'ERROR')]
            );

            $this->redirect($this->Environment->script.'?act=error');
        }

        $strTable = Input::get('table');

        if (null === $strTable && !isset($arrModule['callback'])) {
            $this->redirect($this->addToUrl('table='.$arrModule['tables'][0]));
        }

        $id = (!Input::get('act') && Input::get('id')) ? Input::get('id') : $this->Session->get('CURRENT_ID');

        // Add module style sheet
        if (isset($arrModule['stylesheet'])) {
            $GLOBALS['TL_CSS'][] = $arrModule['stylesheet'];
        }

        // Add module javascript
        if (isset($arrModule['javascript'])) {
            $GLOBALS['TL_JAVASCRIPT'][] = $arrModule['javascript'];
        }

        // Redirect if the current table does not belong to the current module
        if ('' !== $strTable) {
            if (!\in_array($strTable, (array) $arrModule['tables'], true)) {
                $logger->log(
                    'Table "'.$strTable.'" is not allowed in Isotope module "'.$module.'"',
                    'ModuleIaoSetup getIAOModule()',
                    ['contao' => new ContaoContext(__CLASS__.'::'.__METHOD__, 'ERROR')]
                );

                $this->redirect('contao/main.php?act=error');
            }

            // Load the language and DCA file
            $this->loadLanguageFile($strTable);
            $this->loadDataContainer($strTable);

            // Include all excluded fields which are allowed for the current user
            if ($GLOBALS['TL_DCA'][$strTable]['fields']) {
                foreach ($GLOBALS['TL_DCA'][$strTable]['fields'] as $k => $v) {
                    if (isset($v['exclude'])) {
                        if ($this->User->hasAccess($strTable.'::'.$k, 'alexf')) {
                            $GLOBALS['TL_DCA'][$strTable]['fields'][$k]['exclude'] = false;
                        }
                    }
                }
            }

            // Fabricate a new data container object
            if (!\strlen($GLOBALS['TL_DCA'][$strTable]['config']['dataContainer'])) {
                $logger->log(
                    'Missing data container for table "'.$strTable.'"',
                    'Backend getBackendModule()',
                    ['contao' => new ContaoContext(__CLASS__.'::'.__METHOD__, 'ERROR')]
                );

                trigger_error('Could not create a data container object', E_USER_ERROR);
            }

            $dataContainer = 'DC_'.$GLOBALS['TL_DCA'][$strTable]['config']['dataContainer'];
            $dc = new $dataContainer($strTable);
        }

        // AJAX request
        if ($_POST && $this->Environment->isAjaxRequest) {
            $this->objAjax->executePostActions($dc);
        }

        // Call module callback
        elseif (isset($arrModule['callback']) && \class_exists($arrModule['callback'])) {
            $objCallback = new $arrModule['callback']($dc);

            return $objCallback->generate();
        }

        // Custom action (if key is not defined in config.php the default action will be called)
        elseif (Input::get('key') && isset($arrModule[Input::get('key')])) {
            $objCallback = new $arrModule[Input::get('key')][0]();

            return $objCallback->$arrModule[Input::get('key')][1]($dc, $strTable, $arrModule);
        }

        // Default action
        elseif (\is_object($dc)) {
            $act = Input::get('act');

            if (null === $act || 'paste' === $act || 'select' === $act) {
                $act = 'showAll';
            }

            return $dc->$act();
        }

        return null;
    }
}
