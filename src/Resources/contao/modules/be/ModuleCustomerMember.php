<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Modules\Be;

use Contao\Backend;
use Contao\BackendModule;
use Contao\Environment;
use Contao\Input;
use Srhinow\ProjectManagerBundle\Helper\Iao;

/**
 * Class ModuleCustomerMember.
 */
class ModuleCustomerMember extends BackendModule
{
    protected $settings = [];

    /**
     * Change the palette of the current table and switch to edit mode.
     */
    public function generate()
    {
        if ('tl_member' !== $this->table) {
            Backend::redirect(str_replace('do=iao_customer', 'do=member', Environment::get('request')));
        }

        $IaoClass = Iao::getInstance();
        $this->settings = $IaoClass->getSettings($GLOBALS['IAO']['default_settings_id']);

        $GLOBALS['TL_DCA'][$this->table]['config']['onsubmit_callback'][]
            = ['srhinow.projectmanager.listener.dca.member', 'setCustomerGroup'];

        $GLOBALS['TL_DCA'][$this->table]['palettes'] = [
            '__selector__' => $GLOBALS['TL_DCA'][$this->table]['palettes']['__selector__'],
            'default' => $GLOBALS['TL_DCA'][$this->table]['palettes']['iao_customer'],
        ];

        $GLOBALS['TL_DCA'][$this->table]['list']['sorting'] = [
            'mode' => 2,
            'fields' => ['company'],
            'flag' => 11,
            'panelLayout' => 'filter;sort,search,limit',
            'filter' => [['iao_group=?', $this->settings['iao_costumer_group']]],
        ];

        $GLOBALS['TL_DCA'][$this->table]['list']['label'] = [
            'fields' => ['title', 'firstname', 'lastname'],
            'format' => '%s %s %s',
        ];

        unset($GLOBALS['TL_DCA'][$this->table]['list']['operations']['addresses']);

        $GLOBALS['TL_DCA'][$this->table]['fields']['company']['flag'] = false;

        $act = Input::get('act');

        return (null === $act || 'select' === $act) ? $this->objDc->showAll() : $this->objDc->{$act}();
    }

    /**
     * Generate module.
     */
    protected function compile()
    {
        return '';
    }
}
