<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

/*
 * project-manager-bundle Version.
 */
@\define('IAO_VERSION', '1.2');
@\define('IAO_BUILD', '00');
@\define('IAO_PATH', 'vendor/srhinow/project-manager-bundle');
@\define('PMB_PUBLIC_FOLDER', 'bundles/srhinowprojectmanager');
@\define('IAO_PDFCLASS_FILE', IAO_PATH.'/classes/iaoPDF.php');

/*
 * DEFAULT IAO VALUES
*/
$GLOBALS['IAO']['default_settings_id'] = 1;
$GLOBALS['IAO']['default_agreement_cycle'] = '+1 year';
$GLOBALS['IAO']['csv_seperators'] = ['comma' => ',', 'semicolon' => ';', 'tabulator' => '\t', 'linebreak' => '\n'];

/*
 * back-end modules
 */

$GLOBALS['BE_MOD']['iao'] = [
    'iao_projects' => [
        'tables' => ['tl_iao_projects', 'tl_iao_agreements', 'tl_iao_invoice', 'tl_iao_invoice_items', 'tl_iao_offer', 'tl_iao_offer_items', 'tl_iao_credit', 'tl_iao_credit_items', 'tl_iao_reminder'],
        'icon' => 'bundles/srhinowprojectmanager/icons/blackboard_steps.png',
        'importInvoices' => ['Srhinow\ProjectManagerBundle\Helper\Backend\Invoice\ImportExport', 'importInvoices'],
        'exportInvoices' => ['Srhinow\ProjectManagerBundle\Helper\Backend\Invoice\ImportExport', 'exportInvoices'],
        'importOffer' => ['Srhinow\ProjectManagerBundle\Helper\Backend\Offer\ImportExport', 'importOffer'],
        'exportOffer' => ['Srhinow\ProjectManagerBundle\Helper\Backend\Offer\ImportExport', 'exportOffer'],
    ],
    'iao_offer' => [
        'tables' => ['tl_iao_offer', 'tl_iao_offer_items'],
        'icon' => 'bundles/srhinowprojectmanager/icons/16-file-page.png',
        'importOffer' => ['Srhinow\ProjectManagerBundle\Helper\Backend\Offer\ImportExport', 'importOffer'],
        'exportOffer' => ['Srhinow\ProjectManagerBundle\Helper\Backend\Offer\ImportExport', 'exportOffer'],
    ],
    'iao_invoice' => [
        'tables' => ['tl_iao_invoice', 'tl_iao_invoice_items'],
        'icon' => 'bundles/srhinowprojectmanager/icons/kontact_todo.png',
        'importInvoices' => ['Srhinow\ProjectManagerBundle\Helper\Backend\Invoice\ImportExport', 'importInvoices'],
        'exportInvoices' => ['Srhinow\ProjectManagerBundle\Helper\Backend\Invoice\ImportExport', 'exportInvoices'],
    ],
    'iao_credit' => [
        'tables' => ['tl_iao_credit', 'tl_iao_credit_items'],
        'icon' => 'bundles/srhinowprojectmanager/icons/16-tag-pencil.png',
    ],
    'iao_reminder' => [
        'tables' => ['tl_iao_reminder'],
        'icon' => 'bundles/srhinowprojectmanager/icons/warning.png',
        'checkReminder' => ['Srhinow\ProjectManagerBundle\Helper\Backend\Reminder\Reminder', 'checkReminder'],
    ],
    'iao_agreements' => [
        'tables' => ['tl_iao_agreements'],
        'icon' => 'bundles/srhinowprojectmanager/icons/clock_history_frame.png',
    ],
    'iao_customer' => [
        'tables' => ['tl_member', 'tl_iso_address'],
        'callback' => 'Srhinow\ProjectManagerBundle\Modules\Be\ModuleCustomerMember',
        'icon' => 'bundles/srhinowprojectmanager/icons/users.png',
    ],
    'iao_setup' => [
        'callback' => 'Srhinow\ProjectManagerBundle\Modules\Be\ModuleIaoSetup',
        'tables' => [],
        'icon' => 'bundles/srhinowprojectmanager/process.png',
    ],
];

if (\defined('TL_MODE') && 'BE' === TL_MODE) {
    $GLOBALS['TL_CSS'][] = PMB_PUBLIC_FOLDER.'/be.css|static';
}

$GLOBALS['TL_MODELS']['tl_iao_agreements'] = \Srhinow\ProjectManagerBundle\Model\IaoAgreementsModel::class;
$GLOBALS['TL_MODELS']['tl_iao_credit_items'] = \Srhinow\ProjectManagerBundle\Model\IaoCreditItemsModel::class;
$GLOBALS['TL_MODELS']['tl_iao_credit'] = \Srhinow\ProjectManagerBundle\Model\IaoCreditModel::class;
$GLOBALS['TL_MODELS']['tl_iao_invoice_items'] = \Srhinow\ProjectManagerBundle\Model\IaoInvoiceItemsModel::class;
$GLOBALS['TL_MODELS']['tl_iao_invoice'] = \Srhinow\ProjectManagerBundle\Model\IaoInvoiceModel::class;
$GLOBALS['TL_MODELS']['tl_iao_offer_items'] = \Srhinow\ProjectManagerBundle\Model\IaoOfferItemsModel::class;
$GLOBALS['TL_MODELS']['tl_iao_offer'] = \Srhinow\ProjectManagerBundle\Model\IaoOfferModel::class;
$GLOBALS['TL_MODELS']['tl_iao_projects'] = \Srhinow\ProjectManagerBundle\Model\IaoProjectsModel::class;
$GLOBALS['TL_MODELS']['tl_iao_reminder'] = \Srhinow\ProjectManagerBundle\Model\IaoReminderModel::class;
$GLOBALS['TL_MODELS']['tl_iao_settings'] = \Srhinow\ProjectManagerBundle\Model\IaoSettingsModel::class;
$GLOBALS['TL_MODELS']['tl_iao_templates_items'] = \Srhinow\ProjectManagerBundle\Model\IaoTemplatesItemsModel::class;
$GLOBALS['TL_MODELS']['tl_iao_templates'] = \Srhinow\ProjectManagerBundle\Model\IaoTemplatesModel::class;
$GLOBALS['TL_MODELS']['tl_iao_item_units'] = \Srhinow\ProjectManagerBundle\Model\IaoItemUnitsModel::class;
$GLOBALS['TL_MODELS']['tl_iao_tax_rates'] = \Srhinow\ProjectManagerBundle\Model\IaoTaxRatesModel::class;

/*
 * Setup Modules
 */
$GLOBALS['IAO_MOD'] = [
    'config' => [
        'iao_settings' => [
            'tables' => ['tl_iao_settings'],
            'icon' => PMB_PUBLIC_FOLDER.'/icons/construction.png',
        ],
        'iao_tax_rates' => [
            'tables' => ['tl_iao_tax_rates'],
            'icon' => PMB_PUBLIC_FOLDER.'/icons/calculator.png',
        ],
        'iao_item_units' => [
            'tables' => ['tl_iao_item_units'],
            'icon' => PMB_PUBLIC_FOLDER.'/icons/category.png',
        ],
    ],
    'templates' => [
        'iao_templates' => [
            'tables' => ['tl_iao_templates'],
            'icon' => PMB_PUBLIC_FOLDER.'/icons/text_templates_16.png',
        ],
        'iao_templates_items' => [
            'tables' => ['tl_iao_templates_items'],
            'icon' => PMB_PUBLIC_FOLDER.'/icons/templates_items_16.png',
        ],
    ],
];

// Enable tables in iao_setup
if (isset($_GET['do']) && 'iao_setup' === $_GET['do']) {
    foreach ($GLOBALS['IAO_MOD'] as $strGroup => $arrModules) {
        foreach ($arrModules as $strModule => $arrConfig) {
            if (\is_array($arrConfig['tables'])) {
                $GLOBALS['BE_MOD']['iao']['iao_setup']['tables'] = array_merge(
                    $GLOBALS['BE_MOD']['iao']['iao_setup']['tables'],
                    $arrConfig['tables']
                );
            }
        }
    }
}

/*
 * Frontend modules
 */
$GLOBALS['FE_MOD']['iao_fe'] = [
    'fe_iao_offer' => 'Srhinow\ProjectManagerBundle\Modules\Fe\ModuleMemberOffers',
    'fe_iao_invoice' => 'Srhinow\ProjectManagerBundle\Modules\Fe\ModuleMemberInvoices',
    'fe_iao_credit' => 'Srhinow\ProjectManagerBundle\Modules\Fe\ModuleMemberCredits',
    'fe_iao_reminder' => 'Srhinow\ProjectManagerBundle\Modules\Fe\ModuleMemberReminder',
    'fe_iao_agreement' => 'Srhinow\ProjectManagerBundle\Modules\Fe\ModuleMemberAgreements',
    'fe_iao_public_project_list' => 'Srhinow\ProjectManagerBundle\Modules\Fe\ModulePublicProjectList',
    'fe_iao_public_project_details' => 'Srhinow\ProjectManagerBundle\Modules\Fe\ModulePublicProjectDetails',
];

/*
 * HOOKS
 */
$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = [
    'srhinow.projectmanager.listener.hooks.insert_tags',
    'replaceFrontendIaoTags',
];

/*
 * Cron jobs
 */
$GLOBALS['TL_CRON']['weekly'][] = ['srhinow.projectmanager.listener.cron.crons', 'sendAgreementRemindEmail'];

/*
 * Permissions are access settings for user and groups (fields in tl_user and tl_user_group)
 */
$GLOBALS['TL_PERMISSIONS'][] = 'iaomodules';
$GLOBALS['TL_PERMISSIONS'][] = 'iaomodulep';
$GLOBALS['TL_PERMISSIONS'][] = 'iaosettings';
$GLOBALS['TL_PERMISSIONS'][] = 'iaosettingp';
