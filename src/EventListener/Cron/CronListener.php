<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Cron;

use Contao\Controller;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Email;
use Psr\Log\LogLevel;
use Srhinow\IaoAgreementsModel;

/**
 * Class DailyCron.
 */
class CronListener
{
    public function sendAgreementRemindEmail(): void
    {
        $agrObj = IaoAgreementsModel::findBy(['sendEmail=?', 'status=?'], [1, 1]); //Email-senden und status aktiv
        if (null === $agrObj) {
            return;
        }

        $today = time();
        while ($agrObj->next()) {
            //Erinnerungs-Zeitstempel aus Einstellungen generieren
            $beforeTime = strtotime($agrObj->remind_before, $agrObj->end_date);

            if ($today >= $beforeTime) {
                //send email
                $email = new Email();
                $email->from = $agrObj->email_from;
                $email->subject = $agrObj->email_subject;
                $email->text = $agrObj->email_text;
                if ($email->sendTo($agrObj->email_to)) {
                    $logger = Controller::getContainer()->get('monolog.logger.contao');
                    $logger->log(LogLevel::NOTICE, 'Vertrag-Erinnerung von '.$agrObj->title.' gesendet', ['contao' => new ContaoContext('sendAgreementRemindEmail', 'CRON')]);
                }
            }
        }
    }
}
