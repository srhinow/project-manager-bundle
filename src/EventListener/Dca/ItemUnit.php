<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Dca;

use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;

class ItemUnit extends IaoBackend
{
    /**
     * TaxRates constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * List a particular record.
     *
     * @param array
     *
     * @return string
     */
    public function listEntries($arrRow)
    {
        $return = $arrRow['name'];
        if ($arrRow['default_value']) {
            $return .= ' <span style="color:#b3b3b3; padding-left:3px;">[Standart]</span>';
        }

        return $return;
    }
}
