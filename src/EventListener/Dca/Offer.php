<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Dca;

use Contao\BackendTemplate;
use Contao\BackendUser as User;
use Contao\Database as DB;
use Contao\DataContainer;
use Contao\FilesModel;
use Contao\Image;
use Contao\Input;
use Contao\MemberModel;
use Contao\Message;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;
use setasign\Fpdi\PdfParser\PdfParserException;
use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;
use Srhinow\ProjectManagerBundle\Model\IaoInvoiceItemsModel;
use Srhinow\ProjectManagerBundle\Model\IaoInvoiceModel;
use Srhinow\ProjectManagerBundle\Model\IaoOfferItemsModel;
use Srhinow\ProjectManagerBundle\Model\IaoOfferModel;
use Srhinow\ProjectManagerBundle\Model\IaoProjectsModel;
use Srhinow\ProjectManagerBundle\Model\IaoTemplatesModel;

class Offer extends IaoBackend
{
    protected $settings = [];

    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * Check permissions to edit table tl_iao_offer.
     */
    public function checkPermission(): void
    {
        $this->checkIaoModulePermission('tl_iao_offer');
    }

    /**
     * prefill eny Fields by new dataset.
     *
     * @param string
     * @param int
     * @param array
     *
     * @throws \Exception
     */
    public function preFillFields($table, $id, $set): void
    {
        $objProject = IaoProjectsModel::findByPk($set['pid']);
        $settingId = (null !== $objProject && 0 !== $objProject->setting_id) ? $objProject->setting_id : 1;
        $settings = $this->getSettings($settingId);

        $offerId = $this->generateOfferNumber(0, $settings);
        $offerIdStr = $this->generateOfferNumberStr('', $offerId, time(), $settings);

        $set = [
            'offer_id' => $offerId,
            'offer_id_str' => $offerIdStr,
        ];

        DB::getInstance()->prepare('UPDATE '.$table.' %s WHERE `id`=?')
            ->set($set)
            ->limit(1)
            ->execute($id)
        ;
    }

    /**
     * fill date-Field if this empty.
     *
     * @param mixed
     * @param object
     *
     * @return int
     * @return int
     */
    public function generateExpiryDate($varValue, \DataContainer $dc)
    {
        $settings = $this->getSettings($dc->activeRecord->setting_id);

        if ("" === $varValue) {
            $format = ($settings['iao_offer_expiry_date']) ? : '+3 month';
            $tstamp = (int) ($dc->activeRecord->offer_tstamp) ? : time();

            $varValue = strtotime($format, $tstamp);
        }

        return  $varValue;
    }

    public function updateExpiryToTstmp(\DataContainer $dc): void
    {
        $offerObj = IaoOfferModel::findAll();

        if (\is_object($offerObj)) {
            while ($offerObj->next()) {
                if (!stripos($offerObj->expiry_date, '-')) {
                    continue;
                }

                $set = ['expiry_date' => strtotime($offerObj->expiry_date)];
                DB::getInstance()->prepare('UPDATE `tl_iao_offer` %s WHERE `id`=?')
                ->set($set)
                ->execute($offerObj->id)
            ;
            }
        }
    }

    /**
     * fill date-Field if this empty.
     */
    public function generateOfferTstamp(mixed $varValue, DataContainer $dc): int
    {
        return (0 === (int) $varValue) ? time() : $varValue;
    }

    /**
     * fill Member And Address-Text.
     *
     * @param $varValue
     * @param $dc object
     *
     * @return $value string
     */
    public function fillMemberAndAddressFields($varValue, \DataContainer $dc)
    {
        if (\strlen($varValue) < 1) {
            return $varValue;
        }

        $objProj = IaoProjectsModel::findByPk($varValue);
        if (\is_object($objProj)) {
            if ((int) $objProj->member > 0) {
                $addressText = $this->getAddressText((int) $objProj->member);

                $set = [
                    'member' => $objProj->member,
                    'address_text' => $addressText,
                ];
            } else {
                $set = [
                    'member' => '',
                    'address_text' => '',
                ];
            }

            DB::getInstance()->prepare('UPDATE `tl_iao_offer` %s WHERE `id`=?')
                ->limit(1)
                ->set($set)
                ->execute($dc->id)
            ;
        }

        return $varValue;
    }

    /**
     * fill Address-Text.
     *
     * @param $varValue mixed
     * @param $dc object
     *
     * @return string
     */
    public function fillAddressText($varValue, DataContainer $dc)
    {
        if ('1' === $varValue) {
            $intMember = Input::post('member');
            $text = $this->getAddressText((int) $intMember);

            $objOffer = IaoOfferModel::findByPk($dc->id);
            $objOffer->address_text = $text;
            $objOffer->member = $intMember;
            $objOffer->text_generate = '';
            $objOffer->save();

            $this->reload();
        }

        //leere checkbox zurueck geben
        return '';
    }

    /**
     * fill Text before if this field is empty.
     *
     * @return int | void
     */
    public function fillBeforeText(mixed $varValue, DataContainer $dc)
    {
        $varValue = (int) $varValue;

        if (1 > $varValue || $varValue === $dc->activeRecord->before_template) {
            return $varValue;
        }

        //hole das ausgewählte Template
        $objTemplate = IaoTemplatesModel::findByPk($varValue);
        if (null === $objTemplate) {
            return $varValue;
        }
        //hole den aktuellen Datensatz als DB-Object
        $objDbOffer = IaoOfferModel::findByPk($dc->id);
        if (null === $objDbOffer) {
            return $varValue;
        }

        $text = $this->changeIAOTags($objTemplate->text, 'offer', $objDbOffer);

        // schreibe das Textfeld
        $objDbOffer->before_text = $text;
        $objDbOffer->before_template = $varValue;
        $objDbOffer->save();

        return $varValue;
    }

    /**
     * fill Text after if this field is empty.
     *
     * @param $varValue integer
     * @param $dc object
     *
     * @return int | void
     */
    public function fillAfterText($varValue, \DataContainer $dc)
    {
        $varValue = (int) $varValue;

        if (1 > $varValue || $varValue === $dc->activeRecord->after_template) {
            return $varValue;
        }

        //hole das ausgewählte Template
        $objTemplate = IaoTemplatesModel::findByPk($varValue);
        if (null === $objTemplate) {
            return $varValue;
        }

        //hole den aktuellen Datensatz als DB-Object
        $objDbOffer = IaoOfferModel::findByPk($dc->id);
        if (null === $objDbOffer) {
            return $varValue;
        }

        // ersetzte evtl. Platzhalter
        $text = $this->changeIAOTags($objTemplate->text, 'offer', $objDbOffer);

        // schreibe das Textfeld
        $objDbOffer->after_text = $text;
        $objDbOffer->after_template = $varValue;
        $objDbOffer->save();

        $this->reload();
    }

    /**
     * get all template with position = 'offer_before_text'.
     *
     * @param object
     *
     * @return array
     */
    public function getBeforeTemplate(\DataContainer $dc)
    {
        $varValue = [];

        $objTemple = IaoTemplatesModel::findBy('position', 'offer_before_text');

        if (\is_object($objTemple)) {
            while ($objTemple->next()) {
                $varValue[$objTemple->id] = $objTemple->title;
            }
        }

        return $varValue;
    }

    /**
     * get all template with position = 'offer_after_text'.
     *
     * @param object
     *
     * @return array
     */
    public function getAfterTemplate(\DataContainer $dc)
    {
        $varValue = [];

        $objTemple = IaoTemplatesModel::findBy('position', 'offer_after_text');

        if (\is_object($objTemple)) {
            while ($objTemple->next()) {
                $varValue[$objTemple->id] = $objTemple->title;
            }
        }

        return $varValue;
    }

    /**
     * @param $varValue
     * @param DataContainer $dc
     * @return string
     * @throws \Exception
     */
    public function saveBeforeTextAsTemplate($varValue, DataContainer $dc): string
    {
        if (1 > (int) $varValue) {
            return $varValue;
        }

        if($this->saveTextAsTemplate(
            $dc->activeRecord->after_text,
            $dc->activeRecord->after_template,
            'offer_before_text'
        )) {
            Message::addInfo('Das Template wurde erfolgreich gespeichert');
            return '';
        }

        return $varValue;
    }

    /**
     * @param $varValue
     * @param DataContainer $dc
     * @return string
     * @throws \Exception
     */
    public function saveAfterTextAsTemplate($varValue, DataContainer $dc): string
    {
        if (1 > $varValue) {
            return $varValue;
        }

        if($this->saveTextAsTemplate(
            $dc->activeRecord->after_text,
            $dc->activeRecord->after_template,
            'offer_after_text'
        )){
            Message::addInfo('Das Template wurde erfolgreich gespeichert');
            return '';
        }

        return $varValue;
    }
    /**
     * Return the edit header button.
     */
    public function editHeader(
        array $row,
        string $href,
        string $label,
        string $title,
        string $icon,
        string $attributes): string
    {
        return ($this->User->isAdmin || \count(preg_grep('/^tl_iao_offer::/', $this->User->alexf)) > 0)
            ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" title="'
            .StringUtil::specialchars($title).'"'.$attributes.'>'.\Image::getHtml($icon, $label).'</a> '
            : '';
    }

    /**
     * generate invoice from this offer.
     */
    public function addInvoiceButton(
        array $row,
        string $href,
        string $label,
        string $title,
        string $icon): string
    {

        if (!$this->User->isAdmin) {
            return '';
        }

        if ('addInvoice' === Input::get('key') && (int) Input::get('id') > 0) {
            if ( null !== ($objInvoice = $this->addInvoiceFromOffer((int) Input::get('id')))) {
                $url = 'do=iao_invoice&mode=2&table=tl_iao_invoice&s2e=1&id='.$objInvoice->id.'&act=edit&rt='.REQUEST_TOKEN;
                $redirectUrl = $this->addToUrl($url, true,['key']);
                $this->redirect($redirectUrl);
            }
        }

        $link = (1 === (int) Input::get('onlyproj'))
            ? 'do=iao_offer&amp;id='.$row['id'].'&amp;projId='.Input::get('id')
            : 'do=iao_offer&amp;id='.$row['id'];
        $link = $this->addToUrl($href.'&amp;'.$link.'&rt='.REQUEST_TOKEN,true,['table']);

        return '<a href="'.$link.'" title="'.StringUtil::specialchars($title).'">'.\Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * Legt eine neue Rechnung mit allen Informationen aus dem Angebot an.
     *
     * @param int $offerId
     * @return IaoInvoiceModel|null
     */
    public function addInvoiceFromOffer(int $offerId): ?IaoInvoiceModel
    {
        if($offerId < 1) {
            return null;
        }

        if (null === ($objOffer = IaoOfferModel::findByPk($offerId))) {
            return null;
        }

        //Insert Invoice-Entry
        $set = [
            'pid' => (Input::get('projId')) ?: $objOffer->pid,
            'tstamp' => time(),
            'invoice_tstamp' => time(),
            'title' => $objOffer->title,
            'address_text' => $objOffer->address_text,
            'member' => $objOffer->member,
            'price_netto' => $objOffer->price_netto,
            'price_brutto' => $objOffer->price_brutto,
            'noVat' => $objOffer->noVat,
            'notice' => $objOffer->notice,
        ];

        $objInvoice = new IaoInvoiceModel();
        $objInvoice->setRow($set);
        $objInvoice->save();

        //Insert Postions for this Entry
        if ($objInvoice->id > 0) {
            $objOfferItems = IaoOfferItemsModel::findBy(['tl_iao_offer_items.pid=?'], [$objOffer->id]);

            if (null !== $objOfferItems) {
                while ($objOfferItems->next()) {
                    //Insert Invoice-Entry
                    $postenset = [
                        'pid' => $objInvoice->id,
                        'tstamp' => $objOfferItems->tstamp,
                        'type' => $objOfferItems->type,
                        'headline' => $objOfferItems->headline,
                        'headline_to_pdf' => $objOfferItems->headline_to_pdf,
                        'sorting' => $objOfferItems->sorting,
                        'date' => $objOfferItems->date,
                        'time' => $objOfferItems->time,
                        'text' => $objOfferItems->text,
                        'count' => $objOfferItems->count,
                        'amountStr' => $objOfferItems->amountStr,
                        'operator' => $objOfferItems->operator,
                        'price' => $objOfferItems->price,
                        'price_netto' => $objOfferItems->price_netto,
                        'price_brutto' => $objOfferItems->price_brutto,
                        'published' => $objOfferItems->published,
                        'vat' => $objOfferItems->vat,
                        'vat_incl' => $objOfferItems->vat_incl,
                    ];

                    $objInvoiceItem = new IaoInvoiceItemsModel();
                    $objInvoiceItem->setRow($postenset);
                    $objInvoiceItem->save();
                }
            }

            // Update the database
            $objOffer->status = '2';
            $objOffer->assumed = '1';
            $objOffer->save();

            return $objInvoice;
        }

        return null;
    }


    /**
     * wenn GET-Parameter passen dann wird ein PDF erzeugt.
     * @throws PdfParserException
     */
    public function generateOfferPDF(DataContainer $dc): void
    {
        if ('pdf' === Input::get('key') && (int) Input::get('id') > 0) {
            $this->generatePDF((int) Input::get('id'), 'offer');
        }
    }

    /**
     * Generate a "PDF" button and return it as pdf-document on Browser.
     */
    public function showPDFButton(
        array $row,
        string $href,
        string $label,
        string $title,
        string $icon): string
    {
        $settings = $this->getSettings($row['setting_id']);

        // wenn kein Admin dann kein PDF-Link
        if (!$this->User->isAdmin || \count(preg_grep('/^tl_iao_offer::/', $this->User->alexf)) > 0) {
            return '';
        }

        // Wenn keine PDF-Vorlage dann kein PDF-Link
        $objPdfTemplate = FilesModel::findByUuid($settings['iao_offer_pdf']);
        if (\strlen($objPdfTemplate->path) < 1 || !file_exists(TL_ROOT.'/'.$objPdfTemplate->path)) {
            return '';
        }  // template file not found

        $href = 'contao/main.php?do=iao_offer&amp;key=pdf&amp;id='.$row['id'];

        return '<a href="'.$href.'" title="'.specialchars($title).'">'.\Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * fill field offer_id_str if it's empty.
     */
    public function setFieldOfferNumberStr(?string $varValue, DataContainer $dc): string
    {
        if (strlen((string) $varValue) > 0) {
            return $varValue;
        }
        $settings = $this->getSettings($dc->activeRecord->setting_id);
        $tstamp = ($dc->activeRecord->tstamp) ?: time();

        return $this->generateOfferNumberStr($varValue, $dc->activeRecord->offer_id, $tstamp, $settings);
    }

    /**
     * create an offer-number-string and replace placeholder.
     *
     * @param string
     * @param int
     * @param int
     * @param array
     *
     * @return string
     */
    public function generateOfferNumberStr($varValue, $offerId, $tstamp, $settings)
    {
        if (\strlen($varValue) < 1) {
            $format = $settings['iao_offer_number_format'];
            $format = str_replace('{date}', date('Ymd', $tstamp), $format);
            $format = str_replace('{nr}', (string) $offerId, $format);
            $varValue = $format;
        }

        return $varValue;
    }

    /**
     * fill field offer_id if it's empty.
     *
     * @param string
     * @param object
     *
     * @throws \Exception
     *
     * @return string
     */
    public function setFieldOfferNumber($varValue, \DataContainer $dc)
    {
        $settings = $this->getSettings($dc->activeRecord->setting_id);

        return $this->generateOfferNumber($varValue, $settings);
    }

    /**
     * generate a offer-number if not set.
     *
     * @param mixed
     * @param object
     *
     * @throws \Exception
     *
     * @return string
     */
    public function generateOfferNumber($varValue, $settings)
    {
        $autoNr = false;
        $varValue = (int) $varValue;

        // Generate offer_id if there is none
        if (0 === $varValue) {
            $autoNr = true;
            $objNr = DB::getInstance()->prepare('SELECT `offer_id` FROM `tl_iao_offer` ORDER BY `offer_id` DESC')
                ->limit(1)
                ->execute()
            ;

            if ($objNr->numRows < 1 || 0 === $objNr->offer_id) {
                $varValue = $settings['iao_offer_startnumber'];
            } else {
                $varValue = $objNr->offer_id + 1;
            }
        } else {
            $objNr = DB::getInstance()->prepare('SELECT `offer_id` FROM `tl_iao_offer` WHERE `id`=? OR `offer_id`=?')
                ->limit(1)
                ->execute(Input::get('id'), $varValue)
            ;

            // Check whether the OfferNumber exists
            if ($objNr->numRows > 1) {
                if (!$autoNr) {
                    throw new \Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
                }

                $varValue .= '-'.Input::get('id');
            }
        }

        return $varValue;
    }

    /**
     * List a particular record.
     *
     * @param array
     *
     * @return string
     */
    public function listEntries($arrRow): string
    {
        $objMember = MemberModel::findByPk($arrRow['member']);
        $settings = $this->getSettings($arrRow['setting_id']);

        $itemTemplate = new BackendTemplate('be_dca_offer_list_item');
        $itemTemplate->arrRow = $arrRow;
        $itemTemplate->published = ($arrRow['published'] == '1') ? ' published': ' unpublished';
        $itemTemplate->price_brutto = number_format((float) $arrRow['price_brutto'], 2, ',', '.');
        $itemTemplate->currency_symbol = $settings['iao_currency_symbol'];
        $itemTemplate->arrMember = (null === $objMember) ? [] : $objMember->row();

        return $itemTemplate->parse();
    }

    /**
     * @param $row
     * @param $href
     * @param $label
     * @param $title
     * @param $icon
     * @return string
     */
    public function toggleAssumedIcon($row, $href, $label, $title, $icon): string
    {
        $href .= '&amp;id=' . $row['id'].'&amp;rt='.REQUEST_TOKEN;

        if ((int) $row['assumed']  !== 1)
        {
            $icon = 'logout.svg';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '" onclick="Backend.getScrollOffset();return AjaxRequest.toggleField(this,true)">' . Image::getHtml($icon, $label, 'data-icon="' . Image::getPath('ok.svg') . '" data-icon-disabled="' . Image::getPath('logout.svg') . '" data-state="' . ($row['assumed'] === '1' ? '1' : '0') . '"') . '</a> ';
    }

    /**
     * @param $varValue
     *
     * @return
     */
    public function updateStatus($varValue, DataContainer $dc)
    {
        if ('' === $varValue || $varValue === $dc->activeRecord->status) {
            return $varValue;
        }

        $assumed = 0;

        // Angebot angenommen
        if ($varValue === '2') {
            $assumed = 1;
        }

        // Angebot nicht angenommen
        if($varValue === '1') {
            $assumed = 0;
        }

        $set = [
            'status' => $varValue,
            'assumed' => $assumed
        ];

        DB::getInstance()->prepare('UPDATE `tl_iao_offer` %s WHERE `id`=?')
            ->set($set)
            ->execute($dc->id)
        ;

        return $varValue;
    }

    /**
     * @param $varValue
     *
     * @return string
     */
    public function updateAssumed($varValue, DataContainer $dc): string
    {
        if(null === ($objInvoice = IaoOfferModel::findByPk($dc->id))) {
            return $varValue;
        }

        $set = [
            'assumed' => $varValue,
            'status' => ($varValue === '1') ?'2' :'1',
        ];

        DB::getInstance()->prepare('UPDATE `tl_iao_offer` %s WHERE `id`=?')
            ->set($set)
            ->execute($dc->id)
        ;

        return $varValue;
    }
}
