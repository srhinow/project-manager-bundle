<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Dca;

use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;

class Settings extends IaoBackend
{
    protected $settings = [];

    /**
     * Settings constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Check permissions to edit table tl_iao_settings.
     */
    public function checkPermission(): void
    {
        $this->checkIaoSettingsPermission('tl_iao_settings');
    }

    /**
     * Return the link picker wizard.
     *
     * @return string
     */
    public function pagePicker(DataContainer $dc)
    {
        $strField = 'ctrl_'.$dc->field.(('editAll' === Input::get('act')) ? '_'.$dc->id : '');

        return ' '.Image::getHtml('pickpage.gif', $GLOBALS['TL_LANG']['MSC']['pagepicker'], 'style="vertical-align:top; cursor:pointer;" onclick="Backend.pickPage(\''.$strField.'\')"');
    }
}
