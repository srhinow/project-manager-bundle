<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Dca;

use Contao\BackendTemplate;
use Contao\BackendUser as User;
use Contao\Controller;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\DataContainer;
use Contao\FilesModel;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;
use Srhinow\ProjectManagerBundle\Model\IaoAgreementsModel;
use Srhinow\ProjectManagerBundle\Model\IaoInvoiceItemsModel;
use Srhinow\ProjectManagerBundle\Model\IaoInvoiceModel;
use Srhinow\ProjectManagerBundle\Model\IaoProjectsModel;
use Srhinow\ProjectManagerBundle\Model\IaoReminderModel;
use Srhinow\ProjectManagerBundle\Model\IaoTemplatesItemsModel;
use Srhinow\ProjectManagerBundle\Model\IaoTemplatesModel;

class Agreement extends IaoBackend
{
    protected $settings = [];

    /**
     * Agreements constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Check permissions to edit table tl_iao_agreements.
     */
    public function checkPermission(): void
    {
        $this->checkIaoModulePermission('tl_iao_agreements');
    }

    /**
     * List a particular record.
     *
     * @param array
     *
     * @return string
     */
    public function listEntries($arrRow)
    {
        $listItemTemplate = new BackendTemplate('be_dca_agreement_list_item');
        $listItemTemplate->arrRow = $arrRow;
        $listItemTemplate->beginn_date = $this->getDateStr((int) $arrRow['beginn_date']);
        $listItemTemplate->end_date = $this->getDateStr((int) $arrRow['end_date']);
        $listItemTemplate->terminated_date = $this->getDateStr((int) $arrRow['terminated_date']);
        $listItemTemplate->price_brutto = $this->getPriceStr((float) $arrRow['price_brutto']);

        return $listItemTemplate->parse();
    }

    /**
     * fill date-Field if this empty.
     *
     * @param object
     *
     * @return int
     */
    public function generateExecuteDate($varValue, DataContainer $dc)
    {
        $altdate = ($dc->activeRecord->invoice_tstamp) ? $dc->activeRecord->invoice_tstamp : time();

        return (0 === $varValue) ? $altdate : $varValue;
    }

    /**
     * fill date-Field if this empty.
     *
     * @param string
     *
     * @return string
     */
    public function getPeriodeValue($varValue, DataContainer $dc)
    {
        return ('' === $varValue) ? '+1 year' : $varValue;
    }

    /**
     * fill Adress-Text.
     *
     * @param $varValue
     * @param DataContainer $dc object
     */
    public function fillAddressText($varValue, DataContainer $dc): void
    {
        if ('' === $varValue) {
            return;
        }

        $intMember = Input::post('member');
        $text = $this->getAddressText((int) $intMember);

        $objAgreement = IaoAgreementsModel::findByPk($dc->id);
        $objAgreement->address_text = $text;
        $objAgreement->text_generate = '';
        $objAgreement->save();

        Controller::reload();
    }

    /**
     * Return the edit header button.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function editHeader($row, $href, $label, $title, $icon, $attributes)
    {
        $User = User::getInstance();

        return ($User->isAdmin || \count(preg_grep('/^tl_iao_agreements::/', $User->alexf)) > 0)
            ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id'])
            .'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> ' : '';
    }

    /**
     * Generate a button and return it as string.
     *
     * @param $row array
     * @param $href string
     * @param $label string
     * @param $title string
     * @param $icon string
     *
     * @throws \Exception
     *
     * @return string
     */
    public function addInvoice($row, $href, $label, $title, $icon)
    {
        if (!User::getInstance()->isAdmin) {
            return '';
        }

        if (null === ($objAgreement = IaoAgreementsModel::findByPk($row['id']))) {
            return '';
        }

        if ('addInvoice' === Input::get('key') && (int) Input::get('id') === (int) $row['id']) {
            //zuerst die neue range setzen damit fuer die Rechnung auch gleich der richtige Zeitraum steht

            $data['id'] = $row['id'];
            $data['beginn'] = (int) $row['beginn_date'];
            $data['end'] = (int) $row['end_date'];
            $data['periode'] = $row['periode'];
            $data['today'] = time();

            $this->generateNewCycle($data);

            $beforeTemplObj = $this->getTemplateObject('tl_iao_templates', $row['before_template']);
            $afterTemplObj = $this->getTemplateObject('tl_iao_templates', $row['after_template']);
            $invoiceId = $this->generateInvoiceNumber(0, $this->settings);
            $invoiceIdStr = $this->generateInvoiceNumberStr($invoiceId, time(), $this->settings);

            //Insert Invoice-Entry
            $set = [
                'pid' => $row['pid'],
                'tstamp' => time(),
                'invoice_tstamp' => time(),
                'invoice_id' => $invoiceId,
                'invoice_id_str' => $invoiceIdStr,
                'title' => $row['title'],
                'address_text' => $row['address_text'],
                'member' => $row['member'],
                'price_netto' => $row['price_netto'],
                'price_brutto' => $row['price_brutto'],
                'before_template' => $row['before_template'],
                'before_text' => $this->changeIAOTags($beforeTemplObj->text, 'agreement', $objAgreement),
                'after_template' => $row['after_template'],
                'after_text' => $this->changeIAOTags($afterTemplObj->text, 'agreement', $objAgreement),
                'agreement_id' => $row['id'],
            ];

            $objInvoice = new IaoInvoiceModel();
            $objInvoice->setRow($set);
            $objInvoice->save();

            $newInvoiceID = (int) $objInvoice->id;

            //Insert Postions for this Entry
            if ($newInvoiceID > 0) {
                //Posten-Template holen
                $postenTemplObj = $this->getTemplateObject('tl_iao_templates_items', $row['posten_template']);

                if (null === $postenTemplObj) {
                    $headline = $text = '';
                    $time = $date = 0;
                } else {
                    $headline = $this->changeIAOTags($postenTemplObj->headline, 'agreement', $objAgreement);
                    $date = $postenTemplObj->date;
                    $time = $postenTemplObj->time;
                    $text = $this->changeIAOTags($postenTemplObj->text, 'agreement', $objAgreement);
                }

                //Insert Invoice-Entry
                $postenset = [
                    'pid' => $newInvoiceID,
                    'tstamp' => time(),
                    'headline' => $headline,
                    'headline_to_pdf' => '1',
                    'date' => $date,
                    'time' => $time,
                    'text' => $text,
                    'count' => $row['count'],
                    'amountStr' => $row['amountStr'],
                    'price' => $row['price'],
                    'price_netto' => $row['price_netto'],
                    'price_brutto' => $row['price_brutto'],
                    'published' => '1',
                    'vat' => $row['vat'],
                    'vat_incl' => $row['vat_incl'],
                    'posten_template' => 0,
                ];

                $objInvoiceItem = new IaoInvoiceItemsModel();
                $objInvoiceItem->setRow($postenset);
                $objInvoiceItem->save();

                if ($objInvoiceItem->id < 1) {
                    throw new \Exception('Es konnte kein Rechnungs-Element angelegt werden.');
                }

                $request = 'do=iao_invoice&mode=2&table=tl_iao_invoice&s2e=1&id='.$newInvoiceID
                    .'&act=edit&rt='.REQUEST_TOKEN;
                $redirectUrl = $this->addToUrl($request);
                $redirectUrl = str_replace('key=addInvoice&amp;', '', $redirectUrl);
                $this->redirect($redirectUrl);
            }
        }

        //Button erzeugen
        $link = (1 === (int) Input::get('onlyproj'))
            ? 'do=iao_agreements&amp;id='.$row['id'].'&amp;projId='.Input::get('id')
            : 'do=iao_agreements&amp;id='.$row['id'].'';
        $link = $this->addToUrl($href.'&amp;'.$link.'&rt='.REQUEST_TOKEN);
        $link = str_replace('table=tl_iao_agreements&amp;', '', $link);

        return '<a href="'.$link.'" title="'.StringUtil::specialchars($title).'">'.\Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * save the price_netto and price_brutto from actual item.
     *
     * @return void;
     */
    public function saveNettoAndBrutto(DataContainer $dc)
    {
        // Return if there is no active record (override all)
        if (!$dc->activeRecord) {
            return;
        }

        $englprice = str_replace(',', '.', $dc->activeRecord->price);


        if (1 === (int) $dc->activeRecord->vat_incl) {
            $Netto = (float) $englprice;
            $Brutto = $this->getBruttoPrice((float) $englprice, $dc->activeRecord->vat);
        } else {
            $Netto = $this->getNettoPrice((float) $englprice, $dc->activeRecord->vat);
            $Brutto = (float) $englprice;
        }

        $nettoSum = round($Netto, 2) * $dc->activeRecord->count;
        $bruttoSum = round($Brutto, 2) * $dc->activeRecord->count;

        $objAgreement = IaoAgreementsModel::findByPk($dc->id);
        $objAgreement->price_netto = $nettoSum;
        $objAgreement->price_brutto = $bruttoSum;
        $objAgreement->save();
    }

    /**
     * Return the "toggle visibility" button.
     *
     * @param $row array
     * @param $href string
     * @param $label string
     * @param $title string
     * @param $icon string
     * @param $attributes string
     *
     * @return string
     */
    public function toggleIcon($row = [], $href = '', $label = '', $title = '', $icon = '', $attributes = '')
    {
        $this->import('BackendUser', 'User');

        if (\strlen(Input::get('tid'))) {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state')));
            $this->redirect($this->getReferer());
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.(1 === $row['status'] ? 2 : 1);

        if (2 === $row['status']) {
            $icon = 'logout.gif';
        }

        return '<a href="'.$this->addToUrl($href).'" title="'
            .$GLOBALS['TL_LANG']['tl_iao_agreements']['toggle'].'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * paid/not paid.
     *
     * @param int
     * @param bool
     */
    public function toggleVisibility($intId, $blnVisible): void
    {
        // Check permissions to edit
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        $logger = static::getContainer()->get('monolog.logger.contao');

        // Check permissions to publish
        $User = User::getInstance();
        if (!$User->isAdmin && !$User->hasAccess('tl_iao_agreements::status', 'alexf')) {
            $logger->log(
                'Not enough permissions to publish/unpublish comment ID "'.$intId.'"',
                'tl_iao_agreements toggleActivity',
                ['contao' => new ContaoContext(__CLASS__.'::'.__METHOD__, 'ERROR')]
            );
            $this->redirect('contao/main.php?act=error');
        }

        $objVersions = new \Versions('tl_iao_agreements', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_iao_agreements']['fields']['status']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_iao_agreements']['fields']['status']['save_callback'] as $callback) {
                $this->import($callback[0]);
                $blnVisible = $this->$callback[0]->$callback[1]($blnVisible, $this);
            }
        }

        $visibility = (1 === $blnVisible) ? '1' : '2';
        $objAgreement = IaoAgreementsModel::findByPk($intId);

        // Update the database
        $objAgreement->status = $visibility;
        $objAgreement->save();

        //get reminder-Data
        $remindObj = IaoReminderModel::findByPk($intId);

        if (\is_object($remindObj)) {
            $objInvoice = IaoInvoiceModel::findByPk($remindObj->invoice_id);
            $objInvoice->status = $visibility;
            $objInvoice->notice = $objInvoice->notice + $remindObj->notice;
            $objInvoice->save();
        }

        $objVersions->create();
    }

    /**
     * @param $varValue
     *
     * @return int
     */
    public function getAgreementValue($varValue, DataContainer $dc)
    {
        return ('0' === $varValue) ? time() : $varValue;
    }

    /**
     * @param $varValue
     *
     * @return int
     */
    public function getBeginnDateValue($varValue, DataContainer $dc)
    {
        $agreement_date = ($dc->activeRecord->agreement_date) ? $dc->activeRecord->agreement_date : time();
        $beginn_date = ('' === $varValue) ? $agreement_date : $varValue;
        $end_date = $this->getEndDateValue($dc->activeRecord->end_date, $dc);

        $objAgreement = IaoAgreementsModel::findByPk($dc->id);
        $objAgreement->beginn_date = $beginn_date;
        $objAgreement->end_date = $end_date;
        $objAgreement->save();

        return $beginn_date;
    }

    /**
     * @param $varValue
     *
     * @return false|int
     */
    public function getEndDateValue($varValue, DataContainer $dc)
    {
        if ('' !== $varValue) {
            return $varValue;
        }

        $agreement_date = ($dc->activeRecord->agreement_date)
            ? $dc->activeRecord->agreement_date
            : time();

        $beginn_date = ($dc->activeRecord->beginn_date)
            ? $dc->activeRecord->beginn_date
            : $agreement_date;

        $periode = ($dc->activeRecord->periode)
            ? $dc->activeRecord->periode
            : $GLOBALS['IAO']['default_agreement_cycle'];

        // wenn der Wert nicht manuell verändert wurde die Periode berechnen
        return ($varValue === $dc->activeRecord->end_date)
            ? strtotime($periode.' -1 day', $beginn_date)
            : $varValue;
    }

    /**
     * Generate a "PDF" button and return it as string.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function showPDF($row, $href, $label, $title, $icon)
    {
        if (!User::getInstance()->isAdmin || null === $row['agreement_pdf_file']) {
            return '';
        }

        // Wenn keine PDF-Vorlage dann kein PDF-Link
        $objPdf = FilesModel::findByUuid($row['agreement_pdf_file']);
        if (\strlen($objPdf->path) < 1 || !file_exists(TL_ROOT.'/'.$objPdf->path)) {
            return false;
        }  // template file not found

        $pdfFile = TL_ROOT.'/'.$objPdf->path;

        if ('pdf' === Input::get('key') && (int) Input::get('id') === $row['id']) {

            if (!empty($row['agreement_pdf_file']) && file_exists($pdfFile)) {
                header('Content-type: application/pdf');
                header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
                header('Content-Length: '.filesize($pdfFile));
                header('Content-Disposition: inline; filename="'.basename($pdfFile).'";');
                ob_clean();
                flush();
                readfile($pdfFile);
                exit();
            }
        }
        $href = $this->addToUrl($href.'&amp;id='.$row['id']);
        $href = str_replace('&amp;onlyproj=1', '', $href);
        $href = str_replace('do=iao_projects&amp;', 'do=iao_agreements&amp;', $href);
        $href = str_replace('table=tl_iao_agreements&amp;', '', $href);

        return (!empty($row['agreement_pdf_file']) && file_exists($pdfFile))
            ? '<a href="'.$href.'" title="'.StringUtil::specialchars($title).'">'.Image::getHtml($icon, $label).'</a> '
            : '';
    }

    /**
     * @param $data
     */
    public function generateNewCycle($data = []): void
    {
        if (!$data['end'] || !$data['beginn']) {
            return;
        }

        if ('' === $data['periode']) {
            $data['periode'] = $GLOBALS['IAO']['default_agreement_cycle'];
        }

        $new_beginn = strtotime($data['periode'], (int) $data['beginn']);

        $objAgreement = IaoAgreementsModel::findByPk($data['id']);
        $objAgreement->beginn_date = $new_beginn;
        $objAgreement->end_date = strtotime($data['periode'].' -1 day', $new_beginn);
        $objAgreement->new_generate = '';
        $objAgreement->save();
    }

    /**
     * @param $varValue
     *
     * @return string
     */
    public function setNewCycle($varValue, DataContainer $dc)
    {
        if (1 === $varValue) {
            $data['id'] = $dc->id;
            $data['beginn'] = (int) $dc->activeRecord->beginn_date;
            $data['end'] = (int) $dc->activeRecord->end_date;
            $data['periode'] = $dc->activeRecord->periode;
            $data['today'] = time();

            $this->generateNewCycle($data);
        }

        return '';
    }

    /**
     * @return array
     */
    public function getPostenTemplate(\DataContainer $dc)
    {
        $varValue = [];

        $objTemplateItems = IaoTemplatesItemsModel::findBy(['`position`=?'], ['invoice']);
        if (null === $objTemplateItems) {
            return $varValue;
        }

        while ($objTemplateItems->next()) {
            $varValue[$objTemplateItems->id] = $objTemplateItems->headline;
        }

        return $varValue;
    }

    /**
     * get all invoice before template.
     *
     * @param object
     *
     * @return array
     */
    public function getBeforeTemplate(DataContainer $dc)
    {
        $varValue = [];

        $objTemplate = IaoTemplatesModel::findBy(['`position`=?'], ['invoice_before_text']);
        if (null === $objTemplate) {
            return $varValue;
        }

        while ($objTemplate->next()) {
            $varValue[$objTemplate->id] = $objTemplate->title;
        }

        return $varValue;
    }

    /**
     * get all invoice after template.
     *
     * @param object
     *
     * @return array
     */
    public function getAfterTemplate(DataContainer $dc)
    {
        $varValue = [];

        $objTemplate = IaoTemplatesModel::findBy(['`position`=?'], ['invoice_after_text']);
        if (null === $objTemplate) {
            return $varValue;
        }

        while ($objTemplate->next()) {
            $varValue[$objTemplate->id] = $objTemplate->title;
        }

        return $varValue;
    }

    public function fillEmptyMember(DataContainer $dc): void
    {
        $objAgreements = IaoAgreementsModel::findBy(['tl_iao_agreements.member=?'], ['']);
        if (null === $objAgreements) {
            return;
        }

        while ($objAgreements->next()) {
            $objProject = IaoProjectsModel::findByPk($objAgreements->pid);
            if (null === $objProject) {
                continue;
            }

            $objAgreements->member = $objProject->member;
            $objAgreements->save();
        }
    }
}
