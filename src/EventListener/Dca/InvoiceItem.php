<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Dca;

use Contao\BackendUser as User;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\Database as DB;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;
use Srhinow\ProjectManagerBundle\Model\IaoInvoiceItemsModel;
use Srhinow\ProjectManagerBundle\Model\IaoInvoiceModel;
use Srhinow\ProjectManagerBundle\Model\IaoTemplatesItemsModel;

class InvoiceItem extends IaoBackend
{
    protected $settings = [];

    protected $user;

    /**
     * InvoiceItems constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setIaoSettings();
        $this->user = User::getInstance();
    }

    /**
     * get all setting for parent invoice-element.
     */
    public function setIaoSettings(): void
    {
        $id = (int) Input::get('id');

        if (0 < $id) {
            $objInvoice = IaoInvoiceModel::findByPk($id);

            $this->settings = (null === $objInvoice) ? [] : $this->getSettings($objInvoice->setting_id);
        }
    }

    /**
     * @param $href
     * @param $label
     * @param $title
     * @param $class
     *
     * @return string|void
     */
    public function showPDFButton($href, $label, $title, $class)
    {
        $objPdfTemplate = \FilesModel::findByUuid($this->settings['iao_invoice_pdf']);

        if (null === $objPdfTemplate->path || !file_exists(TL_ROOT.'/'.$objPdfTemplate->path)) {
            return;
        }  // template file not found

        return '&nbsp; :: &nbsp;<a href="contao/main.php?do=iao_invoice&table=tl_iao_invoice&'.$href.'" title="'
            .StringUtil::specialchars($title).'" class="'.$class.'">'.$label.'</a> ';
    }

    /**
     * Check permissions to edit table tl_iao_invoice_items.
     */
    public function checkPermission(): void
    {
        $this->checkIaoModulePermission('tl_iao_invoice_items');
    }

    /**
     * Add the type of input field.
     *
     * @param array
     *
     * @return string
     */
    public function listItems($arrRow)
    {
        if ('devider' === $arrRow['type']) {
            return '<div class="pdf-devider"><span>PDF-Trenner</span></div>';
        }

        $key = ($arrRow['published']) ? ' published' : ' unpublished';
        $pagebreak = (1 === $arrRow['pagebreak_after']) ? ' pagebreak' : '';
        $arrSettings = $this->getSettings();

        return '<div class="cte_type'.$key.$pagebreak.'"><strong>'.$arrRow['headline'].'</strong></div>
		 	<div class="limit_height h100"><p>
		 	Netto: '.number_format((float) $arrRow['price_netto'], 2, ',', '.').$arrSettings['iao_currency_symbol'].'
		 	<br />Brutto: '.number_format((float) $arrRow['price_brutto'], 2, ',', '.').$arrSettings['iao_currency_symbol'].' (inkl. '.$arrRow['vat'].'% MwSt.)
		 	</p>
		 	'.$arrRow['text'].'
		 	</div>'."\n";
    }

    /**
     * save the price from all items in parent_table.
     *
     * @param mixed
     * @param object
     */
    public function saveAllPricesToParent(DataContainer $dc): void
    {
        // Return if there is no active record (override all)
        if (!$dc->activeRecord) {
            return;
        }

        $itemObj = DB::getInstance()->prepare('SELECT `price`,`count`,`vat`,`vat_incl`,`operator` FROM `tl_iao_invoice_items` WHERE `pid`=? AND published =?')
            ->execute($dc->activeRecord->pid, 1)
        ;

        if ($itemObj->numRows > 0) {
            $allNetto = 0;
            $allBrutto = 0;

            while ($itemObj->next()) {
                $englprice = (float) str_replace(',', '.', $itemObj->price);
                $priceSum = $englprice * (float) $itemObj->count;

                //if MwSt inclusive
                if (1 === (int) $itemObj->vat_incl) {
                    $Netto = $priceSum;
                    $Brutto = $this->getBruttoPrice($priceSum, (float) $itemObj->vat);
                } else {
                    $Netto = $this->getNettoPrice($priceSum, (float) $itemObj->vat);
                    $Brutto = $priceSum;
                }

                //which operator is set?
                if ('-' === $itemObj->operator) {
                    $allNetto -= $Netto;
                    $allBrutto -= $Brutto;
                } else {
                    $allNetto += $Netto;
                    $allBrutto += $Brutto;
                }

                DB::getInstance()->prepare('
                UPDATE `tl_iao_invoice` 
                SET `price_netto`=?, `price_brutto`=? 
                WHERE `id`=?')
                    ->limit(1)
                    ->execute($allNetto, $allBrutto, $dc->activeRecord->pid)
                ;
            }
        }
    }

    /**
     * save the price_netto and price_brutto from actuell item.
     *
     * @param mixed
     * @param object
     */
    public function saveNettoAndBrutto(DataContainer $dc): void
    {
        // Return if there is no active record (override all)
        if (!$dc->activeRecord) {
            return;
        }

        //von den Haupteinstellungen holen ob diese MwSt befreit ist, dann Brutto und Netto gleich setzen.
        $invoiceObj = DB::getInstance()->prepare('SELECT * FROM `tl_iao_invoice` WHERE `id`=?')
            ->limit(1)
            ->execute($dc->activeRecord->pid)
        ;

        $englprice = (float) str_replace(',', '.', $dc->activeRecord->price);

        if (1 === (int) $dc->activeRecord->vat_incl) {
            $Netto = $englprice;
            $Brutto = (float) $this->getBruttoPrice($englprice, (float) $dc->activeRecord->vat);
        } else {
            $Netto = (float) $this->getNettoPrice($englprice, (float) $dc->activeRecord->vat);
            $Brutto = $englprice;
        }

        if ($invoiceObj->noVat) {
            $Netto = (float) $englprice;
            $Brutto = (float) $englprice;
        }

        $nettoSum = round($Netto, 2) * (float) $dc->activeRecord->count;
        $bruttoSum = round($Brutto, 2) * (float) $dc->activeRecord->count;

        DB::getInstance()->prepare('
        UPDATE `tl_iao_invoice_items` 
        SET `price_netto`=?, `price_brutto`=? 
        WHERE `id`=?')
            ->limit(1)
            ->execute($nettoSum, $bruttoSum, $dc->id)
        ;
    }

    /**
     * Return the link picker wizard.
     *
     * @param object
     *
     * @return string
     */
    public function pagePicker(DataContainer $dc)
    {
        $strField = 'ctrl_'.$dc->field.(('editAll' === $this->Input->get('act')) ? '_'.$dc->id : '');

        return ' '.Image::getHtml(
            'pickpage.gif',
            $GLOBALS['TL_LANG']['MSC']['pagepicker'],
            'style="vertical-align:top; cursor:pointer;" onclick="Backend.pickPage(\''.$strField.'\')"');
    }

    /**
     * Return the "toggle visibility" button.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        $href .= '&amp;id=' . $row['id'];

        if (!$row['published'])
        {
            $icon = 'invisible.svg';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '" onclick="Backend.getScrollOffset();return AjaxRequest.toggleField(this,true)">' . Image::getHtml($icon, $label, 'data-icon="' . Image::getPath('visible.svg') . '" data-icon-disabled="' . Image::getPath('invisible.svg') . '"data-state="' . ($row['published'] ? 1 : 0) . '"') . '</a> ';
    }

    /**
     * Disable/enable a user group.
     *
     * @param integer              $intId
     * @param boolean              $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null): void
    {
        // Check permissions to edit
        $this->Input->setGet('id', $intId);
        $this->Input->setGet('act', 'toggle');

        if ($dc)
        {
            $dc->id = $intId; // see #8043
        }


        // Trigger the onload_callback
        if (is_array($GLOBALS['TL_DCA']['tl_content']['config']['onload_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_content']['config']['onload_callback'] as $callback)
            {
                if (is_array($callback))
                {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                }
                elseif (is_callable($callback))
                {
                    $callback($dc);
                }
            }
        }

        // Check permissions to publish
        $User = User::getInstance();
        if (!$User->hasAccess('tl_iao_invoice_items::published', 'alexf')) {
            throw new AccessDeniedException('Not enough permissions to publish/unpublish form invoice item ID ' . $intId . '.');
        }

        $objRow = DB::getInstance()->prepare("SELECT * FROM tl_iao_invoice_items WHERE id=?")
            ->limit(1)
            ->execute($intId);

        if ($objRow->numRows < 1)
        {
            throw new AccessDeniedException('Invalid invoice item ID ' . $intId . '.');
        }

        // Set the current record
        if ($dc)
        {
            $dc->activeRecord = $objRow;
        }

        $objVersions = new \Versions('tl_iao_invoice_items', $intId);
        $objVersions->initialize();

        // Reverse the logic (form fields have invisible=1)
        $blnVisible = !$blnVisible;

        // Trigger the save_callback
        if (is_array($GLOBALS['TL_DCA']['tl_content']['fields']['invisible']['save_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_content']['fields']['invisible']['save_callback'] as $callback)
            {
                if (is_array($callback))
                {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, $dc);
                }
                elseif (is_callable($callback))
                {
                    $blnVisible = $callback($blnVisible, $dc);
                }
            }
        }

        $time = time();

        // Update the database
        DB::getInstance()->prepare("UPDATE tl_iao_invoice_items SET tstamp=$time, published='".($blnVisible ? '1' : '')."' WHERE id=?")
            ->execute($intId)
        ;

        if ($dc)
        {
            $dc->activeRecord->tstamp = $time;
            $dc->activeRecord->published = ($blnVisible ? '1' : '');
        }

        // Trigger the onsubmit_callback
        if (is_array($GLOBALS['TL_DCA']['tl_content']['config']['onsubmit_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_content']['config']['onsubmit_callback'] as $callback)
            {
                if (is_array($callback))
                {
                    $this->import($callback[0]);
                    $this->{$callback[0]}->{$callback[1]}($dc);
                }
                elseif (is_callable($callback))
                {
                    $callback($dc);
                }
            }
        }

        $objVersions->create();

        if ($dc)
        {
            $dc->invalidateCacheTags();
        }
    }

    /**
     * Generate a button to put a posten-template for invoices.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function addPostenTemplate($row, $href, $label, $title, $icon, $attributes)
    {
        $href .= '&amp;ptid='.$row['id'];
        $strButton = '<a href="'.$this->addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> ';

        if (!$this->user->isAdmin) {
            return '';
        }

        if ('addPostenTemplate' === Input::get('key') && Input::get('ptid') === $row['id']) {
            $objItem = IaoInvoiceItemsModel::findByPk($row['id']);
            if (null === $objItem) {
                return $strButton;
            }

            $templateItemId = $this->createTemplateItem($objItem, 'invoice');

            if (0 < $templateItemId) {
                $this->redirectToTemplateItem($templateItemId);
            }
        }

        return $strButton;
    }

    /**
     * get all invoice-posten-templates.
     *
     * @return array
     */
    public function getPostenTemplate(DataContainer $dc)
    {
        $options = [];

        $objTemplate = IaoTemplatesItemsModel::findBy(['`position`=?'], ['invoice']);
        if (null === $objTemplate) {
            return $options;
        }

        while ($objTemplate->next()) {
            $options[$objTemplate->id] = $objTemplate->headline;
        }

        return $options;
    }

    /**
     * fill Text before.
     *
     * @param int           $varValue
     * @param DataContainer $dc
     *
     * @return int|void
     */
    public function fillPostenFields($varValue, $dc)
    {
        if ((int) $varValue <= 0) {
            return $varValue;
        }

        //Posten-Template holen
        $postenset = $this->getTemplateItemValues($varValue);

        if (!\is_array($postenset) || \count($postenset) < 1) {
            return $varValue;
        }

        $objInvoiceItem = IaoInvoiceItemsModel::findByPk($dc->id);
        if (null === $objInvoiceItem) {
            return $varValue;
        }

        //Insert Invoice-Entry
        $objInvoiceItem->tstamp = time();
        $objInvoiceItem->headline = $this->changeIAOTags($postenset['headline'], 'invoice', $objInvoiceItem);
        $objInvoiceItem->headline_to_pdf = $postenset['headline_to_pdf'];
        $objInvoiceItem->text = $this->changeIAOTags($postenset['text'], 'invoice', $objInvoiceItem);
        $objInvoiceItem->count = $postenset['count'];
        $objInvoiceItem->amountStr = $postenset['amountStr'];
        $objInvoiceItem->operator = $postenset['operator'];
        $objInvoiceItem->price = $postenset['price'];
        $objInvoiceItem->price_netto = $postenset['price_netto'];
        $objInvoiceItem->price_brutto = $postenset['price_brutto'];
        $objInvoiceItem->vat = $postenset['vat'];
        $objInvoiceItem->vat_incl = $postenset['vat_incl'];
        $objInvoiceItem->save();

        $this->reload();
    }

    /**
     * calculate and update fields.
     */
    public function updateRemaining(DataContainer $dc): void
    {
        $itemObj = DB::getInstance()->prepare('SELECT SUM(`price_netto`) as `brutto_sum` FROM `tl_iao_invoice_items` WHERE `pid`=? AND `published`=?')
            ->execute($dc->activeRecord->pid, 1)
        ;

        $sumObj = $itemObj->fetchRow();

        if ($itemObj->numRows > 0) {
            $parentObj = DB::getInstance()->prepare('SELECT `paid_on_dates` FROM `tl_iao_invoice` WHERE `id`=?')
                ->limit(1)
                ->execute($dc->activeRecord->pid)
            ;

            $paidsArr = unserialize($parentObj->paid_on_dates);
            $already = 0;
            $lastPayDate = '';

            if (\is_array($paidsArr) && ('' !== $paidsArr[0]['payamount'])) {
                foreach ($paidsArr as $k => $a) {
                    $already += $a['payamount'];
                    $lastPayDate = $a['paydate'];
                }
            }

            $dif = $dc->activeRecord->price_brutto - $already;
            $status = ($dc->activeRecord->price_brutto === $already && $dc->activeRecord->price_brutto > 0) ? 2 : $dc->activeRecord->status;
            $paid_on_date = ($dc->activeRecord->price_brutto === $already) ? $lastPayDate : $dc->activeRecord->paid_on_date;

            $set = [
                'remaining' => $dif,
                'status' => $status,
                'paid_on_date' => $paid_on_date,
            ];

            DB::getInstance()->prepare('UPDATE `tl_iao_invoice` %s WHERE `id`=?')
                ->set($set)
                ->execute($dc->id)
            ;
        }
    }
}
