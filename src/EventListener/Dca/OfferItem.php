<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Dca;

use Contao\BackendTemplate;
use Contao\BackendUser as User;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;
use Srhinow\ProjectManagerBundle\Model\IaoOfferItemsModel;
use Srhinow\ProjectManagerBundle\Model\IaoOfferModel;
use Srhinow\ProjectManagerBundle\Model\IaoTemplatesItemsModel;

class OfferItem extends IaoBackend
{
    protected $settings = [];

    protected $user;
    /**
     * OfferItems constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setIaoSettings();
        $this->user = User::getInstance();
    }

    /**
     * get all setting for parent offer-element.
     */
    public function setIaoSettings(): void
    {
        $id = Input::get('id');
        if ($id) {
            $objOffer = IaoOfferModel::findByPk($id);

            $this->settings = (null === $objOffer) ? [] : $this->getSettings($objOffer->setting_id);
        }
    }

    /**
     * @param $href
     * @param $label
     * @param $title
     * @param $class
     *
     * @return string
     */
    public function showPDFButton($href, $label, $title, $class)
    {
        $objPdfTemplate = \FilesModel::findByUuid($this->settings['iao_offer_pdf']);

        if (\strlen($objPdfTemplate->path) < 1 || !file_exists(TL_ROOT.'/'.$objPdfTemplate->path)) {
            return '';
        }  // template file not found

        return '&nbsp; :: &nbsp;<a href="contao/main.php?do=iao_offer&table=tl_iao_offer&'.$href.'" title="'
            .StringUtil::specialchars($title).'" class="'.$class.'">'.$label.'</a> ';
    }

    /**
     * Check permissions to edit table tl_iao_offer_items.
     */
    public function checkPermission(): void
    {
        $this->checkIaoModulePermission('tl_iao_offer_items');
    }

    /**
     * Add the type of input field.
     *
     * @param array
     *
     * @return string
     */
    public function listItems($arrRow)
    {
        if ('devider' === $arrRow['type']) {
            $itemTemplate = new BackendTemplate('be_dca_devider');

            return $itemTemplate->parse();
        }

        // Classes
        $class = ($arrRow['published']) ? ' published' : ' unpublished';

        $itemTemplate = new BackendTemplate('be_dca_offeritem_list_item');
        $itemTemplate->arrRow = $arrRow;
        $itemTemplate->class = $class;
        $itemTemplate->currency_symbol = $this->settings['iao_currency_symbol'];
        $itemTemplate->format_netto = number_format((float) $arrRow['price_netto'], 2, ',', '.');
        $itemTemplate->format_brutto = number_format((float) $arrRow['price_brutto'], 2, ',', '.');

        return $itemTemplate->parse();
    }

    /**
     * save the price from all items in parent_table.
     *
     * @param mixed
     * @param object
     *
     * @return string
     */
    public function saveAllPricesToParent(DataContainer $dc): void
    {
        // Return if there is no active record (override all)
        if (!$dc->activeRecord) {
            return;
        }

        $objItems = IaoOfferItemsModel::findPublishedItemsByOfferId($dc->activeRecord->pid);

        if (null === $objItems) {
            return;
        }

        $allNetto = 0;
        $allBrutto = 0;

        while ($objItems->next()) {
            $englprice = str_replace(',', '.', $objItems->price);
            $priceSum = $englprice * $objItems->count;

            if (1 === (int) $objItems->vat_incl) {
                $allNetto += $priceSum;
                $allBrutto += $this->getBruttoPrice($priceSum, $objItems->vat);
            } else {
                $allNetto += $this->getNettoPrice($priceSum, $objItems->vat);
                $allBrutto += $priceSum;
            }
        }

        $objOffer = IaoOfferModel::findByPk($dc->activeRecord->pid);
        $objOffer->price_netto = $allNetto;
        $objOffer->price_brutto = $allBrutto;
        $objOffer->save();
    }

    /**
     * save the price_netto and price_brutto from actuell item.
     */
    public function saveNettoAndBrutto(DataContainer $dc): void
    {
        // Return if there is no active record (override all)
        if (!$dc->activeRecord) {
            return;
        }

        $englprice = str_replace(',', '.', $dc->activeRecord->price);
        $priceSum = $englprice * $dc->activeRecord->count;

        if (1 === (int) $dc->activeRecord->vat_incl) {
            $Netto = $priceSum;
            $Brutto = $this->getBruttoPrice($priceSum, $dc->activeRecord->vat);
        } else {
            $Netto = $this->getNettoPrice($priceSum, $dc->activeRecord->vat);
            $Brutto = $priceSum;
        }

        $objItem = IaoOfferItemsModel::findByPk($dc->id);
        $objItem->price_netto = $Netto;
        $objItem->price_brutto = $Brutto;
        $objItem->save();
    }

    /**
     * Return the "toggle visibility" button.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (Input::get('tid') && \strlen(Input::get('tid'))) {
            $this->toggleVisibility(Input::get('tid'), ('1' === Input::get('state')));
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!$this->user->isAdmin && !$this->user->hasAccess('tl_iao_offer_items::published', 'alexf')) {
            return '';
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);

        if (!$row['published']) {
            $icon = 'invisible.gif';
        }

        return '<a href="'.$this->addToUrl($href).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * Disable/enable a user group.
     *
     * @param int
     * @param bool
     */
    public function toggleVisibility($intId, $blnVisible): void
    {
        // Check permissions to edit
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');
        $this->checkPermission();

        // Check permissions to publish
        if (!$this->user->isAdmin && !$this->user->hasAccess('tl_iao_offer_items::published', 'alexf')) {
            $logger = static::getContainer()->get('monolog.logger.contao');
            $logger->log(
                'Not enough permissions to publish/unpublish event ID "'.$intId.'"',
                'tl_iao_offer_items toggleVisibility',
                ['contao' => new ContaoContext(__CLASS__.'::'.__METHOD__, 'ERROR')]
            );
            $this->redirect('contao/main.php?act=error');
        }

        $objVersions = new \Versions('tl_iao_offer_items', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_iao_offer_items']['fields']['published']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_iao_offer_items']['fields']['published']['save_callback'] as $callback) {
                $this->import($callback[0]);
                $blnVisible = $this->$callback[0]->$callback[1]($blnVisible, $this);
            }
        }

        // Update the database
        $objItem = IaoOfferItemsModel::findByPk($intId);
        $objItem->tstamp = time();
        $objItem->published = $blnVisible ? 1 : '';
        $objItem->save();

        $objVersions->create();
    }

    /**
     * Generate a button to put a posten-template for offer.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function addPostenTemplate($row, $href, $label, $title, $icon, $attributes)
    {
        $href .= '&amp;ptid='.$row['id'];
        $strButton = '<a href="'.$this->addToUrl($href).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a>';

        if (!$this->user->isAdmin) {
            return '';
        }

        if ('addPostenTemplate' === Input::get('key') && Input::get('ptid') === $row['id']) {
            $objItem = IaoOfferItemsModel::findByPk($row['id']);
            if (null === $objItem) {
                return $strButton;
            }

            $templateItemId = $this->createTemplateItem($objItem, 'offer');

            if (0 < $templateItemId) {
                $this->redirectToTemplateItem($templateItemId);
            }
        }

        return $strButton;
    }

    /**
     * get all offer-posten-templates.
     *
     * @return array
     */
    public function getPostenTemplate(DataContainer $dc)
    {
        $options = [];

        $objTemplate = IaoTemplatesItemsModel::findBy(['`position`=?'], ['offer']);
        if (null === $objTemplate) {
            return $options;
        }

        while ($objTemplate->next()) {
            $options[$objTemplate->id] = $objTemplate->headline;
        }

        return $options;
    }

    /**
     * fill Offer-Item from template.
     *
     * @param int           $varValue
     * @param DataContainer $dc
     *
     * @return int|void
     */
    public function fillPostenFields($varValue, $dc)
    {
        if ((int) $varValue < 1) {
            return $varValue;
        }

        $postenset = $this->getTemplateItemValues($varValue);

        if (!\is_array($postenset) || \count($postenset) < 1) {
            return $varValue;
        }

        $objOfferItem = IaoOfferItemsModel::findByPk($dc->id);
        if (null === $objOfferItem) {
            return $varValue;
        }

        $objOfferItem->tstamp = time();
        $objOfferItem->headline = $postenset['headline'];
        $objOfferItem->headline_to_pdf = $postenset['headline_to_pdf'];
        $objOfferItem->text = $postenset['text'];
        $objOfferItem->count = $postenset['count'];
        $objOfferItem->amountStr = $postenset['amountStr'];
        $objOfferItem->operator = $postenset['operator'];
        $objOfferItem->price = $postenset['price'];
        $objOfferItem->price_netto = $postenset['price_netto'];
        $objOfferItem->price_brutto = $postenset['price_brutto'];
        $objOfferItem->vat = $postenset['vat'];
        $objOfferItem->vat_incl = $postenset['vat_incl'];
        $objOfferItem->save();

        $this->reload();
    }
}
