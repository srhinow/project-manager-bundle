<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Dca;

use Contao\BackendTemplate;
use Contao\BackendUser as User;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Database as DB;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\MemberModel;
use Contao\StringUtil;
use Psr\Log\LogLevel;
use setasign\Fpdi\PdfParser\PdfParserException;
use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;
use Srhinow\ProjectManagerBundle\Model\IaoInvoiceModel;
use Srhinow\ProjectManagerBundle\Model\IaoReminderModel;

class Reminder extends IaoBackend
{
    protected $settings = [];

    /**
     * Reminder constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Check permissions to edit table tl_iao_reminder.
     */
    public function checkPermission(): void
    {
        $this->checkIaoModulePermission('tl_iao_reminder');
    }

    /**
     * fill date-Field if this empty.
     *
     * @param $varValue mixed
     * @param DataContainer $dc object
     *
     * @return int
     */
    public function generateExecuteDate(mixed $varValue, DataContainer $dc): int
    {
        $altDate = ($dc->activeRecord->invoice_tstamp) ? $dc->activeRecord->invoice_tstamp : time();

        return (int) (0 === $varValue) ? $altDate : $varValue;
    }

    /**
     * fill date-Field if this empty.
     *
     * @param $varValue mixed
     * @param DataContainer $dc object
     *
     * @return int
     */
    public function generateReminderTstamp(mixed $varValue, DataContainer $dc): int
    {
        return (int) (0 === $varValue) ? time() : $varValue;
    }

    /**
     * get all invoices.
     *
     * @return array
     */
    public function getInvoices(DataContainer $dc): array
    {
        $varValue = [];
        if(null === $dc->activeRecord){
            return $varValue;
        }

        $settings = $this->getSettings($dc->activeRecord->setting_id);

        if ($dc->activeRecord->pid > 0) {
            $all = DB::getInstance()
                ->prepare('
                SELECT `i`.*, `m`.`company` 
                FROM `tl_iao_invoice` as `i` 
                    LEFT JOIN `tl_member` as `m` ON `i`.`member` = `m`.`id` 
                WHERE `i`.`pid`=? 
                ORDER BY `invoice_id_str` DESC')
                ->execute($dc->activeRecord->pid)
            ;
        } else {
            $all = DB::getInstance()
                ->prepare('
                SELECT `i`.*, `m`.`company` 
                FROM `tl_iao_invoice` as `i` 
                    LEFT JOIN `tl_member` as `m` ON `i`.`member` = `m`.`id` 
                ORDER BY `invoice_id_str` DESC')
                ->execute()
            ;
        }

        while ($all->next()) {
            $varValue[$all->id] = $all->invoice_id_str.' :: '.\StringUtil::substr($all->title, 20)
                .' ('.number_format((float) $all->price_brutto, 2, ',', '.')
                .' '.$settings['iao_currency_symbol'].')';
        }

        return $varValue;
    }

    /**
     * fill Text.
     *
     * @param $varValue
     *
     * @throws \Exception
     *
     * @return string
     */
    public function fillStepFields($varValue, DataContainer $dc)
    {
        if (1 !== $varValue) {
            return $varValue;
        }

        $this->fillReminderFields($dc->activeRecords);

        return '';
    }

    /**
     * Generiert aus den Textfeld einen fertigen Text mit allen Tag-Ersetzungen.
     *
     * @throws \Exception
     *
     * @return string
     */
    public function getTextFinish(DataContainer $dc)
    {
        $obj = IaoReminderModel::findByPk($dc->id);
        if (!\is_object($obj)) {
            throw new \Exception('getTextFinish() ist fehlgeschlagen.');
        }
        $text_finish = $this->changeIAOTags($obj->text, 'reminder', $obj);

        $finishTemplate = new BackendTemplate('be_dca_reminder_text_finish');
        $finishTemplate->label = $GLOBALS['TL_LANG']['tl_iao_reminder']['text_finish'][0];
        $finishTemplate->text_finish = $text_finish;

        return $finishTemplate->parse();
    }

    /**
     * @throws \Exception
     */
    public function setTextFinish(DataContainer $dc): void
    {
        $objReminder = IaoReminderModel::findByPk($dc->id);
        if (!\is_object($objReminder)) {
            throw new \Exception('setTextFinish() ist fehlgeschlagen.');
        }
        $text_finish = $this->changeIAOTags($objReminder->text, 'reminder', $objReminder);
        $text_finish = $this->changeTags($text_finish);
        $objReminder->text_finish = $text_finish;

        $objReminder->save();
    }

    /**
     * fill Adress-Text.
     */
    public function fillAddressText($varValue, DataContainer $dc): string
    {
        if (1 === $varValue) {
            $intMember = Input::post('member');
            $text = $this->getAddressText((int) $intMember);

            $objReminder = IaoReminderModel::findByPk($dc->id);
            if (null === $objReminder) {
                return '';
            }

            $objReminder->address_text = $text;
            $objReminder->text_generate = '';
            $objReminder->save();
        }

        return '';
    }

    /**
     * Return the edit header button.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function editHeader($row, $href, $label, $title, $icon, $attributes)
    {
        return (User::getInstance()->isAdmin || \count(preg_grep('/^tl_iao_reminder::/', $this->User->alexf)) > 0)
            ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" title="'
            .StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> '
            : '';
    }

    /**
     * wenn GET-Parameter passen dann wird ein PDF erzeugt.
     * @throws PdfParserException
     */
    public function checkPDF(DataContainer $dc): void
    {
        if ('pdf' === Input::get('key') && (int) Input::get('id') > 0) {
            $this->generateReminderPDF((int) Input::get('id'), 'reminder');
        }
    }

    /**
     * Generate a "PDF" button and return it as string.
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     *
     * @return string
     */
    public function showPDFButton(array $row, string $href, string $label, string $title, string $icon): string
    {
        // wenn kein Admin dann kein PDF-Link
        if (!User::getInstance()->isAdmin) {
            return '';
        }

        $href = 'contao/main.php?do=iao_reminder&amp;key=pdf&amp;id='.$row['id'];

        return '<a href="'.$href.'" title="'.StringUtil::specialchars($title).'">'.Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * Autogenerate an article alias if it has not been set yet.
     *
     * @param $varValue
     *
     * @throws \Exception
     *
     * @return int|mixed|string|null
     */
    public function generateReminderNumber($varValue, DataContainer $dc)
    {
        $autoNr = false;
        $varValue = (int) $varValue;
        $settings = $this->getSettings($dc->activeRecord->setting_id);

        // Generate invoice_id if there is none
        if (0 === $varValue) {
            $autoNr = true;
            $objNr = DB::getInstance()->prepare('SELECT `invoice_id` FROM `tl_iao_reminder` ORDER BY `invoice_id` DESC')
                ->limit(1)
                ->execute()
            ;

            if ($objNr->numRows < 1 || 0 === $objNr->invoice_id) {
                $varValue = $settings['iao_invoice_startnumber'];
            } else {
                $varValue = $objNr->invoice_id + 1;
            }
        } else {
            $objNr = DB::getInstance()->prepare('SELECT `invoice_id` FROM `tl_iao_reminder` WHERE `id`=? OR `invoice_id`=?')
                ->limit(1)
                ->execute($dc->id, $varValue)
            ;

            // Check whether the InvoiceNumber exists
            if ($objNr->numRows > 1) {
                if (!$autoNr) {
                    throw new \Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
                }

                $varValue .= '-'.$dc->id;
            }
        }

        return $varValue;
    }

    /**
     * List a particular record.
     *
     * @param array
     *
     * @return string
     */
    public function listEntries($arrRow)
    {
        $settings = $this->getSettings($arrRow['setting_id']);

        $objReminder = IaoReminderModel::findByPk($arrRow['id']);
        if (null === $objReminder) {
            return 'kein Reminder';
        }

        $objMember = MemberModel::findByPk($objReminder->member);
        if (null === $objMember) {
            return 'kein Kunde zugeordnet';
        }

        $objInvoice = IaoInvoiceModel::findByPk($objReminder->invoice_id);
        if (null === $objInvoice) {
            return 'keine Rechnung gefunden';
        }

        $listTemplate = new BackendTemplate('be_dca_reminder_list_item');
        $listTemplate->reminder = $objReminder;
        $listTemplate->member = $objMember;
        $listTemplate->invoice = $objInvoice;
        $listTemplate->arrSettings = $settings;

        return $listTemplate->parse();
    }

    /**
     * Return the "toggle visibility" button.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (Input::get('tid')) {
            $this->toggleStatus(Input::get('tid'), Input::get('state'), (@func_get_arg(12) ?: null));
            $this->redirect($this->getReferer());
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.('1' === $row['status'] ? '2' : '1');

        if ('2' === $row['status']) {
            $icon = 'logout.gif';
        }

        return '<a href="'.$this->addToUrl($href).'" title="'.$GLOBALS['TL_LANG']['tl_iao_reminder']['toggle'].'"'.$attributes.'>'.Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * paid/not paid.
     *
     * @param int
     * @param bool
     */
    public function toggleStatus($intId, $status, DataContainer $dc = null): void
    {
        // Check permissions to edit
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');
        $User = User::getInstance();
        $logger = static::getContainer()->get('monolog.logger.contao');

        if ($dc) {
            $dc->id = $intId; // see #8043
        }

        // Set the current record
        if ($dc) {
            $objRow = DB::getInstance()->prepare('SELECT * FROM tl_iao_reminder WHERE id=?')
                ->limit(1)
                ->execute($intId)
            ;

            if ($objRow->numRows) {
                $dc->activeRecord = $objRow;
            }
        }

        // Check permissions to publish
        if (!$User->isAdmin && !$User->hasAccess('tl_iao_reminder::status', 'alexf')) {
            $logger->log(
                LogLevel::ERROR,
                'Not enough permissions to publish/unpublish Reminder ID "'.$intId.'"',
                ['contao' => new ContaoContext(__CLASS__.'::'.__METHOD__, 'ERROR')]
            );
            $this->redirect('contao/main.php?act=error');
        }

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_iao_reminder']['fields']['status']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_iao_reminder']['fields']['status']['save_callback'] as $callback) {
                if (\is_array($callback)) {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}((int) $status, $dc);
                } elseif (\is_callable($callback)) {
                    $blnVisible = $callback($status, $dc);
                }
            }
        }

        // Update the database
        DB::getInstance()
            ->prepare('UPDATE tl_iao_reminder SET status=? WHERE id=?')
            ->execute($status, $intId)
        ;

        //get reminder-Data
        $remindObj = DB::getInstance()
            ->prepare('SELECT * FROM `tl_iao_reminder` WHERE `id`=?')
            ->limit(1)
            ->execute($intId)
        ;

        if ($remindObj->numRows) {
            $dbObj = DB::getInstance()
                ->prepare('UPDATE `tl_iao_invoice` SET `status`=?, `notice` = `notice`+?  WHERE id=?')
                ->execute($status, $remindObj->notice, $remindObj->invoice_id)
            ;
        }
    }

    /**
     * ondelete_callback.
     */
    public function onDeleteReminder(DataContainer $dc): void
    {
        $invoiceID = (int) $dc->activeRecord->invoice_id;

        if (0 < $invoiceID) {
            $otherReminderObj = DB::getInstance()->prepare('SELECT `id` FROM `tl_iao_reminder` WHERE `invoice_id`=? AND `id`!=? ORDER BY `step` DESC')
                ->limit(1)
                ->execute($invoiceID, $dc->id)
            ;

            $newReminderID = ($otherReminderObj->numRows > 0) ? $otherReminderObj->id : 0;

            $set = [
                'reminder_id' => $newReminderID,
            ];

            DB::getInstance()
                ->prepare('UPDATE `tl_iao_invoice` %s WHERE `id`=?')
                ->set($set)
                ->execute($invoiceID)
            ;
        }
    }

    public function changeStatusReminder(DataContainer $dc): void
    {
        $state = Input::get('state');
        $reminderID = Input::get('id');
        $invoiceID = $dc->activeRecord->invoice_id;

        $objInvoice = IaoInvoiceModel::findByPk($invoiceID);
        if (null === $objInvoice) {
            return;
        }

        if (2 === $state) {
            if ($invoiceID) {
                $objInvoice->reminder_id = $reminderID;
                $objInvoice->paid_on_date = time();
                $objInvoice->status = 2;
            }
        } elseif (1 === $state) {
            if ((int) $invoiceID > 0) {
                $objInvoice->reminder_id = '';
                $objInvoice->paid_on_date = '';
                $objInvoice->status = 1;
            }
        }

        $objInvoice->save();
    }

    /**
     * @param $varValue
     *
     * @return int
     */
    public function updateStatus($varValue, DataContainer $dc)
    {
        $varValue = (int) $varValue;

        // UPDATE invoice when reminder is market as paid
        if (2 === $varValue && $dc->activeRecord->invoice_id > 0) {
            $objInvoice = IaoInvoiceModel::findByPk($dc->activeRecord->invoice_id);
            if (null === $objInvoice) {
                return $varValue;
            }

            $objInvoice->status = $varValue;
            $objInvoice->paid_on_date = $dc->activeRecord->paid_on_date;
            $objInvoice->save();
        }

        return $varValue;
    }
}
