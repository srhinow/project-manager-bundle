<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Dca;

use Contao\DataContainer;
use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;

class Module extends IaoBackend
{
    /**
     * Return all info templates as array.
     *
     * @return array
     */
    public function getIaoTemplates(DataContainer $dc)
    {
        return \Controller::getTemplateGroup('iao_');
    }
}
