<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Dca;

use Contao\BackendTemplate;
use Contao\BackendUser as User;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;
use Srhinow\ProjectManagerBundle\Model\IaoCreditItemsModel;
use Srhinow\ProjectManagerBundle\Model\IaoCreditModel;
use Srhinow\ProjectManagerBundle\Model\IaoTemplatesItemsModel;

class CreditItem extends IaoBackend
{
    protected $settings = [];

    /**
     * Iao\Dca\CreditItems constructor.
     */
    public function __construct()
    {
        $this->setIaoSettings();
        $this->import('BackendUser', 'User');

        parent::__construct();
    }

    /**
     * get all setting for parent credit-element.
     */
    public function setIaoSettings(): void
    {
        if(null === Input::get('id')) {
            $this->settings = $this->getSettings();
        }


        $id = (int) Input::get('id');

        if (0 < $id) {
            $objCredit = IaoCreditModel::findByPk($id);

            $this->settings = (null === $objCredit) ? [] : $this->getSettings($objCredit->setting_id);
        }
    }

    /**
     * @param $href
     * @param $label
     * @param $title
     * @param $class
     *
     * @return string
     */
    public function showPDFButton($href, $label, $title, $class): string
    {
        $objPdfTemplate = \FilesModel::findByUuid($this->settings['iao_credit_pdf']);

        if (\strlen($objPdfTemplate->path) < 1 || !file_exists(TL_ROOT.'/'.$objPdfTemplate->path)) {
            return '';
        }  // template file not found

        return '&nbsp; :: &nbsp;<a href="contao/main.php?do=iao_credit&table=tl_iao_credit&'.$href.'" title="'
            .StringUtil::specialchars($title).'" class="'.$class.'">'.$label.'</a> ';
    }

    /**
     * Check permissions to edit table Iao\Dca\CreditItems.
     */
    public function checkPermission(): void
    {
        $this->checkIaoModulePermission('Iao\Dca\CreditItems');
    }

    /**
     * Add the type of input field.
     *
     * @param array
     *
     * @return string
     */
    public function listItems($arrRow): string
    {
        if ('devider' === $arrRow['type']) {
            return '<div class="pdf-devider"><span>PDF-Trenner</span></div>';
        }

        // Classes
        $class = ($arrRow['published']) ? ' published' : ' unpublished';

        $itemTemplate = new BackendTemplate('be_dca_credititem_list_item');
        $itemTemplate->arrRow = $arrRow;
        $itemTemplate->class = $class;
        $itemTemplate->currency_symbol = $this->settings['iao_currency_symbol'];
        $itemTemplate->format_netto = number_format((float) $arrRow['price_netto'], 2, ',', '.');
        $itemTemplate->format_brutto = number_format((float) $arrRow['price_brutto'], 2, ',', '.');

        return $itemTemplate->parse();
    }

    /**
     * save the price from all items in parent_table.
     *
     * @param mixed
     * @param object
     *
     * @return void
     */
    public function saveAllPricesToParent(DataContainer $dc): void
    {
        // Return if there is no active record (override all)
        if (!$dc->activeRecord) {
            return;
        }

        $objItems = IaoCreditItemsModel::findBy(['`tl_iao_credit_items`.`pid`=?', '`tl_iao_credit_items`.published =?'], [$dc->activeRecord->pid, 1]);

        if (null === $objItems) {
            $allNetto = 0;
            $allBrutto = 0;

            while ($objItems->next()) {
                $englprice = str_replace(',', '.', $objItems->price);
                $priceSum = $englprice * $objItems->count;

                if (1 === (int) $objItems->vat_incl) {
                    $allNetto += $priceSum;
                    $allBrutto += $this->getBruttoPrice($priceSum, $objItems->vat);
                } else {
                    $allNetto += $this->getNettoPrice($priceSum, $objItems->vat);
                    $allBrutto += $priceSum;
                }
            }

            $objCredit = IaoCreditModel::findByPk($dc->activeRecord->pid);
            $objCredit->price_netto = $allNetto;
            $objCredit->price_brutto = $allBrutto;
            $objCredit->save();
        }
    }

    /**
     * save the price_netto and price_brutto from actual item.
     */
    public function saveNettoAndBrutto(DataContainer $dc): void
    {
        // Return if there is no active record (override all)
        if (!$dc->activeRecord) {
            return;
        }

        $englprice = str_replace(',', '.', $dc->activeRecord->price);
        $priceSum = $englprice * $dc->activeRecord->count;

        if (1 === (int) $dc->activeRecord->vat_incl) {
            $Netto = $priceSum;
            $Brutto = $this->getBruttoPrice($priceSum, $dc->activeRecord->vat);
        } else {
            $Netto = $this->getNettoPrice($priceSum, $dc->activeRecord->vat);
            $Brutto = $priceSum;
        }

        $objItem = IaoCreditItemsModel::findByPk($dc->id);
        $objItem->price_netto = $Netto;
        $objItem->price_brutto = $Brutto;
        $objItem->save();
    }

    /**
     * Return the "toggle visibility" button.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes): string
    {
        if (Input::get('tid') && \strlen(Input::get('tid'))) {
            $this->toggleVisibility(Input::get('tid'), ('1' === Input::get('state')));
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        $User = User::getInstance();
        if (!$User->isAdmin && !$User->hasAccess('Iao\Dca\CreditItems::published', 'alexf')) {
            return '';
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);

        if (!$row['published']) {
            $icon = 'invisible.gif';
        }

        return '<a href="'.$this->addToUrl($href).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * Disable/enable a user group.
     *
     * @param int
     * @param bool
     */
    public function toggleVisibility($intId, $blnVisible): void
    {
        // Check permissions to edit
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        // Check permissions to publish
        if (!$this->User->isAdmin && !$this->User->hasAccess('tl_iao_credit_items::published', 'alexf')) {
            $logger = static::getContainer()->get('monolog.logger.contao');
            $logger->log('Not enough permissions to publish/unpublish event ID "'.$intId.'"',
                'Iao\Dca\CreditItems toggleVisibility',
                ['contao' => new ContaoContext(__CLASS__.'::'.__METHOD__, 'ERROR')]
            );
            $this->redirect('contao/main.php?act=error');
        }

        $objVersions = new \Versions('tl_iao_credit_items', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_iao_credit_items']['fields']['published']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_iao_credit_items']['fields']['published']['save_callback'] as $callback) {
                $this->import($callback[0]);
                $blnVisible = $this->$callback[0]->$callback[1]($blnVisible, $this);
            }
        }

        // Update the database
        $objItem = IaoCreditItemsModel::findByPk($intId);
        $objItem->tstamp = time();
        $objItem->published = $blnVisible ? 1 : '';
        $objItem->save();

        $objVersions->create();
    }

    /**
     * Generate a button to put a posten-template for credit.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function addPostenTemplate($row, $href, $label, $title, $icon, $attributes): string
    {
        $href .= '&amp;ptid='.$row['id'];
        $strButton = '<a href="'.$this->addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> ';

        if (!$this->User->isAdmin) {
            return '';
        }

        if ('addPostenTemplate' === Input::get('key') && Input::get('ptid') === $row['id']) {
            $objItem = IaoCreditItemsModel::findByPk($row['id']);
            if (null === $objItem) {
                return $strButton;
            }

            $templateItemId = $this->createTemplateItem($objItem, 'credit');

            if (0 < $templateItemId) {
                $this->redirectToTemplateItem($templateItemId);
            }
        }

        return $strButton;
    }

    /**
     * get all credit-posten-templates.
     *
     * @return array
     */
    public function getPostenTemplate(DataContainer $dc): array
    {
        $options = [];

        $objTemplate = IaoTemplatesItemsModel::findBy(['`position`=?'], ['credit']);
        if (null === $objTemplate) {
            return $options;
        }

        while ($objTemplate->next()) {
            $options[$objTemplate->id] = $objTemplate->headline;
        }

        return $options;
    }

    /**
     * fill Text before.
     *
     * @param string $varValue
     * @param DataContainer $dc
     *
     * @return int
     */
    public function fillPostenFields(string $varValue, $dc)
    {
        if ((int) $varValue <= 0) {
            return $varValue;
        }

        $postenset = $this->getTemplateItemValues($varValue);
        if (!\is_array($postenset) || \count($postenset) < 1) {
            return $varValue;
        }

        $objCreditItem = IaoCreditItemsModel::findByPk($dc->id);
        $objCreditItem->setRow($postenset);
        $objCreditItem->save();

        $this->reload();

        return $varValue;
    }
}
