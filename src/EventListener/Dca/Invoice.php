<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Dca;

use Contao\BackendTemplate;
use Contao\BackendUser as User;
use Contao\CoreBundle\DependencyInjection\Attribute\AsCallback;
use Contao\Database as DB;
use Contao\DataContainer;
use Contao\FilesModel;
use Contao\Image;
use Contao\Input;
use Contao\MemberModel;
use Contao\Message;
use Contao\StringUtil;
use setasign\Fpdi\PdfParser\PdfParserException;
use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;
use Srhinow\ProjectManagerBundle\Model\IaoAgreementsModel as AgreementsModel;
use Srhinow\ProjectManagerBundle\Model\IaoInvoiceModel;
use Srhinow\ProjectManagerBundle\Model\IaoProjectsModel;
use Srhinow\ProjectManagerBundle\Model\IaoTemplatesModel;

class Invoice extends IaoBackend
{
    protected $settings = [];

    /**
     * Invoice constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Check permissions to edit table tl_iao_invoice.
     */
    public function checkPermission(): void
    {
        $this->checkIaoModulePermission('tl_iao_invoice');
    }

    public function togglePaidIcon($row, $href, $label, $title, $icon)
    {
        $href .= '&amp;id=' . $row['id'].'&amp;rt='.REQUEST_TOKEN;

        if ((int) $row['paid']  !== 1)
        {
            $icon = 'logout.svg';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '" onclick="Backend.getScrollOffset();return AjaxRequest.toggleField(this,true)">' . Image::getHtml($icon, $label, 'data-icon="' . Image::getPath('ok.svg') . '" data-icon-disabled="' . Image::getPath('logout.svg') . '" data-state="' . ($row['paid'] === '1' ? '1' : '0') . '"') . '</a> ';
    }

    /**
     * prefill eny Fields by new dataset.
     *
     * @param $table string
     * @param $id int
     * @param $set array
     * @param $obj object
     *
     * @throws \Exception
     */
    public function preFillFields($table, $id, $set, $obj): void
    {
        $objProject = IaoProjectsModel::findByPk($set['pid']);
        $settingId = (null !== $objProject && 0 !== $objProject->setting_id) ? $objProject->setting_id : 1;
        $settings = $this->getSettings($settingId);
        $invoiceId = $this->generateInvoiceNumber(0, $settings);
        $invoiceIdStr = $this->generateInvoiceNumberStr($invoiceId, time(), $settings);
        $set = [
            'invoice_id' => $invoiceId,
            'invoice_id_str' => $invoiceIdStr,
        ];

        DB::getInstance()->prepare('UPDATE '.$table.' %s WHERE `id`=?')
            ->set($set)
            ->limit(1)
            ->execute($id)
        ;
    }

    /**
     * Generiert das "erstellt am" - Feld.
     *
     * @param $varValue integer
     *
     * @return int
     */
    public function generateExecuteDate($varValue, DataContainer $dc)
    {
        if ((int) $varValue > 0) {
            return $varValue;
        }

        if (null === ($objInvoice = IaoInvoiceModel::findByPk($dc->id))) {
            return $varValue;
        }

        $executeDate = ($dc->activeRecord->invoice_tstamp) ?: time();
        $objInvoice->execute_date = $executeDate;
        $objInvoice->save();

        return $executeDate;
    }

    /**
     * Falls leer wird das "zahlbar bis" - Feld generiert und befüllt.
     *
     * @param $varValue
     * @param DataContainer $dc
     * @return int
     */
    public function generateExpiryDate($varValue, DataContainer $dc)
    {
        $settings = $this->getSettings($dc->activeRecord->setting_id);

        if ("" === $varValue) {
            // Laufzeit in Tagen
            $dur = (int) ($settings['iao_invoice_duration']) ? : 14;
            $invoiceTstamp = (int) ($dc->activeRecord->invoice_tstamp) ? : time();

            //auf Sonabend prüfen, wenn ja, dann auf Montag setzen
            if (6 === date('N', $invoiceTstamp + ($dur * 24 * 60 * 60))) {
                $dur = $dur + 2;
            }

            //auf Sontag prüfen, wenn ja, dann auf Montag setzen
            if (7 === date('N', $invoiceTstamp + ($dur * 24 * 60 * 60))) {
                $dur = $dur + 1;
            }

            $varValue = $invoiceTstamp + ($dur * 24 * 60 * 60);
        }

        return $varValue;
    }

    /**
     * generiert den Rechnung-Zeitstempel.
     *
     * @param $varValue int
     * @param DataContainer $dc
     * @return int
     */
    public function generateInvoiceTstamp(int $varValue, DataContainer $dc): int
    {
        return (0 === (int) $varValue) ? time() : $varValue;
    }

    /**
     * fill Address-Text.
     */
    public function fillAddressText($varValue, DataContainer $dc): string
    {
        if (strlen(trim($varValue)) > 0) {
            $intMember = Input::post('member');
            $text = $this->getAddressText((int) $intMember);

            $objInvoice = IaoInvoiceModel::findByPk($dc->id);
            $objInvoice->address_text = $text;
            $objInvoice->member = $intMember;
            $objInvoice->text_generate = '';
            $objInvoice->save();
        }

        return $varValue;
    }

    /**
     * fill Text before if this field is empty.
     *
     * @param string $varValue
     * @param DataContainer $dc
     * @return int | void
     */
    public function fillBeforeText(string $varValue, DataContainer $dc)
    {
        $varValue = (int) $varValue;

        if (1 > $varValue) {
            return $varValue;
        }

        //hole das ausgewählte Template
        if (null === ($objTemplate = IaoTemplatesModel::findByPk($varValue))) {
            return $varValue;
        }

        //hole den aktuellen Datensatz als DB-Object
        if (null === ($objInvoice = IaoInvoiceModel::findByPk($dc->id))) {
            return $varValue;
        }

        $text = $this->changeIAOTags($objTemplate->text, 'invoice', $objInvoice);

        // schreibe das Textfeld
        $objInvoice->before_text = $text;
        $objInvoice->save();

        return $varValue;
    }

    /**
     * fill Text after if this field is empty.
     *
     * @param string $varValue
     * @param DataContainer $dc
     * @return int | void
     */
    public function fillAfterText(string $varValue, DataContainer $dc)
    {
        $varValue = (int) $varValue;

        if (1 > $varValue) {
            return $varValue;
        }

        //hole das ausgewählte Template
        $objTemplate = IaoTemplatesModel::findByPk($varValue);
        if (null === $objTemplate) {
            return $varValue;
        }

        //hole den aktuellen Datensatz als DB-Object
        $objInvoice = IaoInvoiceModel::findByPk($dc->id);
        if (null === $objInvoice) {
            return $varValue;
        }

        $text = $this->changeIAOTags($objTemplate->text, 'invoice', $objInvoice);

        // schreibe das Textfeld
        $objInvoice->after_text = $text;
        $objInvoice->save();

        return $varValue;
    }

    /**
     * @param $varValue
     * @param DataContainer $dc
     * @return string
     * @throws \Exception
     */
    public function saveBeforeTextAsTemplate($varValue, DataContainer $dc): string
    {
        if (1 > (int) $varValue) {
            return $varValue;
        }

        if($this->saveTextAsTemplate(
            $dc->activeRecord->after_text,
            $dc->activeRecord->after_template,
            'invoice_before_text'
        )) {
            Message::addInfo('Das Template wurde erfolgreich gespeichert');
            return '';
        }

        return $varValue;
    }

    /**
     * @param $varValue
     * @param DataContainer $dc
     * @return string
     * @throws \Exception
     */
    public function saveAfterTextAsTemplate($varValue, DataContainer $dc): string
    {
        if (1 > $varValue) {
            return $varValue;
        }

        if($this->saveTextAsTemplate(
            $dc->activeRecord->after_text,
            $dc->activeRecord->after_template,
            'invoice_after_text'
        )){
            Message::addInfo('Das Template wurde erfolgreich gespeichert');
            return '';
        }

        return $varValue;
    }

    /**
     * get all Agreements to valid groups.
     *
     * @param DataContainer $dc
     * @return array
     */
    public function getAgreements(DataContainer $dc): array
    {
        $varValue = [];

        $objAgr = AgreementsModel::findBy('status', '1');

        if (\is_object($objAgr)) {
            while ($objAgr->next()) {
                $varValue[$objAgr->id] = $objAgr->title.' ('.$objAgr->price.' &euro;)';
            }
        }

        return $varValue;
    }

    /**
     * get all invoice before template.
     *
     * @param DataContainer $dc
     * @return array
     */
    public function getBeforeTemplate(DataContainer $dc): array
    {
        $varValue = [];

        $objTemplates = IaoTemplatesModel::findBy('position', 'invoice_before_text');

        if (\is_object($objTemplates)) {
            while ($objTemplates->next()) {
                $varValue[$objTemplates->id] = $objTemplates->title;
            }
        }

        return $varValue;
    }

    /**
     * get all invoice after template.
     *
     * @return array
     */
    public function getAfterTemplate(DataContainer $dc): array
    {
        $varValue = [];

        $objTemplate = IaoTemplatesModel::findBy('position', 'invoice_after_text');

        if (\is_object($objTemplate)) {
            while ($objTemplate->next()) {
                $varValue[$objTemplate->id] = $objTemplate->title;
            }
        }

        return $varValue;
    }

    /**
     * Return the edit header button.
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     * @return string
     */
    public function editHeader(array $row, string $href, string $label, string $title, string $icon, string $attributes): string
    {
        $User = User::getInstance();

        return $User->hasAccess('css', 'themes')
            ? '<a href="'.$this->addToUrl(
                $href.'&amp;id='.$row['id']).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'
            .Image::getHtml($icon, $label).'</a> '
            : Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)).' ';
    }

    /**
     * wenn GET-Parameter passen dann wird ein PDF erzeugt.
     * @throws PdfParserException
     */
    public function generateInvoicePDF(DataContainer $dc): void
    {
        if ('pdf' === Input::get('key') && (int) Input::get('id') > 0) {
            $this->generatePDF((int) Input::get('id'), 'invoice');
        }
    }

    /**
     * Generate a "PDF" button and return it as string.
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @return string
     */
    public function showPDFButton(array $row, string $href, string $label,string $title, string $icon): string
    {
        $settings = $this->getSettings($row['setting_id']);

        // wenn kein Admin dann kein PDF-Link
        if (!User::getInstance()->isAdmin) {
            return '';
        }

        // Wenn keine PDF-Vorlage dann kein PDF-Link
        $objPdfTemplate = FilesModel::findByUuid($settings['iao_invoice_pdf']);
        if (null === $objPdfTemplate || \strlen($objPdfTemplate->path) < 1 || !file_exists(TL_ROOT.'/'.$objPdfTemplate->path)) {
            return '';
        }  // template file not found

        $href = 'contao/main.php?do=iao_invoice&amp;key=pdf&amp;id='.$row['id'];

        return '<a href="'.$href.'" title="'.StringUtil::specialchars($title).'">'.\Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * fill field invoice_id_str if it's empty.
     */
    public function setFieldInvoiceNumberStr(?string $varValue, DataContainer $dc): string
    {
        if (strlen((string) $varValue) > 0) {
            return $varValue;
        }
        $settings = $this->getSettings($dc->activeRecord->setting_id);
        $timestamp = ($dc->activeRecord->date) ?: time();

        return $this->generateInvoiceNumberStr($dc->activeRecord->invoice_id, $timestamp, $settings);
    }

    /**
     * fill field invoice_id if it's empty.
     *
     * @param int $varValue
     *
     * @throws \Exception
     *
     * @return string
     */
    public function setFieldInvoiceNumber($varValue, DataContainer $dc): int|string
    {
        if (1 > (int) $varValue) {
            $settings = $this->getSettings($dc->activeRecord->setting_id);

            return $this->generateInvoiceNumber($varValue, $settings);
        }

        return $varValue;
    }

    /**
     * List a particular record.
     *
     * @param array $arrRow
     *
     * @return string
     */
    public function listEntries($arrRow)
    {
        $objMember = MemberModel::findByPk($arrRow['member']);

        $settings = $this->getSettings($arrRow['setting_id']);

        $itemTemplate = new BackendTemplate('be_dca_invoice_list_item');
        $itemTemplate->arrRow = $arrRow;
        $itemTemplate->published = ($arrRow['published'] == '1') ? ' published': ' unpublished';
        $itemTemplate->price_brutto = number_format((float) $arrRow['price_brutto'], 2, ',', '.');
        $itemTemplate->remaining = number_format((float) $arrRow['remaining'], 2, ',', '.');
        $itemTemplate->currency_symbol = $settings['iao_currency_symbol'];
        $itemTemplate->arrMember = (null === $objMember) ? [] : $objMember->row();

        return $itemTemplate->parse();
    }

    /**
     * @param $varValue
     *
     * @return
     */
    public function updateStatus($varValue, DataContainer $dc)
    {

        if('' === $varValue || $varValue === $dc->activeRecord->status) {
            return $varValue;
        }

        if(null === ($objInvoice = IaoInvoiceModel::findByPk($dc->id))) {
            return $varValue;
        }

        $paid = 0;
        $remaining = $objInvoice->price_brutto;

        //bezahlt
        if($varValue === '2') {
            $paid = 1;
            $remaining = 0;
        }

        //nicht bezahlt
        if($varValue === '1') {
            $paid = 0;
            $remaining = $objInvoice->price_brutto;
        }

        $set = [
            'status' => $varValue,
            'paid' => $paid,
            'remaining' => $remaining,
            'paid_on_date' => $dc->activeRecord->paid_on_date ?:time(),
        ];

        DB::getInstance()->prepare('UPDATE `tl_iao_invoice` %s WHERE `id`=?')
            ->set($set)
            ->execute($dc->id)
        ;

        return $varValue;
    }

    /**
     * @param $varValue
     *
     * @return
     */
    public function updatePaid($varValue, DataContainer $dc)
    {
        if(null === ($objInvoice = IaoInvoiceModel::findByPk($dc->id))) {
            return $varValue;
        }

        $set = [
            'paid' => $varValue,
            'status' => ($varValue === '1') ?'2' :'1',
            'remaining' => ($varValue === '1') ?'0' : $objInvoice->price_brutto,
            'paid_on_date' => $objInvoice->paid_on_date ?:time(),
        ];

        DB::getInstance()->prepare('UPDATE `tl_iao_invoice` %s WHERE `id`=?')
            ->set($set)
            ->execute($dc->id)
        ;

        return $varValue;
    }

    /**
     * @param $varValue
     *
     * @return string
     */
    public function priceFormat($varValue, DataContainer $dc): string
    {
        return $this->getPriceStr((float) $varValue);
    }

    /**
     * @param $varValue
     *
     * @return string
     */
    public function getPriceallValue($varValue, DataContainer $dc)
    {
        return $dc->activeRecord->price_brutto;
    }

    /**
     * calculate and update fields.
     *
     * @param string $varValue
     *
     * @return string
     */
    public function updateRemaining($varValue, DataContainer $dc)
    {
        $paidsArr = unserialize($varValue);
        $already = 0;
        $lastPayDate = '';

        if (!\is_array($paidsArr) || strlen($paidsArr[0]['payamount']) < 1) {
            return $varValue;
        }

        foreach ($paidsArr as $a) {
            $already += (int) $a['payamount'];
            $lastPayDate = (int) $a['paydate'];
        }


        $dif = $dc->activeRecord->price_brutto - $already;
        $status = ($dc->activeRecord->price_brutto === $already && $dc->activeRecord->price_brutto > 0)
            ? 2
            : $dc->activeRecord->status;
        $paid_on_date = ($dc->activeRecord->price_brutto === $already)
            ? $lastPayDate
            : $dc->activeRecord->paid_on_date;

        $objInvoice = IaoInvoiceModel::findByPk($dc->id);
        $objInvoice->remaining = $dif;
        $objInvoice->status = $status;
        $objInvoice->paid_on_date = $paid_on_date;
        $objInvoice->save();

        return $varValue;
    }

    public function fillEmptyMember(DataContainer $dc): void
    {
        $objInvoices = IaoInvoiceModel::findBy(['tl_iao_invoice.member=?'], ['']);
        if (null === $objInvoices) {
            return;
        }

        while ($objInvoices->next()) {
            $objProject = IaoProjectsModel::findByPk($objInvoices->pid);
            if (null === $objProject) {
                continue;
            }

            $objInvoices->member = $objProject->member;
            $objInvoices->save();
        }
    }
}
