<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Dca;

use Contao\Controller;
use Contao\Database;
use Contao\DataContainer;
use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;

class Member
{
    protected $settings;

    public function setCustomerGroup(DataContainer $dc): void
    {
        $this->settings = IaoBackend::getInstance()->getSettings();
        // Return if there is no active record (override all)
        if (!$dc->activeRecord || 0 === $dc->id) {
            return;
        }
        Database::getInstance()->prepare('UPDATE tl_member SET iao_group=? WHERE id=?')
            ->execute($this->settings['iao_costumer_group'], $dc->id)
        ;
    }

    /**
     * fill Address-Text.
     */
    public function fillAddressText($varValue, DataContainer $dc)
    {
        if (1 === $varValue) {
            $text = '<p>'.$dc->activeRecord->company.'<br />'.('' !== $dc->activeRecord->gender ? $GLOBALS['TL_LANG']['tl_iao']['gender'][$dc->activeRecord->gender].' ' : '').($dc->activeRecord->title ? $dc->activeRecord->title.' ' : '').$dc->activeRecord->firstname.' '.$dc->activeRecord->lastname.'<br />'.$dc->activeRecord->street.'</p>';
            $text .= '<p>'.$dc->activeRecord->postal.' '.$dc->activeRecord->city.'</p>';

            $set = [
                'address_text' => $text,
                'text_generate' => '',
            ];

            Database::getInstance()->prepare('UPDATE `tl_member` %s WHERE `id`=?')
                ->set($set)
                ->limit(1)
                ->execute($dc->id)
            ;

            Controller::reload();
        }

        return $varValue;
    }
}
