<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Dca;

use Contao\BackendTemplate;
use Contao\BackendUser as User;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\MemberModel;
use Contao\StringUtil;
use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;
use Srhinow\ProjectManagerBundle\Model\IaoCreditModel;
use Srhinow\ProjectManagerBundle\Model\IaoProjectsModel;
use Srhinow\ProjectManagerBundle\Model\IaoTemplatesModel;

class Credit extends IaoBackend
{
    protected $settings = [];


    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->settings = $this->getSettings();
        $this->import('BackendUser', 'User');
        parent::__construct();
    }

    /**
     * Check permissions to edit table tl_iao_credit.
     */
    public function checkPermission(): void
    {
        $this->checkIaoModulePermission('tl_iao_credit');
    }

    /**
     * prefill eny Fields by new dataset.
     *
     * @param string $table
     * @param int    $id
     * @param array  $set
     * @param object $obj
     *
     * @throws \Exception
     */
    public function preFillFields($table, $id, $set, $obj): void
    {
        $objProject = IaoProjectsModel::findByPk($set['pid']);
        $settingId = (null !== $objProject && 0 !== $objProject->setting_id) ? $objProject->setting_id : 1;
        $this->settings = $this->getSettings($settingId);

        $creditId = $this->generateCreditNumber(0, $this->settings);
        $creditIdStr = $this->createCreditNumberStr('', $creditId, time(), $this->settings);

        $objCredit = IaoCreditModel::findByPk($id);
        $objCredit->credit_id = $creditId;
        $objCredit->credit_id_str = $creditIdStr;
        $objCredit->save();
    }

    /**
     * fill date-Field if this empty.
     *
     * @param mixed
     * @param object
     *
     * @return string
     */
    public function generateCreditDate($varValue, \DataContainer $dc)
    {
        return $this->getDateStr($varValue);
    }

    /**
     * fill date-Field if this empty.
     *
     * @param $varValue mixed
     * @param $dc object
     */
    public function generateExpiryDate($varValue, \DataContainer $dc)
    {
        $settings = $this->getSettings($dc->activeRecord->setting_id);

        if ("" === $varValue) {
            $format = ($settings['iao_credit_expiry_date']) ? : '+3 month';
            $tstamp = (int) ($dc->activeRecord->credit_tstamp) ? : time();
            $varValue = strtotime($format, $tstamp);
        }

        return  $varValue;
    }

    public function updateExpiryToTstmp(\DataContainer $dc): void
    {
        $objCredits = IaoCreditModel::findAll();

        if (\is_object($objCredits)) {
            while ($objCredits->next()) {
                if (!stripos($objCredits->expiry_date, '-')) {
                    continue;
                }

                $objCredits->expiry_date = strtotime($objCredits->expiry_date);
                $objCredits->save();
            }
        }
    }

    /**
     * fill date-Field if this empty.
     *
     * @param $varValue mixed
     * @param $dc object
     *
     * @return int
     */
    public function generateCreditTstamp($varValue, DataContainer $dc)
    {
        return (0 === (int) $varValue) ? time() : $varValue;
    }

    /**
     * fill Adress-Text.
     *
     * @param $dc object
     *
     * @return string
     */
    public function fillAddressText($varValue, DataContainer $dc)
    {
        if ('1' === $varValue) {
            $intMember = Input::post('member');
            $text = $this->getAddressText((int) $intMember);

            $objCredit = IaoCreditModel::findByPk($dc->id);
            $objCredit->address_text = $text;
            $objCredit->member = $intMember;
            $objCredit->text_generate = '';
            $objCredit->save();

            $this->reload();
        }

        return '';
    }

    /**
     * fill Text before if this field is empty.
     *
     * @param $varValue integer
     * @param $dc object
     *
     * @return int | void
     */
    public function fillBeforeTextFromTemplate($varValue, DataContainer $dc)
    {
        $beforeText = $dc->activeRecord->before_text;
        if (null === $beforeText || '' !== strip_tags($beforeText)) {
            return $varValue;
        }

        //hole das ausgewählte Template
        $objTemplate = IaoTemplatesModel::findByPk($varValue);

        //hole den aktuellen Datensatz als DB-Object
        $objCredit = IaoCreditModel::findByPk($dc->id);

        // ersetzte evtl. Platzhalter
        $text = $this->changeIAOTags($objTemplate->text, 'credit', $objCredit);

        // schreibe das Textfeld
        $objCredit->before_text = $text;
        $objCredit->save();

        $this->reload();
    }

    /**
     * fill Text after if this field is empty.
     *
     * @param $varValue
     * @param $dc object
     *
     * @return int
     */
    public function fillAfterTextFromTemplate($varValue, DataContainer $dc)
    {
        $afterText = $dc->activeRecord->after_text;
        if (null === $afterText || '' === strip_tags($afterText)) {
            if (\strlen($varValue) <= 0) {
                return $varValue;
            }

            //hole das ausgewähte Template
            $objTemplate = IaoTemplatesModel::findByPk($varValue);

            //hole den aktuellen Datensatz als DB-Object
            $objCredit = IaoCreditModel::findByPk($dc->id);

            // ersetzte evtl. Platzhalter
            $text = $this->changeIAOTags($objTemplate->text, 'credit', $objCredit);

            // schreibe das Textfeld
            $objCredit->after_text = $text;
            $objCredit->save();

            $this->reload();
        }

        return $varValue;
    }

    /**
     * get all template with position = 'credit_before_text'.
     *
     * @param object
     *
     * @return array
     */
    public function getBeforeTemplate(DataContainer $dc)
    {
        $varValue = [];

        $objTemplates = IaoTemplatesModel::findBy(['`position`=?'], ['credit_before_text']);
        if (null === $objTemplates) {
            return $varValue;
        }

        while ($objTemplates->next()) {
            $varValue[$objTemplates->id] = $objTemplates->title;
        }

        return $varValue;
    }

    /**
     * get all credit after template.
     *
     * @param object
     *
     * @return array
     */
    public function getAfterTemplate(DataContainer $dc)
    {
        $varValue = [];

        $objTemplates = IaoTemplatesModel::findBy(['`position`=?'], ['credit_after_text']);
        if (null === $objTemplates) {
            return $varValue;
        }

        while ($objTemplates->next()) {
            $varValue[$objTemplates->id] = $objTemplates->title;
        }

        return $varValue;
    }

    /**
     * @param $varValue
     *
     * @return string
     */
    public function saveBeforeTextAsTemplate($varValue, DataContainer $dc)
    {
        if (1 > $varValue) {
            return $varValue;
        }

        return $this->saveTextAsTemplate(
            $dc->activeRecord->after_text,
            $dc->activeRecord->after_template,
            'credit_before_text'
        );
    }

    /**
     * @param $varValue
     *
     * @return string
     */
    public function saveAfterTextAsTemplate($varValue, DataContainer $dc)
    {
        if (1 > $varValue) {
            return $varValue;
        }

        return $this->saveTextAsTemplate(
            $dc->activeRecord->after_text,
            $dc->activeRecord->after_template,
            'credit_after_text'
        );
    }

    /**
     * Return the edit header button.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function editHeader($row, $href, $label, $title, $icon, $attributes)
    {
        return ($this->User->isAdmin || \count(preg_grep('/^tl_iao_credit::/', $this->User->alexf)) > 0)
            ? '<a href="'.$this->addToUrl($href.'&amp;id='.$row['id']).'" title="'.specialchars($title).'"'
            .$attributes.'>'.Image::getHtml($icon, $label).'</a> '
            : '';
    }

    /**
     * wenn GET-Parameter passen dann wird eine PDF erzeugt.
     */
    public function generateCreditPDF(DataContainer $dc): void
    {
        if ('pdf' === Input::get('key') && (int) Input::get('id') > 0) {
            $this->generatePDF((int) Input::get('id'), 'credit');
        }
    }

    /**
     * Generate a "PDF" button and return it as string.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function showPDFButton($row, $href, $label, $title, $icon)
    {
        if (isset($row['setting_id'])) {
            $this->settings = $this->getSettings($row['setting_id']);
        }

        if (!$this->User->isAdmin || \count(preg_grep('/^tl_iao_credit::/', $this->User->alexf))) {
            return '';
        }

        $objPdfTemplate = \FilesModel::findByUuid($this->settings['iao_credit_pdf']);
        if (\strlen($objPdfTemplate->path) < 1 || !file_exists(TL_ROOT.'/'.$objPdfTemplate->path)) {
            // template file not found
            return '';
        }

        $href = 'contao/main.php?do=iao_credit&amp;key=pdf&amp;id='.$row['id'];

        return '<a href="'.$href.'" title="'.StringUtil::specialchars($title).'">'.\Image::getHtml($icon, $label).'</a> ';
    }

    /**
     * fill field invoice_id_str if it's empty.
     *
     * @param string
     * @param object
     *
     * @return string
     */
    public function setFieldCreditNumberStr($varValue, \DataContainer $dc)
    {
        if (strlen((string) $varValue) > 0) {
            return $varValue;
        }
        $settings = $this->getSettings($dc->activeRecord->setting_id);
        $tstamp = ($dc->activeRecord->date) ?: time();

        return $this->createCreditNumberStr($varValue, $dc->activeRecord->credit_id, $tstamp, $settings);
    }

    /**
     * generate a Credit-number-string if not set.
     *
     * @param string
     * @param int
     * @param int
     * @param array
     *
     * @return string
     */
    public function createCreditNumberStr($varValue, $creditId, $tstamp, $settings)
    {
        if (\strlen($varValue) < 1) {
            $format = $settings['iao_credit_number_format'];
            $format = str_replace('{date}', date('Ymd', $tstamp), $format);
            $format = str_replace('{nr}', (string) $creditId, $format);
            $varValue = $format;
        }

        return $varValue;
    }

    /**
     * fill field invoice_id if it's empty.
     *
     * @param string
     * @param object
     *
     * @throws \Exception
     *
     * @return string
     */
    public function setFieldCreditNumber($varValue, \DataContainer $dc)
    {
        $settings = $this->getSettings($dc->activeRecord->setting_id);

        if ((int) $varValue > 0) {
            return $varValue;
        }

        return $this->generateCreditNumber($varValue, $settings);
    }

    /**
     * Autogenerate an credit number if it has not been set yet.
     *
     * @param int   $varValue
     * @param array $settings
     *
     * @throws \Exception
     *
     * @return string
     */
    public function generateCreditNumber($varValue = 0, $settings = [])
    {
        $id = Input::get('id');

        // Generate credit_id if there is none
        if (0 === $varValue) {
            $objLastCredit = IaoCreditModel::findOneBy(null, null, ['order' => '`credit_id` DESC']);

            if (null === $objLastCredit || 0 === $objLastCredit->credit_id) {
                $varValue = $settings['iao_credit_startnumber'];
            } else {
                $varValue = $objLastCredit->credit_id + 1;
            }
        } else {
            $objLastCredit = IaoCreditModel::findOneBy(['`tl_iao_credit`.`id`='.$id.' OR `credit_id`='.$varValue], null);
            if (null !== $objLastCredit) {
                $varValue .= '-'.$id;
            }
        }

        return $varValue;
    }

    /**
     * List a particular record.
     *
     * @param array
     *
     * @return string
     */
    public function listEntries($arrRow)
    {
        $objMember = MemberModel::findByPk($arrRow['member']);
        $settings = $this->getSettings($arrRow['setting_id']);

        $itemTemplate = new BackendTemplate('be_dca_credit_list_item');
        $itemTemplate->arrRow = $arrRow;
        $itemTemplate->published = ($arrRow['published'] == '1') ? ' published': ' unpublished';
        $itemTemplate->price_brutto = number_format(
            (float) $arrRow['price_brutto'],
            2,
            ',',
            '.'
        );
        $itemTemplate->currency_symbol = $settings['iao_currency_symbol'];
        $itemTemplate->arrMember = (null === $objMember) ? [] : $objMember->row();

        return $itemTemplate->parse();
    }

}
