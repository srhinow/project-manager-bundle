<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\EventListener\Hooks;

use Contao\CoreBundle\Framework\ContaoFrameworkInterface;
use Contao\Input;
use Contao\StringUtil;

class InsertTagsListener
{
    /**
     * @var ContaoFrameworkInterface
     */
    private $framework;

    /**
     * Constructor.
     */
    public function __construct(ContaoFrameworkInterface $framework)
    {
        $this->framework = $framework;
    }

    /**
     * replace iao-specific inserttag if get-paramter isset
     * {{iao::BEREICH::COLUMN[::ID|ALIAS]}}
     * z.B. {{iao::invoice::title}} oder {{iao::invoice::title::4}}.
     *
     * @param string
     *
     * @return string
     */
    public function replaceFrontendIaoTags($strTag)
    {
        if ('iao::' === substr($strTag, 0, 5)) {
            //inserttag in Stuecke teilen
            $split = explode('::', $strTag);

            //wenn die ID nicht mit übergeben wurde die Detailseite vorraussetzen
            $idAlias = (\strlen((string) $split[3]) > 0) ? $split[3] : Input::get('auto_item');

            switch ($split[1]) {
                case 'agreement':
                    $objResult = \Srhinow\ProjectManagerBundle\Model\IaoAgreementsModel::findByIdOrAlias($idAlias);
                    break;
                case 'credit':
                    $objResult = \Srhinow\ProjectManagerBundle\Model\IaoCreditModel::findByIdOrAlias($idAlias);
                    break;
                case 'credititem':
                    $objResult = \Srhinow\ProjectManagerBundle\Model\IaoCreditItemsModel::findByIdOrAlias($idAlias);
                    break;
                case 'invoice':
                    $objResult = \Srhinow\ProjectManagerBundle\Model\IaoInvoiceModel::findByIdOrAlias($idAlias);
                    break;
                case 'invoiceitem':
                    $objResult = \Srhinow\ProjectManagerBundle\Model\IaoInvoiceItemsModel::findByIdOrAlias($idAlias);
                    break;
                case 'offer':
                    $objResult = \Srhinow\ProjectManagerBundle\Model\IaoOfferModel::findByIdOrAlias($idAlias);
                    break;
                case 'offeritem':
                    $objResult = \Srhinow\ProjectManagerBundle\Model\IaoOfferItemsModel::findByIdOrAlias($idAlias);
                    break;
                case 'project':
                    $objResult = \Srhinow\ProjectManagerBundle\Model\IaoProjectsModel::findByIdOrAlias($idAlias);
                    break;
                default:
                    $objResult = null;
            }

            if (null !== $objResult) {
                return $objResult->{$split[2]};
            }
        }

        return false;
    }
}
