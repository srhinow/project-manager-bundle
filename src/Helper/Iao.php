<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Helper;

use Contao\Controller;
use Contao\Database as DB;
use Contao\FilesModel;
use Contao\MemberModel;
use Contao\Model;
use Contao\Model\Collection;
use Contao\StringUtil;
use setasign\Fpdi\PdfParser\PdfParserException;
use Srhinow\ProjectManagerBundle\Helper\PDF\IaoPDF;
use Srhinow\ProjectManagerBundle\Model\IaoCreditModel;
use Srhinow\ProjectManagerBundle\Model\IaoInvoiceModel;
use Srhinow\ProjectManagerBundle\Model\IaoItemUnitsModel;
use Srhinow\ProjectManagerBundle\Model\IaoProjectsModel;
use Srhinow\ProjectManagerBundle\Model\IaoReminderModel;
use Srhinow\ProjectManagerBundle\Model\IaoSettingsModel;

/**
 * Class iao
 * Provide methods to handle project-manager-bundle-module.
 */
class Iao extends Controller
{
    /**
     * Current object instance (do not remove).
     *
     * @var Iao
     */
    protected static Iao $objInstance;

    protected $db;

    /**
     * Initialize the object.
     */
    protected function __construct()
    {
        parent::__construct();

        $this->db = DB::getInstance();
    }

    /**
     * Instantiate a new iao object (Factory).
     *
     * @return static The object instance
     */
    public static function getInstance(): Iao|static
    {
        if (null === static::$objInstance) {
            static::$objInstance = new static();
        }

        return static::$objInstance;
    }

    /**
     * get current settings.
     *
     * @param null $id
     *
     * @return array
     */
    public function getSettings($id = null): array
    {
        if(isset($GLOBALS['IAO']['default_settings_id']) && null === $id) {
            $id = $GLOBALS['IAO']['default_settings_id'];
        }

        if (null === $id || null === ($objDb = IaoSettingsModel::findByPk($id))) {
            if (null === ($objDb = IaoSettingsModel::findOneBy(['fallback=?'], ['1']))) {
                $objDb = IaoSettingsModel::findOneBy(['id != ?'], ['0']);
            }
        }

        return $objDb->row();
    }

    /**
     * Get netto-price from brutto.
     *
     * @param float $brutto
     * @param int $vat
     *
     * @return float
     */
    public function getNettoPrice(float $brutto, float $vat): float
    {
        return ($brutto * 100) / ($vat + 100);
    }

    /**
     * Get brutto-price from netto.
     *
     * @param float $netto
     * @param int $vat
     *
     * @return float
     */
    public function getBruttoPrice(float $netto, float $vat): float
    {
        return ($netto / 100) * ($vat + 100);
    }

    /**
     * get Umsatzsteuer-Betrag vom netto.
     *
     * @param float $netto
     * @param int $vat
     *
     * @return float|int
     */
    public function getUmstAmount(float $netto, float $vat): float|int
    {
        return ($netto * $vat) / 100;
    }

    /**
     * Get formated price-string.
     *
     * @param float $price
     * @param string $currencyStr
     *
     * @return string
     */
    public function getPriceStr(float $price, string $currencyStr = 'iao_currency'): string
    {
        $arrSettings = $this->getSettings();
        return number_format((float) $price, 2, ',', '.').' '.$arrSettings[$currencyStr];
    }

    public function getDateStr($value): string
    {
        $timestamp = (0 === $value) ? time() : $value;

        return date($GLOBALS['TL_CONFIG']['dateFormat'], $timestamp);
    }

    /**
     * change Contao-Placeholder with html-characters.
     *
     * @param string $text
     *
     * @return string
     */
    public function changeTags(string $text): string
    {
        // replace [&] etc.
        $text = StringUtil::restoreBasicEntities($text);

        // replace Insert tags
        return $this->replaceInsertTags($text);
    }

    /**
     * replace Insert-Tags from IAO - DB-Tables.
     *
     * @param $strBuffer string
     * @param $sector string
     * @param $obj object
     *
     * @return string
     */
    public function changeIAOTags(string $strBuffer, string $sector, object $obj): string
    {
        if (!$strBuffer) {
            return $strBuffer;
        }

        $tags = preg_split('/\{\{([^}]+)}}/', $strBuffer, -1, PREG_SPLIT_DELIM_CAPTURE);
        if(count($tags) < 2) {
            return $strBuffer;
        }

        $strBuffer = '';
        $arrCache = [];

        for ($_rit = 0; $_rit <= \count($tags); $_rit = $_rit + 2) {
            $strBuffer .= $tags[$_rit];

            // Skip empty tags
            if(!isset($tags[$_rit + 1])) {
                continue;
            }
            $strTag = $tags[$_rit + 1];

            // Load value from cache array
            if (isset($arrCache[$strTag])) {
                $strBuffer .= $arrCache[$strTag];
                continue;
            }

            $parts = StringUtil::trimsplit('::', $strTag);
            $parts[0] = strip_tags($parts[0]);
            $parts[1] = strip_tags($parts[1]);

            $arrCache[$strTag] = '';

            // Replace the tag
            switch (strtolower($parts[0])) {
                // get table - Data
                case 'member':
                    $objMember = MemberModel::findByPk($obj->member);
                    if (null !== $objMember) {
                        $objMember->gender = ('male' === $objMember->gender)
                            ? $GLOBALS['TL_LANG']['tl_iao']['gender']['male']
                            : $GLOBALS['TL_LANG']['tl_iao']['gender']['female'];

                        $arrCache[$strTag] = $objMember->{$parts[1]};
                    }
                    break;
                case 'invoice':
                    $invoiceId = false;
                    $sector = strtolower($sector);

                    //relative from this section
                    if ('invoice' === $sector) {
                        $invoiceId = $obj->id;
                    } elseif ('reminder' === $sector) {
                        $invoiceId = $obj->invoice_id;
                    }

                    if ($invoiceId) {
                        $objInvoice = IaoInvoiceModel::findByPk($invoiceId);
                        $objInvoice->expiry_date = date($GLOBALS['TL_CONFIG']['dateFormat'], (int) $objInvoice->expiry_date);
                        $objInvoice->brutto = $this->getPriceStr((float) $objInvoice->price_brutto);
                        $objInvoice->netto = $this->getPriceStr((float) $objInvoice->price_netto);

                        $arrCache[$strTag] = $objInvoice->{$parts[1]};
                    }
                    break;
                case 'reminder':
                    $objReminder = IaoReminderModel::findByPk($obj->id);
                    if (null !== $objReminder) {
                        $arrR = $objReminder->row();
                        $arrR['PeriodeDateFormat'] = date($GLOBALS['TL_CONFIG']['dateFormat'], (int) $objReminder->periode_date);
                        $arrR['step'] = !\strlen($obj->step) ? 1 : $obj->step;
                        $arrR['postageStr'] = (((int) ($objReminder->postage) <= 0)) ? '' : $this->getPriceStr((float) $objReminder->postage);
                        $arrR['taxStr'] = ((int) ($objReminder->tax) > 0) ? $objReminder->tax.'%' : '';
                        $arrR['sum'] = $this->getReminderSum($objReminder);
                        $arrR['sumStr'] = $this->getPriceStr((float) $arrR['sum']);

                        $arrCache[$strTag] = $arrR[$parts[1]];
                    }
                    break;

                case 'credit':
                    $objCredit = IaoCreditModel::findByPk($obj->id);

                    $arrCache[$strTag] = $objCredit->{$parts[1]};
                    break;

                case 'project':
                    // holt alle Projektdaten anhand der Eltern-ID (pid) aus den bereichen invoice_offer_credit etc.
                    // die ein Projekt zugeordnet sein können.
                    $objProject = IaoProjectsModel::findByPk($obj->pid);

                    $arrCache[$strTag] = $objProject->{$parts[1]};
                    break;

                case 'agreement':
                    if ('date_format' === $parts[2]) {
                        $arrCache[$strTag] = date($GLOBALS['TL_CONFIG']['dateFormat'], (int) $obj->{$parts[1]});
                    } else {
                        $arrCache[$strTag] = $obj->{$parts[1]};
                    }
                    break;

                case 'iao':
                    switch (strtolower($parts[1])) {
                        case 'current_date':
                            $arrCache[$strTag] = date($GLOBALS['TL_CONFIG']['dateFormat']);
                            break;
                    }
                    break;
            }
            $strBuffer .= $arrCache[$strTag];
        }

        return $strBuffer;
    }

    /**
     * @param string $table
     * @param $id
     * @return Collection|array|Model|null
     */
    public function getTemplateObject(string $table, $id): Model\Collection|array|Model|null
    {
        //get referenz
        $strModelClass = Model::getClassFromTable($table);
        if (!class_exists($strModelClass)) {
            return null;
        }

        /* @var Model $strModelClass */
        return $strModelClass::findByPk($id);
    }

    /**
     * generiert für die jeweilige Mahnung-Stufe ein PDF und gibt diese an den Browser.
     *
     * @param int $id
     * @param string $type
     *
     * @throws PdfParserException
     */
    public function generateReminderPDF(int $id, string $type = ''): void
    {
        $table = 'tl_iao_'.$type;

        //wenn type oder id fehlen abbrechen
        if ((int) $id < 1 || $this->db->tableExists($table) < 1) {
            return;
        }

        $modelClass = Model::getClassFromTable($table);

        if (!$modelClass) {
            return;
        }

        $objData = $modelClass::findByPk($id);
        if (null === $objData) {
            return;
        }

        $settings = $this->getSettings($objData->setting_id);

        //template zuweisen
        $templateFile = ('reminder' === $type)
            ? $settings['iao_'.$type.'_'.$objData->step.'_pdf']
            : $settings['iao_'.$type.'_pdf'];

        $invoiceObj = IaoInvoiceModel::findByPk($objData->invoice_id);
        $strReminderName = $GLOBALS['TL_LANG']['tl_iao_reminder']['steps'][$objData->step]
            .'-'.$invoiceObj->invoice_id_str
            .'-'.$objData->id;

        // Calculating dimensions
        $margins = unserialize($settings['iao_pdf_margins']);         // Margins as an array
        switch ($margins['unit']) {
            case 'cm':      $factor = 10.0; break;
            default:        $factor = 1.0;
        }

        $dim['top'] = !is_numeric($margins['top']) ? PDF_MARGIN_TOP : $margins['top'] * $factor;
        $dim['right'] = !is_numeric($margins['right']) ? PDF_MARGIN_RIGHT : $margins['right'] * $factor;
        $dim['bottom'] = !is_numeric($margins['top']) ? PDF_MARGIN_BOTTOM : $margins['bottom'] * $factor;
        $dim['left'] = !is_numeric($margins['left']) ? PDF_MARGIN_LEFT : $margins['left'] * $factor;

        // TCPDF configuration
        $l['a_meta_dir'] = 'ltr';
        $l['a_meta_charset'] = $GLOBALS['TL_CONFIG']['characterSet'];
        $l['a_meta_language'] = $GLOBALS['TL_LANGUAGE'];
        $l['w_page'] = 'page';

        $objPdfTemplate = \FilesModel::findByUuid($templateFile);
        if (\strlen($objPdfTemplate->path) < 1 || !file_exists(TL_ROOT.'/'.$objPdfTemplate->path)) {
            return;
        }  // template file not found

        $pdf = new IaoPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);
        $pdf->setSourceFile(TL_ROOT.'/'.$objPdfTemplate->path);          // Set PDF template

        // Set document information
        $pdf->iaoSettings = $settings;
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle($strReminderName);
        $pdf->SetSubject($strReminderName);
        $pdf->SetKeywords($strReminderName);
        $pdf->SetDisplayMode('fullwidth', 'OneColumn', 'UseNone');
        $pdf->SetHeaderData();

        // Remove default header/footer
        $pdf->setPrintHeader(false);

        // Set margins
        $pdf->SetMargins($dim['left'], $dim['top'], $dim['right']);

        // Set auto page breaks
        $pdf->SetAutoPageBreak(true, $dim['bottom']);

        // Set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // Set some language-dependent strings
        $pdf->setLanguageArray($l);

        // Initialize document and add a page
        $pdf->AddPage();

        // Include CSS (TCPDF 5.1.000 an newer)
        $file = \FilesModel::findByUuid($settings['iao_pdf_css']);

        if (null !== $file && \strlen($file->path) > 0 && file_exists(TL_ROOT.'/'.$file->path)) {
            $styles = "<style>\n".file_get_contents(TL_ROOT.'/'.$file->path)."\n</style>\n";
            $pdf->writeHTML($styles, true, false, true, false, '');
        }

        // write the address-data
        $addressText = '';
        if(strlen((string) $objPdfTemplate->address) > 0) {
            $addressText = $this->changeIAOTags($objData->address_text, $type, $objData);
            $addressText = $this->changeTags($addressText);
        }
        $pdf->drawAddress($addressText);

        //Mahnungsnummer
        $pdf->drawDocumentNumber($invoiceObj->invoice_id_str);

        //Datum
        $pdf->drawDate(date($GLOBALS['TL_CONFIG']['dateFormat'], (int) $objData->{$type.'_tstamp'}));

        //ausgeführt am
        $newdate = (int) $objData->periode_date;
        if ($newdate > 0) {
            $pdf->drawInvoiceDurationDate(date($GLOBALS['TL_CONFIG']['dateFormat'], $newdate));
        }

        //Text
        if (strip_tags($objData->text_finish)) {
            $pdf->drawTextBefore($objData->text_finish);
        }

        // Close and output PDF document
        $pdf->lastPage();
        $pdf->Output($strReminderName.'.pdf', 'D');

        // Stop script execution
        exit();
    }

    /**
     * generiert von den verschiedenen Bereiche (offer,invoice,credit) eine PDF und gibt diese an den Browser.
     *
     * @param int $id
     * @param string $type
     *
     * @throws PdfParserException
     */
    public function generatePDF(int $id, string $type = ''): void
    {
        //wenn type oder id fehlen abbrechen
        $table = 'tl_iao_'.$type;
        if ((int) $id < 1 || $this->db->tableExists($table) < 1) {
            return;
        }

        $modelClass = Model::getClassFromTable($table);
        if (!$modelClass) {
            return;
        }

        $objData = $modelClass::findByPk($id);
        if (null === $objData) {
            return;
        }

        $settings = $this->getSettings($objData->setting_id);

        //template zuweisen
        $templateFile = ('reminder' === $type)
            ? $settings['iao_'.$type.'_'.$objData->step.'_pdf']
            : $settings['iao_'.$type.'_pdf'];

        //wenn ein festes PDF zugewiesen wurde
        $localFileName = (string) $objData->{$type.'_pdf_file'};
        if ('' !== $localFileName) {
            $objPdf = FilesModel::findByPk($localFileName);
            if (!empty($objPdf->path) && file_exists(TL_ROOT.'/'.$objPdf->path)) {
                header('Content-type: application/pdf');
                header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header('Last-Modified: '.\gmdate('D, d M Y H:i:s').' GMT');
                header('Content-Length: '.\strlen($localFileName));
                header('Content-Disposition: inline; filename="'.basename($objPdf->path).'";');

                // The PDF source is in original.pdf
                readfile(TL_ROOT.'/'.$localFileName);
                exit();
            }
        }

        $pdfname = $GLOBALS['TL_LANG']['tl_iao']['types'][$type].'-'.$objData->{$type.'_id_str'};

        // Calculating dimensions
        $margins = unserialize($settings['iao_pdf_margins']);         // Margins as an array
        switch ($margins['unit']) {
            case 'cm':      $factor = 10.0; break;
            default:        $factor = 1.0;
        }

        $dim['top'] = !is_numeric($margins['top']) ? PDF_MARGIN_TOP : $margins['top'] * $factor;
        $dim['right'] = !is_numeric($margins['right']) ? PDF_MARGIN_RIGHT : $margins['right'] * $factor;
        $dim['bottom'] = !is_numeric($margins['top']) ? PDF_MARGIN_BOTTOM : $margins['bottom'] * $factor;
        $dim['left'] = !is_numeric($margins['left']) ? PDF_MARGIN_LEFT : $margins['left'] * $factor;

        // TCPDF configuration
        $l['a_meta_dir'] = 'ltr';
        $l['a_meta_charset'] = $GLOBALS['TL_CONFIG']['characterSet'];
        $l['a_meta_language'] = $GLOBALS['TL_LANGUAGE'];
        $l['w_page'] = 'page';

        $objPdfTemplate = \FilesModel::findByUuid($templateFile);
        if (null === $objPdfTemplate) {
            return;
        }

        if (\strlen($objPdfTemplate->path) < 1 || !file_exists(TL_ROOT.'/'.$objPdfTemplate->path)) {
            return;
        }  // template file not found

        $pdf = new IaoPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);
        $pdf->setSourceFile(TL_ROOT.'/'.$objPdfTemplate->path);          // Set PDF template

        // Set document information
        $pdf->iaoSettings = $settings;
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle($pdfname);
        $pdf->SetSubject($pdfname);
        $pdf->SetKeywords($pdfname);

        $pdf->SetDisplayMode('fullwidth', 'OneColumn', 'UseNone');
        $pdf->SetHeaderData();

        // Remove default header/footer
        $pdf->setPrintHeader(false);

        // Set margins
        $pdf->SetMargins($dim['left'], $dim['top'], $dim['right']);

        // Set auto page breaks
        $pdf->SetAutoPageBreak(true, $dim['bottom']);

        // Set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // Set some language-dependent strings
        $pdf->setLanguageArray($l);

        // Initialize document and add a page
        $pdf->AddPage();

        // Include CSS (TCPDF 5.1.000 an newer)
        $file = \FilesModel::findByUuid($settings['iao_pdf_css']);
        if (null !== $file && \strlen($file->path) > 0 && file_exists(TL_ROOT.'/'.$file->path)) {
            $styles = "<style>\n".file_get_contents(TL_ROOT.'/'.$file->path)."\n</style>\n";
            $pdf->writeHTML($styles, true, false, true, false, '');
        }

        // write the address-data
        $addressText = $this->changeIAOTags($objData->address_text, $type, $objData);
        $addressText = $this->changeTags($addressText);
        $pdf->drawAddress($addressText);

        //Rechnungsnummer
        $pdf->drawDocumentNumber($objData->{$type.'_id_str'});

        //Datum
        $pdf->drawDate(date($GLOBALS['TL_CONFIG']['dateFormat'], (int) $objData->{$type.'_tstamp'}));

        //ausgeführt am
        if ($objData->execute_date) {
            $newdate = (int) $objData->execute_date;
            $pdf->drawInvoiceExecuteDate(date($GLOBALS['TL_CONFIG']['dateFormat'], $newdate));
        }

        //gültig bis
        if ($objData->expiry_date) {
            $newdate = (int) $objData->expiry_date;
            $pdf->drawInvoiceDurationDate(date($GLOBALS['TL_CONFIG']['dateFormat'], $newdate));
        }

        //Text vor der Posten-Tabelle
        if ($objData->before_text && strip_tags($objData->before_text)) {
            $beforeText = $this->changeIAOTags($objData->before_text, $type, $objData);
            $beforeText = $this->changeTags($beforeText);
            $pdf->drawTextBefore($beforeText);
        }

        //Posten-Tabelle
        $header = ['Menge', 'Beschreibung', 'Einzelpreis', 'Gesamt'];
        $fields = $this->getPosten($objData->id, $type);

        $pdf->drawPostenTable($header, $fields, $objData->noVat);

        //Text nach der Posten-Tabelle
        if ($objData->after_text && strip_tags($objData->after_text)) {
            $afterText = $this->changeIAOTags($objData->after_text, $type, $objData);
            $afterText = $this->changeTags($afterText);
            $pdf->drawTextAfter($afterText);
        }

        // Close and output PDF document
        $pdf->lastPage();
        $pdf->Output($pdfname.'.pdf', 'D');

        // Stop script execution
        exit();
    }

    public function getPosten($id, $type = '')
    {
        $posten = [];

        //wenn type oder id fehlen abbrechen
        if ((int) $id < 1 || \strlen($type) < 1) {
            return $posten;
        }

        //hole zum jeweiligen Modul gehoerende Sprachdatei
        $this->loadLanguageFile('tl_iao_'.$type.'_items');

        //hole alle zum Elternelemente gehoerende Eintraege
        $itemTable = 'tl_iao_'.$type.'_items';

        $modelClass = Model::getClassFromTable($itemTable);
        if (null === $modelClass) {
            return $posten;
        }

        /** @var Model $modelClass */
        $objResult = $modelClass::findBy([$itemTable.'.`pid`=?', $itemTable.'.`published`= ?'], [$id, 1],['order'=>'sorting ASC']);

        // wenn keine vorhanden dann leeres array zurueck
        if (null === $objResult || $objResult->count() === 0) {
            return $posten;
        }

        while ($objResult->next()) {

            if("devider" === $objResult->type) {
                $posten['type'][] = $objResult->type;
                $posten['fields'][] = $objResult->row();
                continue;
            }

            // zum Rechnen evtl. vorhandenes deutsches format in english umwandeln
            $objResult->price_netto = str_replace(',', '.', $objResult->price_netto);

            //Überschrift zum Posten ausgabetext hinzufügen
            if (1 === (int) $objResult->headline_to_pdf) {
                $objResult->text = substr_replace(
                    $objResult->text,
                    '<p><strong>'.$objResult->headline.'</strong><br>',
                    0,
                    3
                );
            }
            $objResult->text = $this->changeTags($objResult->text);

            // get units from DB-Table
            $objUnit = IaoItemUnitsModel::findOneBy(['value=?'], [$objResult->amountStr]);

            $formatCount = stripos($objResult->count, '.') ? number_format((float) $objResult->count, 1, ',', '.') : $objResult->count;

            $single_price = $count_price = 0;
            // wenn netto-Preis eingegeben wurde
            if (1 === (int) $objResult->vat_incl ) {
                if (0 < (float) $objResult->price_brutto) {
                    $single_price = $objResult->price_brutto / ($objResult->count>0)?:1;
                    $count_price = $objResult->price_brutto;
                }
            }
            // wenn Brutto-Preis eingegeben wurde
            else {
                if (0 < (float) $objResult->price_netto) {
                    $single_price = $objResult->price_netto / ($objResult->count>0)?:1;
                    $count_price = $objResult->price_brutto;
                }
            }

            $netto_price = $this->getNettoPrice((float) $count_price, (float) $objResult->vat);

            $posten['fields'][] = [
                $formatCount.' '.(((float) $objResult->count <= 1) ? $objUnit->singular : $objUnit->majority),
                $objResult->text,
                number_format((float) $objResult->price, 2, ',', '.'),
                number_format((float) $count_price, 2, ',', '.'),
            ];

            $posten['pagebreak_after'][] = $objResult->pagebreak_after;
            $posten['type'][] = $objResult->type;

            $posten['discount'] = false;
            if(!isset($posten['summe'])) {
                $posten['summe'] = [
                    'single_price'=>0,
                    'count_price'=>0,
                    'mwst'=>[],
                    'netto_format'=>0,
                    'brutto_format'=>0
                ];
            }

            if ('-' === $objResult->operator) {
                $posten['summe']['single_price'] -= $single_price;
                $posten['summe']['count_price'] -= $count_price;
            } else {
                $posten['summe']['single_price'] += $single_price;
                $posten['summe']['count_price'] += $count_price;
            }

            $table = 'tl_iao_'.$type;
            $tableModelClass = Model::getClassFromTable($table);

            if (!class_exists($tableModelClass)) {
                return $posten;
            }

            $objParent = $tableModelClass::findByPk($id);
            if (null !== $objParent && 1 !== $objParent->noVat) {
                if(!isset($posten['summe']['mwst'][$objResult->vat])) {
                    $posten['summe']['mwst'][(string) $objResult->vat] = 0;
                }
                $posten['summe']['mwst'][(string) $objResult->vat] += $this->getUmstAmount((float) $netto_price, (float) $objResult->vat);
            }
        }

        $netto = $this->getNettoPrice((float) $posten['summe']['count_price'], (float) $objResult->vat);
        $posten['summe']['netto_format'] = number_format(
            (float) $netto,
            2,
            ',',
            '.'
        );

        $posten['summe']['brutto_format'] = number_format(
            (float) $posten['summe']['count_price'],
            2,
            ',',
            '.'
        );

        return $posten;
    }

    /**
     * get the sum incl. tax and postage.
     *
     * @param object $objReminder
     *
     * @return float
     */
    public function getReminderSum($objReminder)
    {
        if (null === $objReminder || !\is_object($objReminder)) {
            return 0;
        }

        $postage = (float) $objReminder->postage;
        $tax = (float) $objReminder->tax;
        $price = (1 === $objReminder->tax_typ) ? $objReminder->brutto : $objReminder->netto;
        $unpaid = ((float) ($objReminder->unpaid) > 0) ? (float) $objReminder->unpaid : (float) $price;
        $sum = $unpaid;

        if ((float) $tax > 0) {
            $sum = (($sum * (100 + $tax)) / 100);
        }
        if ((float) $postage > 0) {
            $sum += $postage;
        }

        return $sum;
    }

    /**
     * set monday if date on weekend.
     *
     * @param int
     * @param int
     *
     * @return int
     */
    public function noWE($time, $dur)
    {
        //auf Sonnabend prüfen, wenn "ja" dann auf Montag setzen
        if (6 === date('N', $time + ($dur * 24 * 60 * 60))) {
            $dur = $dur + 2;
        }

        //auf Sontag prüfen, wenn "ja" dann auf Montag setzen
        if (7 === date('N', $time + ($dur * 24 * 60 * 60))) {
            $dur = $dur + 1;
        }

        return $time + ($dur * 24 * 60 * 60);
    }

    /**
     * get the next periode-date.
     *
     * @param $reminderObj
     *
     * @return int
     */
    public function getPeriodeDate($reminderObj)
    {
        $objLastReminder = IaoReminderModel::findOneBy(
            ['`invoice_id`=?', '`tl_iao_reminder`.`id`!=?'],
            [$reminderObj->invoice_id, $reminderObj->id],
            ['order' => '`tl_iao_reminder`.`id` DESC']
        );

        $now = time();
        $settings = $this->getSettings();

        $lastPeriodeDate = ($objLastReminder->periode_date) ?: $now;
        $time = (0 === $reminderObj->periode_date) ? $lastPeriodeDate : $reminderObj->periode_date;
        $step = ((int) $reminderObj->step < 1) ? 1 : $reminderObj->step;
        $dur = ((int) ($settings['iao_reminder_'.$step.'_duration']) > 0)
            ? (int) $settings['iao_reminder_'.$step.'_duration']
            : 14;

        $nextDate = ($this->noWE($time, $dur) > time())
            ? $this->noWE($time, $dur)
            : $this->noWE($now, $dur);

        return $nextDate;
    }
}
