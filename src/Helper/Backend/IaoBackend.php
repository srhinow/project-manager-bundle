<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Helper\Backend;

use Contao\BackendTemplate;
use Contao\BackendUser as User;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\Database as DB;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\MemberModel;
use Contao\StringUtil;
use Contao\System;
use Contao\UserGroupModel;
use Contao\UserModel;
use Exception;
use Srhinow\ProjectManagerBundle\Helper\Iao;
use Srhinow\ProjectManagerBundle\Model\IaoInvoiceModel;
use Srhinow\ProjectManagerBundle\Model\IaoItemUnitsModel;
use Srhinow\ProjectManagerBundle\Model\IaoProjectsModel;
use Srhinow\ProjectManagerBundle\Model\IaoReminderModel;
use Srhinow\ProjectManagerBundle\Model\IaoSettingsModel;
use Srhinow\ProjectManagerBundle\Model\IaoTaxRatesModel;
use Srhinow\ProjectManagerBundle\Model\IaoTemplatesItemsModel;
use Srhinow\ProjectManagerBundle\Model\IaoTemplatesModel;

/**
 * Class iaoBackend.
 *
 * Parent class for iaoBackend modules.
 *
 * @copyright  Sven Rhinow 2011-2017
 */
class IaoBackend extends Iao
{
    protected $db;

    /**
     * Initialize the object.
     */
    public function __construct()
    {
        parent::__construct();

        $this->db = DB::getInstance();
    }

    /**
     * check permissions for dca-modules.
     *
     * @param string
     */
    public function checkIaoModulePermission($table): void
    {
        $User = User::getInstance();
        $logger = static::getContainer()->get('monolog.logger.contao');

        if ($User->isAdmin) {
            return;
        }

        $objUser = UserModel::findByPk($User->id);

        // Set root IDs
        if (!\is_array($objUser->iaomodules) || \count($objUser->iaomodules) < 1) {
            $root = [0];
        } else {
            $root = $objUser->iaomodules;
        }

        $GLOBALS['TL_DCA'][$table]['list']['sorting']['root'] = $root;

        // Check permissions to add archives
        if (!$User->hasAccess('create', 'newp')) {
            $GLOBALS['TL_DCA'][$table]['config']['closed'] = true;
        }

        // Check current action
        switch (Input::get('act')) {
            case 'create':
            case 'select':
                // Allow
                break;

            case 'edit':
                // Dynamically add the record to the user profile
                if (!\in_array(Input::get('id'), $root, true)) {
                    $arrNew = $this->Session->get('new_records');

                    if (\is_array($arrNew[$table]) && \in_array(Input::get('id'), $arrNew[$table], true)) {
                        // Add permissions on user level
                        if ('custom' === $User->inherit || !$User->groups[0]) {
                            $arrModulep = deserialize($objUser->iaomodulep);

                            if (\is_array($arrModulep) && \in_array('create', $arrModulep, true)) {
                                $arrModules = deserialize($objUser->iaomodules);
                                $arrModules[] = Input::get('id');

                                $objUser->iaomodules = serialize($arrModules);
                                $objUser->save();
                            }
                        }

                        // Add permissions on group level
                        elseif ($User->groups[0] > 0) {
                            $objGroup = UserGroupModel::findByPk($User->groups[0]);

                            $arrModulep = deserialize($objGroup->iaomodulep);

                            if (\is_array($arrModulep) && \in_array('create', $arrModulep, true)) {
                                $arrModules = deserialize($objGroup->iaomodules);
                                $arrModules[] = Input::get('id');

                                $objGroup->iaomodules = serialize($$arrModules);
                                $objGroup->save();
                            }
                        }

                        // Add new element to the user object
                        $root[] = Input::get('id');
                        $User->iaomodules = $root;
                    }
                }
                // no break;

            case 'copy':
            case 'delete':
            case 'show':
                if (!\in_array(Input::get('id'), $root, true)
                    || ('delete' === Input::get('act')
                        && !$User->hasAccess('delete', 'iaomodulep'))
                ) {
                    $logger->log(
                        'Not enough permissions to '.Input::get('act').' iao module ID "'.Input::get('id').'"',
                        $table.' checkPermission',
                        ['contao' => new ContaoContext(__CLASS__.'::'.__METHOD__, 'ERROR')]
                    );
                    $this->redirect('contao/main.php?act=error');
                }
            break;

            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
                $objSession = System::getContainer()->get('session');
                $session = $objSession->all();
                if ('deleteAll' === Input::get('act') && !$User->hasAccess('delete', 'iaomodulep')) {
                    $session['CURRENT']['IDS'] = [];
                } else {
                    $session['CURRENT']['IDS'] = array_intersect($session['CURRENT']['IDS'], $root);
                }
                $objSession->replace($session);
                break;

            default:
                if (\strlen(Input::get('act'))) {
                    $logger->log(
                        'Not enough permissions to '.Input::get('act').' iao modules',
                        $table.' checkPermission',
                        ['contao' => new ContaoContext(__CLASS__.'::'.__METHOD__, 'ERROR')]
                    );
                    $this->redirect('contao/main.php?act=error');
                }
            break;
        }
    }

    /**
     * check permissions for dca-settings.
     *
     * @param string
     */
    public function checkIaoSettingsPermission($table): void
    {
        $User = User::getInstance();
        $logger = static::getContainer()->get('monolog.logger.contao');

        if ($User->isAdmin) {
            return;
        }

        $objUser = UserModel::findByPk($User->id);

        // Set root IDs
        if (!\is_array($objUser->iaomodules) || \count($objUser->iaomodules) < 1) {
            $root = [0];
        } else {
            $root = $objUser->iaomodules;
        }

        // Check permissions to add archives
        if (!$User->hasAccess('create', 'newp')) {
            $GLOBALS['TL_DCA'][$table]['config']['closed'] = true;
        }

        // Check current action
        switch (Input::get('act')) {
            case 'create':
            case 'select':
                // Allow
                break;
            case 'edit':
                // Dynamically add the record to the user profile
                if (!\in_array(Input::get('id'), $root, true)) {
                    $arrNew = $this->Session->get('new_records');

                    if (\is_array($arrNew[$table]) && \in_array(Input::get('id'), $arrNew[$table], true)) {
                        // Add permissions on user level
                        if ('custom' === $User->inherit || !$User->groups[0]) {
                            $arrModulep = StringUtil::deserialize($objUser->iaosettingp);

                            if (\is_array($arrModulep) && \in_array('create', $arrModulep, true)) {
                                $arrModules = StringUtil::deserialize($objUser->iaosettings);
                                $arrModules[] = Input::get('id');

                                $objUser->iaosettings = serialize($arrModules);
                                $objUser->save();
                            }
                        }

                        // Add permissions on group level
                        elseif ($User->groups[0] > 0) {
                            $objGroup = UserGroupModel::findByPk($User->groups[0]);

                            $arrModulep = deserialize($objGroup->iaosettingp);

                            if (\is_array($arrModulep) && \in_array('create', $arrModulep, true)) {
                                $arrModules = deserialize($objGroup->iaosettings);
                                $arrModules[] = Input::get('id');

                                $objGroup->iaosettings = serialize($arrModules);
                                $objGroup->save();
                            }
                        }

                        // Add new element to the user object
                        $root[] = Input::get('id');
                        $User->iaosettings = $root;
                    }
                }
                // no break;
            case 'copy':
            case 'delete':
            case 'show':
                if (!\in_array(Input::get('id'), $root, true)
                    || ('delete' === Input::get('act') && !$User->hasAccess('delete', 'iaosettingp'))) {
                    $logger->log(
                        'Not enough permissions to '.Input::get('act').' iao module ID "'.Input::get('id').'"',
                        $table.' checkPermission',
                        ['contao' => new ContaoContext(__CLASS__.'::'.__METHOD__, 'ERROR')]
                    );
                    $this->redirect('contao/main.php?act=error');
                }
                break;

            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
                $objSession = System::getContainer()->get('session');
                $session = $objSession->all();
                if ('deleteAll' === Input::get('act') && !$User->hasAccess('delete', 'iaosettingp')) {
                    $session['CURRENT']['IDS'] = [];
                } else {
                    $session['CURRENT']['IDS'] = array_intersect($session['CURRENT']['IDS'], $root);
                }
                $objSession->replace($session);
                break;

            default:
                if (\strlen(Input::get('act'))) {
                    $logger->log('Not enough permissions to '.Input::get('act').' iao modules', $table.' checkPermission', ['contao' => new ContaoContext(__METHOD__, 'ERROR')]);
                    $this->redirect('contao/main.php?act=error');
                }
                break;
        }
    }

    /**
     * Return the "toggle visibility" button.
     *
     * @param array
     * @param string
     * @param string
     * @param string
     * @param string
     * @param string
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        $href .= '&amp;id=' . $row['id'].'&amp;rt='.REQUEST_TOKEN;

        if (!$row['published'])
        {
            $icon = 'logout.gif';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '" onclick="Backend.getScrollOffset();return AjaxRequest.toggleField(this,true)">' . Image::getHtml($icon, $label, 'data-icon="' . Image::getPath('ok.gif') . '" data-icon-disabled="' . Image::getPath('logout.gif') . '" data-state="' . ($row['published'] ? 1 : 0) . '"') . '</a> ';
    }

    /**
     * set the default-value for tax-field.
     *
     * @param $table
     * @param $id
     */
    public function setDefaultTaxRate($table, $id): void
    {
        $objDefaultTaxRate = IaoTaxRatesModel::findOneBy(['`default_value`=?'], 1);

        if (null !== $objDefaultTaxRate) {
            $set = [
                'vat' => $objDefaultTaxRate->value,
            ];

            DB::getInstance()
                ->prepare("UPDATE $table %s WHERE id=?")
                ->set($set)
                ->limit(1)
                ->execute($id)
            ;
        }
    }

    /**
     * get options for tax rates.
     *
     * @param object
     *
     * @return array
     */
    public function getTaxRatesOptions($dc)
    {
        $options = [];
        $arrModelOptions['order'] = '`default_value` DESC,`sorting` ASC';

        $objTaxRates = IaoTaxRatesModel::findAll($arrModelOptions);
        if (null === $objTaxRates) {
            return $options;
        }

        while ($objTaxRates->next()) {
            $options[(string) $objTaxRates->value] = $objTaxRates->name;
        }

        return $options;
    }

    /**
     * set the default-value for tax-field.
     *
     * @param $table
     * @param $id
     */
    public function setDefaultItemUnit($table, $id): void
    {
        $objItemUnit = IaoItemUnitsModel::findOneBy(['`default_value`=?'], 1);

        if (null !== $objItemUnit) {
            $set = [
                'amountStr' => $objItemUnit->value,
            ];

            DB::getInstance()
                ->prepare("UPDATE $table %s WHERE id=?")
                ->set($set)
                ->limit(1)
                ->execute($id)
            ;
        }
    }

    /**
     * get options for item units.
     *
     * @param object
     *
     * @return array
     */
    public function getItemUnitsOptions(DataContainer $dc)
    {
        $options = [];
        $arrModelOptions['order'] = '`sorting` ASC';

        $objUnitItems = IaoItemUnitsModel::findAll($arrModelOptions);
        if (null === $objUnitItems) {
            return $options;
        }

        while ($objUnitItems->next()) {
            $options[$objUnitItems->value] = $objUnitItems->name;
        }

        return $options;
    }

    /**
     * get all members to valid groups.
     *
     * @param object
     *
     * @return array
     */
    public function getMemberOptions(DataContainer $dc)
    {
        //fallback
        $setId = ($dc->activeRecord->setting_id)?? $GLOBALS['IAO']['default_settings_id'];
        $settings = $this->getSettings($setId);
        $options = [];

        if (!$settings['iao_costumer_group']) {
            return $options;
        }

        $objMember = MemberModel::findBy(['`iao_group`=?'], [$settings['iao_costumer_group']]);
        if (null === $objMember) {
            return $options;
        }

        while ($objMember->next()) {
            $strLabel = $objMember->firstname.' '.$objMember->lastname;
            if ($objMember->company) {
                $strLabel = $objMember->company.' :: '.$strLabel;
            }
            $options[$objMember->id] = $strLabel;
        }

        return $options;
    }

    /**
     * get all settings as select-option-values.
     *
     * @param object
     *
     * @return array
     */
    public function getSettingOptions(DataContainer $dc)
    {
        $options = [];
        $arrModelOptions['order'] = '`fallback` DESC, `name` DESC';

        $objSettings = IaoSettingsModel::findAll($arrModelOptions);
        if (null === $objSettings) {
            return $options;
        }

        while ($objSettings->next()) {
            $options[$objSettings->id] = $objSettings->name;
        }

        return $options;
    }

    /**
     * get all projects as select-option-values.
     *
     * @return array
     */
    public function getProjectOptions()
    {
        $options = [];

        $objProjects = IaoProjectsModel::findAll();
        if (null === $objProjects) {
            return $options;
        }

        while ($objProjects->next()) {
            $options[$objProjects->id] = $objProjects->name;
        }

        return $options;
    }

    /**
     * @throws Exception
     */
    public function fillReminderFields(IaoReminderModel $objReminder): void
    {
        if (null === $objReminder) {
            return;
        }

        $settings = $this->getSettings($objReminder->setting_id);
        $address_text = '';

        $objInvoice = IaoInvoiceModel::findByPk($objReminder->invoice_id);
        if (!\is_object($objInvoice)) {
            throw new \Exception('Keine passende Rechnung in fillReminderFields() gefunden.');
        }

        //falls kein Member (Bug: Service->Invoice) gesetzt wurde, member aus den Projekt-Einstellungen ermitteln
        $memberId = (int) $objInvoice->member;
        if (1 > $memberId && 0 < $objInvoice->pid) {
            $objProject = IaoProjectsModel::findByPk($objInvoice->pid);
            if (null === $objProject) {
                throw new \Exception('Kein Projekt in fillReminderFields() gefunden.');
            }

            $memberId = (int) $objProject->member;
        }

        $objMember = MemberModel::findByPk($memberId);
        if (null === $objMember) {
            throw new \Exception('Kein Member in fillReminderFields() gefunden.');
        }

        if (!empty($objInvoice->address_text)) {
            $address_text = $objInvoice->address_text;
        } elseif (null !== $objMember) {
            $addressTemplate = new BackendTemplate('be_address');
            $addressTemplate->member = $objMember;
            $addressTemplate->strGender = $GLOBALS['TL_LANG']['tl_iao_reminder']['gender'][$objMember->gender];

            $address_text = $addressTemplate->parse();
        }

        $objLastReminder = IaoReminderModel::getLastReminder($objInvoice->id, $objReminder->id);

        $newStep = (null === $objLastReminder) ? 1 : (int) $objLastReminder->step + 1;

        //set an error if newStep > 4
        if ($newStep > 4) {
            \Message::addError(
                sprintf(
                    $GLOBALS['TL_LANG']['tl_iao_reminder']['to_much_steps'],
                    $objInvoice->invoice_id_str
                )
            );
            $this->reload();
        }

        $newUnpaid = (($objLastReminder->numRows > 0) && ((int) $objLastReminder->sum > 0))
            ? $objLastReminder->sum
            : $objInvoice->price_brutto;

        $tax = (float) $settings['iao_reminder_'.$newStep.'_tax'];
        $postage = (float) $settings['iao_reminder_'.$newStep.'_postage'];
        $periode_date = (int) $this->getPeriodeDate($objReminder);

        //set sum after other facts is saved
        $text_finish = $this->changeIAOTags($settings['iao_reminder_'.$newStep.'_text'], 'reminder', $objReminder);
        $text_finish = $this->changeTags($text_finish);

        $objReminder->title = (string) $GLOBALS['TL_LANG']['tl_iao_reminder']['steps'][$newStep].'::'.$objInvoice->id;
        $objReminder->address_text = (string) $address_text;
        $objReminder->member = (int) $objMember->id;
        $objReminder->unpaid = (float) $newUnpaid;
        $objReminder->step = (int) $newStep;
        $objReminder->text = (string) $settings['iao_reminder_'.$newStep.'_text'];
        $objReminder->periode_date = (int) $periode_date;
        $objReminder->tax = (int) $tax;
        $objReminder->postage = (int) $postage;
        $objReminder->sum = $this->getReminderSum($objReminder);
        $objReminder->text_finish = $text_finish;
        $objReminder->save();

        //update invoice-data with current reminder-step
        $objInvoice->reminder_id = $objReminder->id;
        $objInvoice->save();
    }

    /**
     * if GET-Param projonly then fill member and address-field.
     *
     * @param string
     * @param int
     * @param array
     * @param object
     */
    public function setMemberfieldsFromProject($table, $id, $set, $obj): void
    {
        if (1 === (int) Input::get('onlyproj') && (int) $set['pid'] > 0) {
            $objProject = IaoProjectsModel::findByPk($set['pid']);

            if (null !== $objProject) {
                $set = [
                    'member' => $objProject->member,
                    'address_text' => $this->getAddressText((int) $objProject->member),
                ];

                DB::getInstance()->prepare('UPDATE '.$table.' %s WHERE `id`=?')
                                ->set($set)
                                ->limit(1)
                                ->execute($id)
                ;
            }
        }
    }

    /**
     * generate a Invoice-number-string if not set.
     *
     * @param int
     * @param int
     * @param array
     *
     * @return string
     */
    public function generateInvoiceNumberStr($invoiceId, $tstamp, $settings)
    {
        $format = $settings['iao_invoice_number_format']??'';
        $format = str_replace('{date}', date('Ymd', $tstamp), $format);

        return str_replace('{nr}', (string) $invoiceId, $format);
    }

    /**
     * generate a invoice-number if not set.
     *
     * @param int   $varValue
     * @param array $settings
     *
     * @return int|mixed|string|null
     */
    public function generateInvoiceNumber($varValue = 0, $settings = [])
    {
        $id = Input::get('id');
        $varValue = (int) $varValue;
        // Generate invoice_id if there is none
        if (0 === $varValue) {
            $objLastInvoice = IaoInvoiceModel::findOneBy(null, null, ['order' => '`invoice_id` DESC']);

            if (null === $objLastInvoice || 0 === $objLastInvoice->invoice_id) {
                $varValue = $settings['iao_invoice_startnumber'];
            } else {
                $varValue = $objLastInvoice->invoice_id + 1;
            }
        } else {
            $objLastInvoice = IaoInvoiceModel::findOneBy(['`tl_iao_invoice`.`id`='.$id.' OR `tl_iao_invoice`.`invoice_id`='.$varValue], null);
            if (null !== $objLastInvoice) {
                $this->generateInvoiceNumber(++$varValue);
            }
        }

        return $varValue;
    }

    /**
     * generiert HTML für das Anschrift-Feld für Rechnung, Angebot,Gutschrift etc.
     *
     * @param int $intMember
     *
     * @return string
     */
    public function getAddressText(int $intMember): string
    {
        $returnAddressText = '';

        $objMember = MemberModel::findByPk($intMember);
        if (null === $objMember) {
            return $returnAddressText;
        }

        $returnAddressText = $objMember->address_text;

        if (null === $returnAddressText || '' === trim(strip_tags($returnAddressText))) {
            $addressTemplate = new BackendTemplate('be_address');
            $addressTemplate->member = $objMember;
            $addressTemplate->strGender = $GLOBALS['TL_LANG']['tl_iao_reminder']['gender'][$objMember->gender];
            $returnAddressText = $addressTemplate->parse();
        }

        return $returnAddressText;
    }

    public function getTemplateItemValues($id = 0)
    {
        $options = [];

        if (0 === $id) {
            return $options;
        }

        $objItems = IaoTemplatesItemsModel::findByPk($id);
        if (null === $objItems) {
            return $options;
        }

        return [
            'tstamp' => time(),
            'headline' => $objItems->headline,
            'headline_to_pdf' => $objItems->headline_to_pdf,
            'sorting' => $objItems->sorting,
            'date' => $objItems->date,
            'time' => $objItems->time,
            'text' => $objItems->text,
            'count' => $objItems->count,
            'amountStr' => $objItems->amountStr,
            'operator' => $objItems->operator,
            'price' => $objItems->price,
            'price_netto' => $objItems->price_netto,
            'price_brutto' => $objItems->price_brutto,
            'published' => $objItems->published,
            'vat' => $objItems->vat,
            'vat_incl' => $objItems->vat_incl,
        ];
    }

    /**
     * Erstellt ein Template-Item mit einem bestimmten Typ.
     *
     * @param string $type
     *
     * @return int
     */
    public function createTemplateItem(\Contao\Model $objResult = null, $type = '')
    {
        if (!\is_object($objResult) || \strlen($type) < 1) {
            return 0;
        }

        //Insert Invoice-Entry
        $postenset = [
            'tstamp' => time(),
            'headline' => $objResult->headline,
            'headline_to_pdf' => $objResult->headline_to_pdf,
            'sorting' => $objResult->sorting,
            'date' => $objResult->date,
            'time' => $objResult->time,
            'text' => $objResult->text,
            'count' => $objResult->count,
            'price' => $objResult->price,
            'amountStr' => $objResult->amountStr,
            'operator' => $objResult->operator,
            'price_netto' => $objResult->price_netto,
            'price_brutto' => $objResult->price_brutto,
            'published' => $objResult->published,
            'vat' => $objResult->vat,
            'vat_incl' => $objResult->vat_incl,
            'position' => $type,
        ];

        $objTemplateItem = new IaoTemplatesItemsModel();
        $objTemplateItem->setRow($postenset);
        $objTemplateItem->save();

        return $objTemplateItem->id;
    }

    /**
     * @param int $id
     */
    public function redirectToTemplateItem($id = 0): void
    {
        if (0 === (int) $id) {
            return;
        }

        $location = 'contao/main.php?do=iao_setup&mod=iao_templates_items&table=tl_iao_templates_items&act=edit&id='.$id;
        $this->redirect($location);
    }

    /**
     * save templates for any sections.
     *
     * @param string $text
     * @param int $template
     * @param string $position
     *
     * @return string
     */
    public function saveTextAsTemplate(string $text = '', int $template = 0, string $position = ''): bool
    {
        $rowText = strip_tags($text);
        if ('' !== $rowText) {
            $arrSet = [
                'title' => StringUtil::substr($rowText, 50),
                'text' => $text,
                'position' => $position,
                'tstamp' => time(),
            ];

            // Wenn vorher ein Template ausgewählt wurde, wird es aktualisiert
            if (null === ($objTemplate = IaoTemplatesModel::findByPk((int) $template))) {
                    $objTemplate = new IaoTemplatesModel();
            }

            foreach($arrSet as $key => $value) {
                $objTemplate->{$key} = $value;
            }
            $objTemplate->save();
        }

        return true;
    }
}
