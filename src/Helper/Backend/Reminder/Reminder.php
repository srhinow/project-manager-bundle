<?php

declare(strict_types=1);

/*
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    srhinow/project-manager-bundle
 * @license    LGPL-3.0+
 * @see	    https://gitlab.com/srhinow/project-manager-bundle
 */

namespace Srhinow\ProjectManagerBundle\Helper\Backend\Reminder;

use Contao\Backend;
use Contao\Environment;
use Srhinow\ProjectManagerBundle\Helper\Backend\IaoBackend;
use Srhinow\ProjectManagerBundle\Model\IaoInvoiceModel;
use Srhinow\ProjectManagerBundle\Model\IaoReminderModel;

/**
 * Class Reminder.
 */
class Reminder
{
    /**
     * check all Invoices of reminder.
     *
     * @throws \Exception
     */
    public function checkReminder(): void
    {
        $IaoBackend = new IaoBackend();

        $objInvoice = IaoInvoiceModel::findBy(
            ['`status`=?', '`published`=?', '`expiry_date`<?'],
            [1, 1, time()],
            ['order' => 'invoice_id DESC']
        );

        if (null !== $objInvoice) {
            while ($objInvoice->next()) {
                // kontrollieren ob es wirklich diese Erinnerung gibt
                $objReminder = IaoReminderModel::findByPk((int) $objInvoice->reminder_id);

                if (null === $objReminder) {
                    $objReminder = new IaoReminderModel();
                    $objReminder->invoice_id = $objInvoice->id;
                    $objReminder->member = $objInvoice->member;
                    $objReminder->pid = $objInvoice->pid;
                    $objReminder->setting_id = $objInvoice->setting_id;
                    $objReminder->reminder_tstamp = time();
                    $objReminder->status = $objInvoice->status;
                    $objReminder->step = 1;
                    $objReminder->tstamp = time();
                    $objReminder->save();

                    $IaoBackend->fillReminderFields($objReminder);
                    continue;
                }

                // only the invoices in past
                if (time() < $objReminder->periode_date) {
                    continue;
                }

                // drop all where step > 3. Mahnung
                if (4 === $objReminder->step) {
                    continue;
                }

                // drop all where status = 2
                if (2 === $objReminder->status) {
                    continue;
                }

                $IaoBackend->fillReminderFields($objReminder);
            }
        }

        \Message::addConfirmation($GLOBALS['TL_LANG']['tl_iao_reminder']['Reminder_is_checked']);

        setcookie('BE_PAGE_OFFSET', '', 0, '/');
        Backend::redirect(str_replace('&key=checkReminder', '', Environment::get('request')));
    }
}
