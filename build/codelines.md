## PHP Lines of Code
`build/tools/phploc src`

## PHP Copy And Paste Detector
`build/tools/phpcpd src`

## PHP Code Sniffer
`build/tools/phpcs --colors --standard=PSR2 src`

## PHP CS Fixer
`build/tools/php-cs-fixer --config=./php.cs.dist.php fix`

## PHP CBF (Code Beautifier)
`build/tools/phpcbf src/Controller/Aircall.php --suffix=.fixed `

## PHP UNIT TESTS
`build/tools/phpunit --configuration phpunit.xml.dist`

## Robo
`build/tools/robo -f build/RoboFile.php run:tests`
