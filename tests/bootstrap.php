<?php

/*
 * This file is part of Contao.
 *
 * (c) Leo Feyer
 *
 * @license LGPL-3.0-or-later
 */

error_reporting(E_ALL);

$include = function ($file) {
    return file_exists($file) ? include $file : false;
};

if (
    false === ($loader = $include(__DIR__.'/../../../../autoload.php'))
    && false === ($loader = $include(__DIR__.'/../../../htdocs/vendor/autoload.php'))
) {
    echo __DIR__.'/../../../../autoload.php und '.__DIR__.'/../../../htdocs/vendor/autoload.php existiert nicht.'.PHP_EOL;

    exit(1);
}

return $loader;
