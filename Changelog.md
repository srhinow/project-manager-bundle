# Changes in srhinow/project-manager-bundle

## 1.2.28 (26.12.2023)
- fix setField...NumberStr() in Credit.php, Invoice.php and Offer.php

## 1.2.27.1 (26.12.2023)
- change require to  "php": ">=8.0"

## 1.2.27 (26.12.2023)
- div Fixes (template) and code-refactoring in EventListener

## 1.2.26 (27.09.2023)
- fix Invoice.php: no new invoice_id_str if not empty

## 1.2.25 (31.07.2023)
- set eval parameter as int in Agreement::addInvoice()

## 1.2.24 (19.06.2023)
- paid als toggler in Invoice eingefügt, div. Felder beim stauts/paid wechsel updaten
- assumed als toggler in Offer eingefügt, div. Felder beim status/assumed wechsel updaten

## 1.2.23 (04.04.2023)
- fix PHP8.2 issues wie false Argument-type, and empty arrmember in list-item-templates

## 1.2.22 (03.03.2023)
- update composer.json require "menatwork/contao-multicolumnwizard-bundle": "^3.5"

## 1.2.21 (03.03.2023)
- fix options from vat_icl field in tl_iao_invoice_items.php

## 1.2.20 (03.03.2023)
- fix select dca-files
- change status_options from tl_iao_credit.php to default.php
- fix InvoiceItem.php: handle prices-variables as float

## 1.2.19 (03.03.2023)
- fix IaoBackend::generateInvoiceNumberStr(), Iao::changeIaoTags()

## 1.2.18 (03.03.2023)
- fix 'addInvoice (from offer)'

## 1.2.17 (02.03.2023)
- Fix diverse Fehler unter PHP 8.2 mit debug-Modus

## 1.2.16 (02.03.2023)
- fix 'deleteConfirm' by migrate in all dca-files, remove label-rows

## 1.2.15 (02.03.2023)
- fix and rewrite with Contao 4.13 and PHP 8.2

## 1.2.14 (02.03.2023)
- remove labels and fix toggle-button in dca-files
- Iao.php: fix Posten-Sortierung, Fehler in Kommentaren korrigiert
- InvoiceItem.php: fix Methods for toggle-button

## 1.2.13 (09.06.2022)
- fix div. dca-files: remove 'rte' => false, 'mandatory' => false

## 1.2.12 (10.02.2022)
- fix generateExpiryDate() in any Classes

## 1.2.11 (10.02.2022)
- fix IaoOfferItemsModel::findPublishedItemsByOfferId() check $pid not $arrPids
- fix onsubmit-callback order in tl_iao_offer_items.php

## 1.2.10 (10.02.2022)
- fix: strict vat_inkl as integer value

## 1.2.9 (02.12.2021)
- fix: EventListener/Invoice::generateExecuteDate()
- fix: run Iao::getPosten() and check strict integer-type by field 'headline_to_pdf'
- create and fill this Changelog.md file
- run codefixer

## 1.2.8 (06.08.2021)
- fix: test with (int) to generate Netto/Brutto
- remove \ in Class-calls
- fix #1: set null as Fallback for setting_id
- fix #1: rewrite Iao::getSettings()
- fix #1: set 'saveNettoAndBrutto' as first in tl_iao_invoice_items.php

## 1.2.7 (12.03.2021)
- fix: run Iao::changeIAOTags() with $objAgreement as third Parameter, and fix generate invoice and invoice-item text-fields

## 1.2.6 (11.03.2021)
- fix: generateNewCycle() and changeIAOTags()
- set BE-Navi-Icon for >=C4.9

## 1.2.5 (13.12.2020)
- \#6: fix code in OfferItem::fillPostenFields()
- \#6: fix code in InvoiceItem::fillPostenFields()

## 1.2.4 (12.12.2020)
- \#3: add findPublishedItemsByOfferId() in IaoOfferItemsModel.php
- \#3: fix code in OfferItem::saveAllPricesToParent()
- \#3: fix code Reminder.php to correct status-toggle without errors
- \#3: fix code in OfferItem::fillPostenFields()

## 1.2.3 (04.03.2020)
- remove default => '' because db-filed is integer
- IaoBackend.php: findById -> findByPk
- fix: fillAddressText, fillBeforeText, fillAfterText

## 1.2.2 (04.02.2020)
- Projekt not found in template abfangen
- Ausgabe des Projekt-Details so optimiert das es sowohl per Ajax als auch auf separater Seite ohne Empty-Page funktioniert, Code cleaning
- code rebase from findProjectByIdOrReferenceAlias() -> findOnePublicProject()

## 1.2.1 (16.12.2019)
- Merge branch 'master' of gitlab.com:srhinow/project-manager-bundle
- move duplicate code from Models in parent class IaoModel
- src/Resources/contao/library -> src/Helper, src/Resources/contao/model -> src/Models
- create templates for dca-methodes
- run codefixer and any code-refactoring
- fix: src/Models -> scr/Model
- fix: (float) in number_format()
- add published,unpublished classes in be.css
- drop whitespace for complete class-string
- register ModelClasses for tl_iao_item_units and tl_iao_tax_rates, fix: tl_iao_template_items -> tl_iao_templates_items
- check type of null and empty string
- code refactoring and set explicit type of value
- rename IaoTaxRates.php to IaoTaxRatesModel.php
- fix tablename: tl_iao_template_items > tl_iao_templates_items
- set status default = 1 in tl_iao_invoice.php
- create list-templates for EventListener-Classes
- code refactoring and set explicit type of value, add getTemplateItemValues()
- break to long codelines
- Invoice.php: code autofixer
- fix: setcookie paramter-type 0 => ''
- refactoring code, add fillEmptyMember()
- agreement_status1 -> agreement_status1 *
- delete '; in template be_dca_agreement_list_item.html5
- create reminder-templates
- fix callback-classes in config.php
- rename ModuleIAOSetup.php -> ModuleIaoSetup.php
- add fillEmptyMember()
- add fillEmptyMember in dca onload_callback
- code refactoring and set explicit type of value
- remove unused ModuleRemember.php
- add StringUtil:: for specialchars()
- delete 'editheader' from template and template_item dca

## 1.0.31 (15.11.2019)
- add muster-files
- Fix: Issue #3
- Fix: Netto/Brutto reagiert jetzt auf von Brutto/von Netto und Postenausgabe in Brutto
- saveNettoAndBrutto() ohne Preis-Rundungen, aktualisiert
- add use Massage und Invoice-Models
- Netto und Brutto-Berechnungen angepasst
- set "use" Classes for Message, Input etc
- import: set default-Fallback when its field is empty
- use PageNotFoundException
- Cron in EventListener\Cron\CronListener geändert und funktionsweise gefixt.
- IaoHooks zu EventListener\Hooks\InsertTagsListener geändert
- services für Cron und InsertTags angepasst
- aufrufe in service-string geändert, daily-cron in weekly-cron geändert
- Agreements-Class in EventListener\Dca aus dca-File ausgelagert
- refactoring separate dca-classes in EventListener
- fix: auch deaktivierte/gekündigte Verträge bei der Email-Erinnerung ausschließen
- Fix: Iao\Dca\IaoMember -> srhinow.projectmanager.listener.dca.member
- drop empty new line

## 1.0.22 (09.09.2018)
- Fix: DB-Error durch fehlendes alias-Feld in tl_projects mit Anpassung der Modelabfrage

## 1.0.21 (30.05.2018)
- change BUOLD to 20
- Fix: findProjectByIdOrAlias() -> findByIdOrAlias()

## 1.0.20 (28.05.2018)
- FIX: replace findProjectByIdOrAlias() -> findById()

## 1.0.19 (15.05.2018)
- set pgaeTitle, keywords and description from project in nonAjax-detail-view

## 1.0.18.1 (15.05.2018)
- Fix: $objTemplate -> $this->Template in compile() - Method

## 1.0.18 (14.05.2018)
- add reference_alias-Field and generateReferenceAlias()-Method
- change to findProjectByIdOrReferenceAlias() -Method
- separate Ajax-Request from normal http-Request for FrontendTemplate
- add reference_alias in detail-link
- add isAjax-Flag for custom Frontend-Layout

## 1.0.17 (30.04.2018)
- add default (vat,amountStr) value for new item
- Fix: adressfields change if address_generate-checkbox has clicked
- manuelles Anlegen von Erinnerungen gefixt und Felderanordnung verbessert
- Fix: PDF-link-Icon in reminder-List
- Fix: generateReminder with member-data in changeIAOTags()

## 1.0.16 (24.04.2018)
- überall generate_text eingebaut und ...adress... in ...address... umbenannt

## 1.0.15 (24.04.2018)
- Fix: Fehler bei Aufruf
- Fix: übergabe und generierung des Templatetextes mit richtigem replace der Platzhalter und setzen des neuen Ranges wenn Rechnung generiert wird
- add adress_text and generate_text to customice PDF-Address
- setMemberfieldsFromProject() used now getAdressText()
- Fix: Fehler bei Aufruf
- Fix adress_text and add generate_text to customice PDF-Address

## 1.0.14 (22.03.2018)
- Fix: DB:: statt $this->Database

## 1.0.13 (22.03.2018)
- Fix: delete m from setMemmberfieldsFromProject

## 1.0.12 (21.03.2018)
- Fix: mm in setMemmberfieldsFromProject
- change int -> float in vat/tax-fields
- Build: 12

## 1.0.11 (14.03.2018)
- beforeText und AfterText mit $this->reload();

## 1.0.10 (13.03.2018)
- Fix: setMemberfieldsFromProject, List-Ausgabe angepasst
- List-Ausgabe angepasst

## 1.0.9 (12.03.2018)
- build 8
- BackendUser -> FrontendUser
- URl modifiziert um aus project-gefiltertet Sicht die PDF anzuzeigen
- button_callback für editheader askommentiert, da neuerdings der rt nicht mit übergeben wird
- in unterverzeichnisse fe und be verschoben
- autoload.ini und autoload.php gelöscht, wird in Contao 4 nicht mehr benötigt
- tl_iao_posten_templates.php entfernt
- iao_reminder.php wurde durch Reminder.php ersetzt
- Build=9
- csv_export_dir als Feld angelegt
- importLib und PHPRechnung entfernt, da es nicht weiter unterstützt wird
- Fix: Reminder "Erinnerungen aktualisieren"
- Fix: CSV-Export funktionsfähig gemacht

## 1.0.8 (08.03.2018)
- Fix: FE-Module-Klassen mit namespace
- Fix: Felder-Anordnung berichtigt
- Member -> IaoMember ohne extend BeckendModule, setCostumer konnte sonst nicht aufgerufen werden
- add agreements
- Fix: falsche Deklaration
- Fix: setCostumerGroup von Iao\Dca\IaoMember
- Fix: setCostumerGroup von Iao\Dca\IaoMember
- Fix: Frontendausgabe und Feature: Ausgabe per Projekte untergeordnet

## 1.0.7 (01.03.2018)
- add namespace and use

## 1.0.6 (28.02.2018)
- set namespace Iao\Cron\iaoCrons in CRON
- auf namespace und use umgestellt
- add @method in Commit
- kleine Anpassungen, ist aber noch zutun
- add namespace
- BUILD = 6

## 1.0.5 (27.02.2018)
- ueberfluessige Strukturen geloescht

## 1.0.4 (27.02.2018)
- auf namespase: und use: umgestellt
- add CSV import/export to project-section
- add csv-export_folder-field
- rewrite csv import/export for >= contao 4.4
- strutkur und Code modernisiert
- div. Klassen und Helper unter library neu Strutkuriert

## 1.0.3 (23.02.2018)
- Bereich "Einstellungen" gefixt und gestylet

## 1.0.2.0 (23.02.2018)
- invoice_and_offer -> project-manager-bundle
- set icon-public-path
- meny other little changes

## 1.0.1.4 (22.02.2018)
- Fix: Syntax-Einrueckung gesetzt
- InsertTagListener

## 1.0.1 (22.02.2018)
- an neuer Contao-Bundle-Struktur angepasst und grob eingerichtet
